#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

//struct A
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};

//int main()
//{
//	printf("%zd\n", sizeof(struct A));//8
//
//	return 0;
//}

//
//struct S
//{
//	char a : 3;
//	char b : 4;
//	char c : 5;
//	char d : 4;//冒号后面的数字不能超过类型的大小
//};
//
//int main()
//
//{
//	struct S s = { 0 };
//	printf("%zd\n", sizeof(struct S));//3  说明浪费了前面那一个bit位
//	s.a = 10;
//	s.b = 12;
//	s.c = 3;
//	s.d = 4;
//
//	return 0;
//}

