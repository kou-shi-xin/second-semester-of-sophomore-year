#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

void PrintArray(int* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}


void Swap(int* p1,int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}


//向下调整算法
//排升序  建大堆
void AdjustDown(int* arr, int sz, int root)
{
	int parent = root;
	int child = parent * 2 + 1;//默认是左孩子小

	while (child < sz)
	{
		//选出左右孩子中小的那一个
		if (child + 1 <sz && arr[child + 1] > arr[child])//child + 1 <sz 是防止右孩子越界
		{                               //  >  建大堆
			child += 1;//如果右孩子比左孩子小，小的那一个就变为右孩子
		}

		//再把较小的那一个和父亲比较,如果比父亲小就交换两个数据
		if (arr[child] > arr[parent])
		{          //  >  建大堆
			Swap(&arr[child], &arr[parent]);//交换
			parent = child;//再把父亲向下调整到孩子的位置，继续比较
			child = parent * 2 + 1;
		}
		else
		{
			//如果小的孩子比父亲大，那就排好了
			break;
		}
	}
}

void HeapSort(int* arr, int sz)
{
	//建堆
	for (int i = ( sz - 1 - 1 ) / 2; i >= 0; i--)//i = ( sz - 1 - 1 ) / 2 就是最后一个叶子的
	{                                        //父亲的下标，即最后一个非叶子的子树的下标
		AdjustDown(arr, sz, i);
	}

	int end = sz - 1;//置为最后一个数据
	while (end > 0)
	{
		Swap(&arr[0], &arr[end]);//把第一个和最后一个交换
		AdjustDown(arr, end, 0);//再把剩下的sz-1个继续向下调整
		end--;
	}
}


//排升序
void InsertSort(int* arr, int sz)
{
	for (int i = 0; i < sz - 1; i++)
	{
		//[0 end]有序，把end+1位置的值插入到[0 end]中，让[0 end+1]有序
		int end = i;
		int tmp = arr[end + 1];//保存end+1位置的值

		while (end >= 0)
		{
			if (tmp < arr[end])
			{
				arr[end + 1] = arr[end];//顺序往后挪
				end--;
			}
			else
			{
				break;
			}
		}
		arr[end + 1] = tmp;
	}
	
}

int main()
{
	int arr[] = { 3,6,4,8,1,2,7,0,5,9 };
	int sz = sizeof(arr) / sizeof(int);

	//HeapSort(arr, sz);//堆排序

	InsertSort(arr, sz);//直接插入排序
	PrintArray(arr, sz);

	return 0;
}