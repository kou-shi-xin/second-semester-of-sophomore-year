#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLTDataType;

typedef struct ListNode
{
	SLTDataType data;
	struct ListNode* next;
	struct ListNode* prev;

}ListNode;


//初始化链表
ListNode* ListInit();

//销毁链表
void ListDestory(ListNode* phead);

//打印数据
void ListPrint(ListNode* phead);

//尾插
void ListPushBcck(ListNode* phead, SLTDataType x);

//头插
void ListPushFront(ListNode* phead, SLTDataType x);

//尾删
void ListPopBcck(ListNode* phead);

//头删
void ListPopFront(ListNode* phead);

//查找
ListNode*  ListFind(ListNode* phead, SLTDataType x);

//在pos前插入数据
void ListInsert( ListNode* pos, SLTDataType x);

//删除pos处的数据
void ListEarse( ListNode* pos);





