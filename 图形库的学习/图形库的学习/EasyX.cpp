#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

//包含图形库头文件
#include <graphics.h>

int main()
{
	//1.创建一个窗口
	initgraph(640, 480,SHOWCONSOLE);//参数：宽度  高度

	//设置背景颜色:设置完后一定要清屏，清除原来的背景颜色
	setbkcolor(WHITE);//参数：颜色
	//清屏函数
	cleardevice();

	//设置填充颜色
	setfillcolor(BLUE);//参数：颜色

	//设置线条样式：实线，虚线，间断线……
	setlinestyle(PS_SOLID,3);//参数：参考转到定义，里面的样式单词   线条的粗细

	//设置线条颜色
	setlinecolor(BLACK);//参数：颜色

	//2.画一个圆,无填充
	circle(50,50,50);//参数：圆心坐标x y  半径

	//2.1 有边框填充
	fillcircle(150, 150, 50);//参数：圆心坐标x y  半径

	//2.2 无边框填充
	solidcircle(150, 255, 50);//参数：圆心坐标x y  半径

	//在图形界面绘制文字：文字默认是白色
	//设置文字颜色
	settextcolor(RED);//参数：颜色
	//1.绘制字符
	//outtextxy(50, 50, 'a');//参数：坐标x  y   文本内容

	//2.绘制字符串
	//会发生由于字符集导致的错误。3种解决方法：
	//1.在字符串前面加上大写的 L
	//outtextxy(150, 150, L"参数错误，找不到对应的函数~");
	//2.用TEXT()或_T将字符串括起
	//outtextxy(150, 150, TEXT("参数错误，找不到对应的函数~"));
	//3.不需要添加任何代码，项目-- 属性-- 配置属性-- 高级-- 字符集-- 改为多字节字符集(推荐使用这个)
	
	//设置文字样式
	settextstyle(30, 0, "楷体");//参数：高度  宽度  字体

	//设置文件背景模式。文字的背景默认是白色
	setbkmode(TRANSPARENT);//设置为透明

	//设置文字颜色
	settextcolor(BROWN);//参数：颜色
	//settextcolor(RGB(0,128,99));//参数：三种三原色的数值

	//outtextxy(150, 150, "参数错误，找不到对应的函数~");

	//把文字居中
	fillrectangle(200, 50, 500, 100);//参数：左上角的坐标，右下角的坐标
	char arr[] = "我是居中显示";

	//求字符串所占像素的宽度
	int width = (500 - 200) / 2 - textwidth(arr) / 2;
	int height = (100 - 50) / 2 - textheight(arr) / 2;

	outtextxy(width + 200, height + 50, arr);

	getchar();
	//1.1 关闭窗口
	closegraph();
	return 0;
}

