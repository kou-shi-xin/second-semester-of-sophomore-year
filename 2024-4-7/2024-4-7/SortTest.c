#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

//void QuickySort(int* arr, int left, int right)
//{
//	//当左右子区间不存在，或只有一个元素时，
//	//就不需要递归了，排序完成
//	if (left >= right)
//	{
//		return;
//	}
//
//	int begin = left;
//	int end = right;
//	int key = arr[begin];
//	int pivot = begin;
//
//	//这是排一趟，只排好了一个数
//	while (begin < end)
//	{
//		//左边有坑，右边end找比key小的
//		while (begin < end && arr[end] > key)
//		{
//			end--;
//		}
//
//		//小的放到了左边的坑里，右边end自己形成了新的坑
//		arr[pivot] = arr[end];
//		pivot = end;
//
//		//右边有坑，左边end找比key大的
//		while (begin < end && arr[begin] < key)
//		{
//			begin++;
//		}
//
//		//大的放到右边的坑里，左边begin自己形成新的坑
//		arr[pivot] = arr[begin];
//		pivot = begin;
//	}
//
//	//最后begin和end相遇了，把key放入该位置
//	pivot = begin;
//	arr[begin] = key;
//
//	//[left] pivot [right]
//	// [left pivot-1]  pivot [pivot+1 right]
//	//左子区间和右子区间有序，整体就有序了
//
//	QuickySort(arr, left, pivot - 1);
//	QuickySort(arr, pivot + 1, right);
//
//}
//
//void PrintArray(int* arr, int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//
//}
//
//int main()
//{
//	int arr[] = { 6,7,9,2,4,3,5,1,0,8,-1 };
//	int sz = sizeof(arr) / sizeof(int);
//
//	//快速排序
//	QuickySort(arr, 0, sz - 1);
//	PrintArray(arr, sz);
//}


void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

void QuickySort(int* arr, int left, int right)
{
	//当左右子区间不存在，或只有一个元素时，
	//就不需要递归了，排序完成
	if (left >= right)
	{
		return;
	}

	int keyi = left;
	int prev = left;
	int cur = left + 1;

	while (cur <= right)
	{
		//++prev != cur是指当cur和prev重合时不用多于的交换
		if (arr[cur] < arr[keyi] && ++prev != cur)
		{
			Swap(&arr[cur], &arr[prev]);
		}
		cur++;
	}
	Swap(&arr[keyi], &arr[prev]);

	//[left] pivot [right]
	// [left pivot-1]  pivot [pivot+1 right]
	//左子区间和右子区间有序，整体就有序了

	QuickySort(arr, left, keyi - 1);
	QuickySort(arr, keyi + 1, right);

}

void PrintArray(int* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");

}

int main()
{
	int arr[] = { 6,7,9,2,4,3,5,1,0,8,-1 };
	int sz = sizeof(arr) / sizeof(int);

	//快速排序
	QuickySort(arr, 0, sz - 1);
	PrintArray(arr, sz);
}