#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>

//用冒泡思想模拟实现qsort函数
//void Print_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//int cmp_int(const void* p1, const void* p2)
//{
//	return *(int*)p1 - *(int*)p2;
//}
//
//void Swap(char* buf1, char* buf2, size_t width)
//{
//	assert(buf1 && buf2);
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//
//}
//
//void my_qsort(void* base, size_t sz, size_t width, int (*cmp)(const void* p1, const void* p2))
//{
//	//趟数
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//每趟两两之间的比较
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (cmp((char*)base + j * width ,(char*)base + (j + 1) * width) > 0)
//			{
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
//			}
//		}
//	}
//}
//
//void test1()
//{
//	int arr[] = { 2,5,6,9,8,7,4,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	my_qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	Print_arr(arr, sz);
//}
////
//struct Stu
//{
//	char name[20];
//	int age;
//};

//int cmp_name(const void* p1, const void* p2)
//{
//	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
//}
//
//void Print_struct(struct Stu* arr,int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%s %d\n", arr[i].name, arr[i].age);
//	}
//}
//
//void test2()
//{
//	struct Stu arr[3] = { {"zhangsan",38},{"lisi",24},{"wangwu",58}};
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	my_qsort(arr, sz, sizeof(arr[0]), cmp_name);
//	Print_struct(arr, sz);
//}
//
//int main()
//{
//	//test1();//排序整型
//	test2();//排序结构体
//
//	return 0;
//}

//int cmp_char(const void* p1, const void* p2)
//{
//	return *(char*)p1 - *(char*)p2;
//}
//
//void Print_char(char arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%c ", arr[i]);
//	}
//	printf("\n");
//}
//
//void test3()
//{
//	char arr[5] = { 'o','r','w','q','a' };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_char);
//	Print_char(arr, sz);
//}
//int main()
//{
//	test3();
//	return 0;
//}

//int main()
//{
//	printf("hehe\n");
//}

int cmp_int(const void* p1, const void* p2)
{
	//int* e1 = (int*)p1;
	//int* e2 = (int*)p2;
	//if (*e1 > *e1)
	//	return 1;
	//else if (*e1 == *e1)
	//	return 0;
	//else
	//	return -1;

	//上面的代码可简化为：
	return *(int*)p1 - *(int*)p2;
}

void print_int(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void test1()
{
	int arr[] = { 5,4,8,9,6,3,2,1 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	qsort(arr, sz, sizeof(arr[0]), cmp_int);
	print_int(arr, sz);

}
int main()
{
	test1();

	return 0;
}

//int cmp_char(const void* p1, const void* p2)
//{
//	return *(char*)p1 - *(char*)p2;
//}



//int cmp_char(const void* p1, const void* p2)
//{
//	return (*(char*)p1-*(char*)p2);
//}
//
//void print_char(char arr[][20], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%s\n", arr[i]);
//	}
//}
//
//void test2()
//{
//	char arr[5][20] = { "hello world","apple","baana" };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_char);
//	print_char(arr, sz);
//}
//
//int main()
//{
//	test2();
//
//	return 0;
//}

//int cmp_name(const void* p1, const void* p2)
//{
//	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
//}

//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//void print_struct(struct Stu* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%s %d\n", arr[i].name, arr[i].age);
//	}
//}
//
//int cmp_age(const void* p1, const void* p2)
//{
//	return ((struct Stu*)p1)->age - ((struct Stu*)p2)->age;
//}
//
//int main()
//{
//	struct Stu arr[3] = { {"zhangsan",24},{"lisi",18},{"wangwu",49} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_age);
//	print_struct(arr, sz);
//
//	return 0;
//}
//
//void test4()
//{
//	struct Stu arr[3] = { {"zhangsan",24},{"lisi",18},{"wangwu",49} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_name);
//	print_struct(arr, sz);
//}