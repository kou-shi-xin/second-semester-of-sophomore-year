#define _CRT_SECURE_NO_WARNINGS 

#include "SeqList.h"

void SLInit(SL* ps)
{
	ps->arr = (SQDataType*)malloc(sizeof(SQDataType) * 4);
	if (ps->arr == NULL)
	{
		perror("malloc fail!\n");
		return ;
	}

	ps->capacity = 4;
	ps->size = 0;

}


void SLDestory(SL* ps)
{
	free(ps->arr);
	ps->arr = NULL;
	ps->capacity = 0;
	ps->size = 0;
}

void CheckCapacity(SL* ps,SQDataType x)
{
	if (ps->size == ps->capacity)
	{
		SQDataType* tmp = (SQDataType*)realloc(ps->arr, sizeof(SQDataType) * ps->capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc fail!\n");
			return ;
		}
		else
		{
			ps->arr = tmp;
			ps->capacity *= 2;
		}
	}
}

//void SLPrint(SL ps)
//{
//	for (int i = 0; i < ps.size; i++)
//	{
//		printf("%d ", ps.arr[i]);
//	}
//	printf("\n");
//
//}


void SLPushBack(SL* ps, SQDataType x)
{
	assert(ps);
	CheckCapacity(ps, x);

	ps->arr[ps->size] = x;
	ps->size++;
}

void SLPushFront(SL* ps, SQDataType x)
{
	assert(ps);
	CheckCapacity(ps, x);

	int i = ps->size;
	for (i = ps->size; i > 0; i--)
	{
		ps->arr[i] = ps->arr[i - 1];
	}

	ps->arr[i] = x;
	ps->size++;
}

void SLPopBack(SL* ps)
{
	assert(ps);
	if (ps->size == 0)
	{
		printf("无数据可删除！\n");
		return;
	}

	ps->size--;
}

void SLPopFront(SL* ps)
{
	assert(ps);
	if (ps->size == 0)
	{
		printf("无数据可删除！\n");
		return;
	}

	for (int i = 0; i < ps->size; i++)
	{
		ps->arr[i] = ps->arr[i + 1];
	}
	ps->size--;
}


//void SLFind(const SL* ps, SQDataType x)
//{
//	assert(ps);
//	if (ps->size == 0)
//	{
//		printf("无数据！\n");
//		return;
//	}
//
//	for (int i = 0; i < ps->size; i++)
//	{
//		if (ps->arr[i] == x)
//		{
//			printf("找到了\n");
//			return;
//		}
//	}
//
//	printf("找不到\n");
//}


void SLInsert(SL* ps, int pos, SQDataType x)
{
	assert(ps && pos < ps->size && ps->size>0);
	CheckCapacity( ps, x);

	int i = ps->size;
	for ( i = ps->size; i > pos; i--)
	{
		ps->arr[i] = ps->arr[i - 1];
	}
	ps->arr[i] = x;
	ps->size++;
}


void SLErase(SL* ps, int pos)
{
	assert(pos < ps->size && ps->size>0);

	for (int i = pos; i < ps->size; i++)
	{
		ps->arr[i] = ps->arr[i + 1];
	}
	ps->size--;
}