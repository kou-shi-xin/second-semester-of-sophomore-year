#pragma once

#include <stdio.h>
#include <string.h>

#define MAX_NAME 10
#define MAX_GENDER 10
#define MAX_TEL 20
#define MAX_ADDR 20


//定义联系人数据结构:姓名+性别+年龄+电话+地址
typedef struct PeosonInfo
{
	char name[MAX_NAME];
	char gender[MAX_GENDER];
	int age;
	char tel[MAX_TEL];
	char addr[MAX_ADDR];
}peoInfo;

//前置声明
typedef struct SeqList Contact;
//给通讯录改个名字，叫做通讯录
//注意：这里不能写做typedef SL Contact，而是要用原名。
//因为SL是在struct SeqList定义好了之后才重命名的


//要用到顺序表相关的方法，对通讯录的操作实际上就是对顺序表的操作
//有了前置声明，下面的 Contact* con 就是顺序表中的 SL*sl

//初始化通讯录
void InitContact(Contact* con);

//添加通讯录数据
void AddContact(Contact* con);

//删除通讯录数据
void DelContact(Contact* con);

//展示通讯录数据
void ShowContact(const Contact* con);

//查找通讯录数据
void FindContact(const Contact* con);

//修改通讯录数据
void ModifyContact(Contact* con);

//销毁通讯录数据
void DestroyContact(Contact* con);