#define _CRT_SECURE_NO_WARNINGS 


#include "SeqList.h"
#include "Contact.h"

//void TestSeqList()
//{
//	SL sl;
//	SLInit(&sl);
//
//	SLPushBack(&sl, 1);
//	SLPushBack(&sl, 2);
//	SLPushBack(&sl, 3);
//	SLPushBack(&sl, 4);
//	SLPrint(sl);
//
//	SLPushFront(&sl, -3);
//	SLPushFront(&sl, -2);
//	SLPushFront(&sl, -1);
//	SLPushFront(&sl, 0);
//	SLPrint(sl);
//
//	//SLPopBack(&sl);
//	//SLPopBack(&sl);
//	//SLPrint(sl);
//
//	//SLPopFront(&sl);
//	//SLPopFront(&sl);
//	//SLPopFront(&sl);
//	//SLPrint(sl);
//
//	//SLFind(&sl, 3);
//	SLInsert(&sl, 3, 30);
//	SLPrint(sl);
//
//	SLErase(&sl, 3);
//	SLPrint(sl);
//
//	SLDestory(&sl);
//}


//void ContactTest()
//{
//	Contact con;//创建通讯录对象，实际上就是顺序表对象，等价于SL sl
//
//	InitContact(&con);
//	AddContact(&con);
//	AddContact(&con);
//	ShowContact(&con);
//
//	//DelContact(&con);
//	//ShowContact(&con);
//
//	ModifyContact(&con);
//	ShowContact(&con);
//
//	FindContact(&con);
//
//	DestroyContact(&con);
//}

void menu()
{
	printf("**************    通讯录    *************\n");
	printf("******* 1.添加联系人  2.删除联系人 *******\n");
	printf("******* 3.查找联系人  4.修改联系人 *******\n");
	printf("******* 5.展示联系人  0.退出通讯录   *****\n");
	printf("******************************************\n");

}

enum option
{
	Exit,
	addcontact,
	delcontact,
	findcontact,
	modifycontact,
	showcontact
};

int main()
{
	//TestSeqList();
	//ContactTest();

	Contact con;
	InitContact(&con);

	int input = 1;
	do
	{
		menu();
		printf("请选择:");
		scanf("%d", &input);

		switch (input)
		{
		case Exit:
			printf("退出通讯录！\n");
			break;
		case addcontact:
			AddContact(&con);
			break;
		case delcontact:
			DelContact(&con);
			break;
		case findcontact:
			FindContact(&con);
			break;
		case modifycontact:
			ModifyContact(&con);
			break;
		case showcontact:
			ShowContact(&con);
			break;
		default:
			printf("选择错误，请重新选择!\n");
			break;
		}

	} while (input);

	return 0;
}