#define _CRT_SECURE_NO_WARNINGS 1

#include "Contact.h"
#include "SeqList.h"

//初始化通讯录
void InitContact(Contact* con)//就是相当于SL* sl
{
	//实际上就是顺序表的初始化
	SLInit(con);
}

//销毁通讯录数据
void DestroyContact(Contact* con)
{
	SLDestory(con);
}

//添加通讯录数据
void AddContact(Contact* con)
{
	peoInfo info;

	printf("请输入要添加联系人的姓名：\n");
	scanf("%s", info.name);

	printf("请输入要添加联系人的性别：\n");
	scanf("%s", info.gender);

	printf("请输入要添加联系人的年龄：\n");
	scanf("%d", &(info.age));

	printf("请输入要添加联系人的电话：\n");
	scanf("%s", info.tel);

	printf("请输入要添加联系人的地址：\n");
	scanf("%s", info.addr);

	//往通讯录中添加联系人数据
	SLPushBack(con, info);//与顺序表中尾插函数的参数对应
}

//展示通讯录数据
void ShowContact(const Contact* con)
{
	//表头：姓名+性别+年龄+电话+地址
	printf("%-5s %-5s %-5s %-5s %-5s\n", "姓名", "性别", "年龄", "电话", "地址");

	for (int i = 0; i < con->size; i++)
	{
		printf("%-5s %-5s %-5d %-5s %-5s\n",
			    con->arr[i].name,
			    con->arr[i].gender, 
			    con->arr[i].age, 
			    con->arr[i].tel,
				con->arr[i].addr);
	} 
	printf("\n");
}

int FindByname(const Contact* con, char name[])
{
	for (int i = 0; i < con->size; i++)
	{
		if (strcmp(con->arr[i].name, name) == 0)
		{
			//找到了
			return i;
		}
	}
	//没有找到
	return -1;
}


//删除指定位置通讯录数据
void DelContact(Contact* con)
{
	//1.判断要删除的人是否存在
	char name[MAX_NAME];
	printf("请输入要删除人的姓名：\n");
	scanf("%s",name);

	int find = FindByname(con, name);
	if (find < 0)
	{
		printf("要删除的联系人不存在!\n");
		return;
	}

	//2.删除：知道了要删除的联系人数据对应的下标
	SLErase(con, find);
	printf("删除成功！\n");
	printf("\n");
}

//修改通讯录数据
void ModifyContact(Contact* con)
{
	//1.判断要修改的联系人是否存在
	char name[MAX_NAME];
	printf("请输入要修改人的姓名：\n");
	scanf("%s", name);

	int find = FindByname(con, name);
	if (find < 0)
	{
		printf("要修改的联系人不存在!\n");
		return;
	}

	//联系人存在，直接修改指定下标的联系人信息
	printf("请输入新的联系人的姓名：\n");
	scanf("%s", con->arr[find].name);

	printf("请输入新的联系人的性别：\n");
	scanf("%s", con->arr[find].gender);

	printf("请输入新的联系人的年龄：\n");
	scanf("%d", &(con->arr[find].age));

	printf("请输入新的联系人的电话：\n");
	scanf("%s", con->arr[find].tel);

	printf("请输入新的联系人的地址：\n");
	scanf("%s", con->arr[find].addr);

	printf("修改成功!\n");
	printf("\n");

}

//查找通讯录数据
void FindContact(const Contact* con)
{
	char name[MAX_NAME];
	printf("请输入要查找的人的姓名：\n");
	scanf("%s", name);

	int find = FindByname(con, name);
	if (find < 0)
	{
		printf("要查找的人不存在!\n");
		return;
	}

	printf("%-5s %-5s %-5s %-5s %-5s\n", "姓名", "性别", "年龄", "电话", "地址");

		printf("%-5s %-5s %-5d %-5s %-5s\n", 
			con->arr[find].name,
			con->arr[find].gender,
			con->arr[find].age,
			con->arr[find].tel,
			con->arr[find].addr);

		printf("\n");
}