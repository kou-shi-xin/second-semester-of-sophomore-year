#pragma once

#include <stdio.h>
#include  <stdlib.h>
#include <assert.h>
#include "Contact.h"

//typedef int SQDataType;
typedef peoInfo SQDataType;

typedef struct SeqList
{
	SQDataType* arr;
	int size;
	int capacity;
}SL;


void SLInit(SL* ps);

void SLDestory(SL* ps);

//void SLPrint(SL ps);

void SLPushBack(SL* ps, SQDataType x);

void SLPushFront(SL* ps, SQDataType x);

void SLPopBack(SL* ps);

void SLPopFront(SL* ps);

//void SLFind(const SL* ps, SQDataType x);

void SLInsert(SL* ps, int pos, SQDataType x);

void SLErase(SL* ps, int pos);

