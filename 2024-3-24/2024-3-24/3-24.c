#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//void GetMemory(char* p)
//{
//	p = (char*)malloc(100);
//}
//
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(str);
//	strcpy(str, "hello world");
//	printf(str);
//}
//
//int main()
//{
//	Test();
//
//	return 0;
//}

//内存泄漏
//程序崩溃

//修改1
//void GetMemory(char** p)
//{
//	*p = (char*)malloc(100);
//}
//
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(&str);
//	strcpy(str, "hello world");
//	printf(str);
//	free(str);
//	str = NULL;
//}
//
//int main()
//{
//	Test();
//
//	return 0;
//}

//修改2
//char* GetMemory()
//{
//	char* p = (char*)malloc(100);
//	return p;
//}
//
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory();
//	strcpy(str, "hello world");
//	printf(str);
//	free(str);
//	str = NULL;
//}
//
//int main()
//{
//	Test();
//
//	return 0;
//}


//返回栈空间地址的问题

//char* GetMemory(void)
//{
//	char p[] = "hello world";
//	return p;
//}
//
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory();
//	printf(str);
//}
//
//int main()
//{
//	Test();
//
//	return 0;
//}


//没有free释放
//void GetMemory(char** p, int num)
//{
//	*p = (char*)malloc(num);
//}
//
//void Test(void)
//{
//	char* str = NULL;
//	GetMemory(&str, 100);
//	strcpy(str, "hello");
//	printf(str);
//
//	free(str);
//	str = NULL;
//}
//
//int main()
//{
//	Test();
//
//	return 0;
//}

//void Test(void)
//{
//	char* str = (char*)malloc(100);
//	strcpy(str, "hello");
//	free(str);
//	str = NULL;
//
//	if (str != NULL)
//	{
//		strcpy(str, "world");//非法访问内存空间
//		printf(str);
//	}
//}
//
//
//int main()
//{
//	Test();
//
//	return 0;
//}


#include <stdlib.h>

//int main()
//{
//	////情况1
//	//char arr[] = "1234";
//
//	//int num = atoi(arr);
//	//printf("%d\n", num);//1234
//
//	//情况2
//	//char arr[] = "  1234";
//
//	//int num = atoi(arr);
//	//printf("%d\n", num);//1234
//
//	//情况3：含正负号
//	//char arr[] = "  -1234";
//
//	//int num = atoi(arr);
//	//printf("%d\n", num);//-1234
//
//	//情况4：含字母
//	//char arr[] = "  asd1234";
//
//	//int num = atoi(arr);
//	//printf("%d\n", num);//0
//
//	//情况5：正负号后的第一个位置是空格
//	//char arr[] = "   + 1234";
//
//	//int num = atoi(arr);
//	//printf("%d\n", num);//0
//
//	return 0;
//}


//#include <ctype.h>
//
//int my_atoi(const char* str)
//{
//	//跳过前导空格
//	while (isspace(*str++))
//	{
//		;
//	}
//
//	int flag = 0;//判断正负号，默认为正号
//	if (*str == '+')
//	{
//		str++;
//	}
//	else if(*str=='-')
//	{
//		flag = 1;
//		str++;
//	}
//
//	int ret = 0;
//	while (isdigit(*str))
//	{
//		ret *= 10;
//		ret += *str - '0';
//		str++;
//	}
//
//	return flag ? -ret : ret;
//}
//
//int main()
//{
//	char* str = "  a1234";
//	int ret = my_atoi(str);
//	printf("%d\n", ret);
//
//	return 0;
//}


//int main()
//{
//	char* str = "  asd1234";
//	int num = atoi(str);
//	printf("%d\n", num);
//
//	return 0;
//}


//malloc和free
//int main()
//{
//	int* ps = (int*)malloc(sizeof(int) * 5);
//	if (ps == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	//使用
//	for (int i = 0; i < 5; i++)
//	{
//		*(ps + i) = i;
//	}
//
//	//释放
//	free(ps);
//	ps = NULL;
//
//	return 0;
//}

//int main()
//{
//	int* ps = (int*)calloc(5, sizeof(int));
//	if (ps == NULL)
//	{
//		perror("calloc");
//		return 1;
//	}
//	//使用
//	for (int i = 0; i < 5; i++)
//	{
//		*(ps + i) = i;
//	}
//
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", *(ps + i));
//
//	}
//	//释放
//	free(ps);
//	ps = NULL;
//
//	return 0;
//}

//
//int main()
//{
//	int* ps = (int*)malloc(sizeof(int) * 5);
//	if (ps == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	for (int i = 0; i < 5; i++)
//	{
//		*(ps + i) = i + 1;
//	}
//	//扩容
//	int* ptr = (int*)realloc(ps, sizeof(int) * 10);
//	if (ptr == NULL)
//	{
//		perror("realloc");
//		return 1;
//	}
//	else
//	{
//		ps = ptr;
//	}
//
//	for (int i = 5; i < 10; i++)
//	{
//		*(ps + i) = i;
//	}
//
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", *(ps + i));
//	}
//
//	//释放
//	free(ps);
//	ps = NULL;
//
//
//	return 0;
//}
//
//int* getConcatenation(int* nums, int numsSize, int* returnSize) {
//    int* ans = (int*)malloc(sizeof(int) * numsSize * 2);
//    if (ans == NULL)
//    {
//        perror("malloc");
//        return 1;
//    }
//
//    for (int i = 0; i < numsSize; i++)
//    {
//        ans[i] = nums[i];
//        ans[i + numsSize] = nums[i];
//    }
//
//    *returnSize = numsSize * 2;
//
//    return ans;
//}


//int main()
//{
//	int** arr = (int**)malloc(sizeof(int*) * 3);
//	if (arr == NULL)
//	{
//		perror("arr");
//		return 1;
//	}
//
//	for (int i = 0; i < 3; i++)
//	{
//		arr[i] = (int*)malloc(sizeof(int) * 5);
//		if (arr[i] == NULL)
//		{
//			perror("arr[i]");
//			return 1;
//		}
//	}
//	//进行赋值
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			arr[i][j] = 2;
//		}
//	}
//	//打印
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	
//	for (int i = 0; i < 3; i++)
//	{
//		free(arr[i]);
//		arr[i] = NULL;
//	}
//
//	free(arr);
//	arr = NULL;
//	return 0;
//}


//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test.exe", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//写文件
//	//fputc('a', pf);
//	//fputc('b', pf);
//	//fputc('c', pf);
//
//	char ch = 0;
//	for (ch = 'a'; ch <= 'z'; ch++)
//	{
//		fputc(ch, pf);
//	}
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}

//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test.exe", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//读文件
//	char ch = 0;
//	while ((ch = fgetc(pf)) != EOF)
//	{
//		printf("%c ", ch);
//	}
//	printf("\n");
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}

//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test.exe", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//写文件
//	fputs("hello world\n", pf);
//	fputs("hello bit", pf);
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}

//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test.exe", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	char arr[20] = { 0 };
//	while (fgets(arr, 20, pf) != NULL)
//	{
//		printf("%s", arr);
//	}
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}

//使用所学文件操作，在当前目录下放一个文件data.txt，
// 写一个程序，将data.txt文件拷贝一份，生成data_copy.txt文件。

//基本思路：
//打开文件data.txt，读取数据
//打开文件data_copy.txt，写数据
//从data.txt中读取数据存放到data_copy.txt文件中，直到文件结束。
//关闭两个文件


//int main()
//{
//	FILE* pf1 = fopen("data.txt", "r");
//	if (pf1 == NULL)
//	{
//		perror("fopen pf1");
//		return 1;
//	}
//
//	FILE* pf2 = fopen("data_copy.txt", "w");
//	if (pf2 == NULL)
//	{
//		perror("fopen pf2");
//		return 1;
//	}
//	int ch = 0;
//
//	while ((ch = fgetc(pf1)) != EOF)
//	{
//		fputc(ch, pf2);
//	}
//
//	fclose(pf1 && pf2);
//	pf1 = pf2 = NULL;
//
//	return 0;
//}


//#include <ctype.h>
//#include <stdio.h>
//#include <assert.h>
//
//int my_atoi(const char* str)
//{
//	assert(str);
//
//	if (*str == '\0')
//		return 0;
//
//	//跳过前导空格
//	while (isspace(*str))
//	{
//		str++;
//	}
//
//	int flag = 1;//判断正负号，默认为正号
//	if (*str == '+')
//	{
//		flag = 1;
//			str++;
//	}
//	else if (*str == '-')
//	{
//		flag = -1;
//		str++;
//	}
//
//	long long ret = 0;
//	while (*str != '\0')
//	{
//		if (isdigit(*str))
//		{
//			ret = ret * 10 + (*str - '0') * flag;
//
//			//判断要转换的数有没有超过整型的最大值，或是低于整型最小值
//			if (ret > INT_MAX)
//				return INT_MAX;
//			if (ret < INT_MIN)
//				return INT_MIN;
//		}
//		else
//		{
//			return (int)ret;
//		}
//
//		str++;
//	}
//
//	return (int)ret;
//}

//int main()
//{
//	char* str = "-21111111111";
//	int ret = my_atoi(str);
//
//	printf("%d\n", ret);
//
//	return 0;
//}


//int my_atoi(const char* str)
//{
//	assert(str);
//
//	if (*str == '\0')
//		return 0;
//	//跳过前导空格
//	while (isspace(*str++))
//	{
//		;
//	}
//
//	int flag = 0;//判断正负号，默认为正号
//	if (*str == '+')
//	{
//		flag = 0;
//		str++;
//	}
//	else if (*str == '-')
//	{
//		flag = 1;
//		str++;
//	}
//
//	int ret = 0;
//	while (isdigit(*str))
//	{
//		ret *= 10;
//		ret += *str - '0';
//		str++;
//	}
//
//	return flag ? -ret : ret;
//}

//int main()
//{
//	char* str = "a1234";
//	int ret = my_atoi(str);
//	//int ret = atoi(str);
//	printf("%d\n", ret);
//
//	return 0;
//}