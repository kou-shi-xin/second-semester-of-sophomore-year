#define _CRT_SECURE_NO_WARNINGS 

#include "Queue.h"

void QueueInit(Queue* pq)
{
	assert(pq);
	pq->head = pq->tail = NULL;
}

void QueueDestory(Queue* pq)
{
	assert(pq);

	Qnode* cur = pq->head;
	while (cur)
	{
		Qnode* next = cur->next;
		free(cur);
		cur = next;
	}

	pq->head = pq->tail = NULL;
}


//队尾入队列
void QueuePush(Queue* pq, QDataType x)
{
	assert(pq);

	//开辟节点
	Qnode* newnode = (Qnode*)malloc(sizeof(Qnode));
	if (newnode == NULL)
	{
		perror("malloc fail!\n");
		return;
	}
	newnode->data = x;
	newnode->next = NULL;

	//没有一个节点时
	if (pq->head == NULL)
	{
		pq->head = pq->tail = newnode;
	}
	else//多个节点时
	{
		pq->tail->next = newnode;//先把tail链接上新结点
		pq->tail = newnode;//再让tail等于新结点
	}
}

//队头出队列
void QueuePop(Queue* pq)
{
	assert(pq);
	assert(pq->head);

	if (pq->head->next == NULL)
	{
		free(pq->head);
		pq->head = pq->tail = NULL;
	}
	else
	{
		Qnode* next = pq->head->next;
		free(pq->head);
		pq->head = next;
	}
}


//获取队列尾部元素
QDataType QueueBack(Queue* pq)
{
	assert(pq);
	assert(pq->head);

	return pq->tail->data;
}

//获取队列头部元素
QDataType QueueFront(Queue* pq)
{
	assert(pq);
	assert(pq->head);

	return pq->head->data;
}

//获取队列的有效元素个数
int QueueSize(Queue* pq)
{
	if (pq->head == NULL)
	{
		printf("队列为空\n");
	}
	else
	{
		int size = 0;
		Qnode* cur = pq->head;
		while (cur)
		{
			size++;
			cur = cur->next;
		}

		return size;
	}
}

//检查队列是否为空，如果为空返回非零，不为空返回0
bool QueueEmpty(Queue* pq)
{
	assert(pq);

	return pq->head == NULL;
}
