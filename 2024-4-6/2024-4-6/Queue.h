#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int QDataType;

typedef struct Queuenode
{
	struct Queuenode* next;
	QDataType data;
}Qnode;

typedef struct Queue
{
	Qnode* head;
	Qnode* tail;
}Queue;

void QueueInit(Queue* pq);

void QueueDestory(Queue* pq);

//队尾入队列
void QueuePush(Queue* pq, QDataType x);

//队头出队列
void QueuePop(Queue* pq);

//获取队列尾部元素
QDataType QueueBack(Queue* pq);

//获取队列头部元素
QDataType QueueFront(Queue* pq);

//获取队列的有效元素个数
int QueueSize(Queue* pq);

//检查队列是否为空，如果为空返回非零，不为空返回0
bool QueueEmpty(Queue* pq);

