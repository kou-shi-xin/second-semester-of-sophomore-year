#define _CRT_SECURE_NO_WARNINGS 

#include "SeqList.h"

void SeqListTest()
{
	SL sl;
	SeqListInit(&sl);

	SeqListPushBack(&sl, 8);
	SeqListPushBack(&sl, 9);
	SeqListPushBack(&sl, 10);
	SeqListPrint(&sl);

	SeqListModify(&sl, 1, 20);
	SeqListPrint(&sl);

}

int main()
{

	SeqListTest();

	return 0;
}