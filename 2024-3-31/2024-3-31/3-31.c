#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
//#include <stdlib.h>

//写一个宏，交换一个整数二进制的奇偶位
//#define SWAP(n)  ((((n) & 0x55555555) << 1 )|((((n) & 0xaaaaaaaa) >> 1 )))
//
//int main()
//{
//	printf("%d\n", SWAP(13));
//
//	return 0;
//}


//写一个宏，实现offsetof
//#define OFFSETOF(type,member)   (size_t)&(((type*)0)->member)
//
//struct A
//{
//	char a;
//	int b;
//	short c;
//};
//
//int main()
//{
//	struct A a;
//
//	printf("%zd\n", OFFSETOF(struct A, a));
//	printf("%zd\n", OFFSETOF(struct A, b));
//	printf("%zd\n", OFFSETOF(struct A, c));
//
//	return 0;
//}