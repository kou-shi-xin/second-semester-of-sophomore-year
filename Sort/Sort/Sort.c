#define _CRT_SECURE_NO_WARNINGS 

#include "Sort.h"

typedef int STQType;
typedef struct Stack
{
	STQType* a;
	int top;
	int capacity;
}ST;

void STInit(ST* ps)
{
	ps->a = (STQType*)malloc(sizeof(STQType) * 4);
	ps->top = 0;//指向栈顶元素的下一个位置
	ps->capacity = 4;
}

void STDestroy(ST* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->top = 0;
	ps->capacity = 0;
}
//栈顶插入元素
void STPush(ST* ps, STQType x)
{
	assert(ps);
	//检查扩容
	if (ps->top == ps->capacity)
	{
		ps->a = (STQType*)realloc(ps->a, sizeof(STQType) * ps->capacity * 2);
		if (ps->a == NULL)
		{
			perror("realloc fail");
			return;
		}
		ps->capacity *= 2;
	}

	ps->a[ps->top++] = x;
}
//获取栈顶元素
STQType STtop(ST* ps)
{
	assert(ps && ps->top > 0);
	return ps->a[ps->top - 1];
}
//删除栈顶元素
void STPop(ST* ps)
{
	assert(ps && ps->top > 0);
	ps->top--;
}
//判断栈是否为空，为空返回非零，不为空返回0
bool STEmpty(ST* ps)
{
	assert(ps);
	return ps->top == 0;
}

void Print(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

//时间复杂度：O(N^2)
//最好：顺序有序O(N)
//最坏：逆序O(N)
void InsertSort(int* arr, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int end = i;
		int tmp = arr[end + 1];

		while (end >= 0)
		{
			if (tmp < arr[end])
			{
				arr[end + 1] = arr[end];
				end--;
			}
			else
			{
				break;
			}
		}
		arr[end + 1] = tmp;
	}
}

//时间复杂度：O(N^2)
//最坏：O(N^2)
//最好：有序或接近有序O(N)
void BubbleSort(int* arr, int n)
{
	for (int j = 0; j < n - 1; j++)
	{
		int exchange = 0;

		for (int i = 1; i < n-j; i++)
		{
			if (arr[i - 1] > arr[i])
			{
				Swap(&arr[i - 1], &arr[i]);
				exchange = 1;//发生了交换
			}
		}
		if (exchange == 0)
			break;
	}
}

//时间复杂度O(N^1.3)
void ShellSort(int* arr, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap /= 2;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = arr[end + gap];
			while (end >= 0)
			{
				if (tmp < arr[end] )
				{
					arr[end + gap] = arr[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			arr[end + gap] = tmp;
		}
	}
}

//时间复杂度：O(N^2)
void SelectSort(int* arr, int n)
{
	//区间变量
	int begin = 0;
	int end = n - 1;

	//最大值和最小值的下标位置
	int maxi = begin;
	int mini = end;

	//确定最大值和最小值的位置
	while (begin < end)
	{
		for (int i = begin; i <=end; i++)
		{
			if (arr[i] > arr[maxi])
			{
				maxi = i;
			}

			if (arr[i] < arr[mini])
			{
				mini = i;
			}
		}
		Swap(&arr[begin], &arr[mini]);

		if (begin == maxi)
			maxi = mini;

		Swap(&arr[end], &arr[maxi]);

		begin++;
		end--;
	}
}

//三数取中
int GetMid(int* arr, int left, int right)
{
	int mid = (left + right) >> 1;

	if (arr[mid] > arr[left])
	{
		if (arr[left] > arr[right])
		{
			return left;
		}
		else if (arr[mid] < arr[right])
		{
			return mid;
		}
		else
		{
			return right;
		}
	}
	else  //arr[mid] < arr[left]
	{
		if (arr[mid] > arr[right])
		{
			return mid;
		}
		else if (arr[left] < arr[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
}

//时间复杂度：O(N*logN)
//最坏情况：有序或接近有序 O(N^2)
void QuickSort1(int* arr, int left, int right)
{
	//区间只有一个值或者不存在就结束
	if (left >= right)
		return;
	
	int begin = left;
	int end = right;

	//选区间中的随机位置做keyi
	//int randi = rand() % (right - left);
	//randi += left;//在递归过程中，left不一定从0开始

	int midi = GetMid(arr, left, right);

	Swap(&arr[left], &arr[midi]);//还是保证最左边的值做keyi
	
	int keyi = left;//基准位置
	while (left < right)
	{
		//右边找小
		while (left < right && arr[right] >= arr[keyi])
		{
			right--;
		}

		//左边找大
		while (left < right && arr[left] <= arr[keyi])
		{
			left++;
		}

		Swap(&arr[left], &arr[right]);
	}

	Swap(&arr[left], &arr[keyi]);
	keyi = left; // 增加可读性

	//[begin,keyi-1] keyi [keyi+1,end]
	QuickSort1(arr, begin, keyi - 1);
	QuickSort1(arr, keyi + 1, end);
}

void QuickSort2(int* arr, int left, int right)
{
	if (left >= right)
		return;

	int prev = left;
	int cur = left + 1;
	int keyi = left;

	while (cur <= right)
	{
		if (arr[cur] < arr[keyi] && ++prev != cur)
			Swap(&arr[cur], &arr[prev]);
		
		++cur;
	}

	Swap(&arr[prev], &arr[keyi]);
	keyi = prev;

	//[left,keyi-1] keyi [keyi+1,right]
	QuickSort2(arr, left, keyi - 1);
	QuickSort2(arr, keyi + 1, right);
}

//向下调整算法
void AdjustDown(int* arr, int n , int parent)
{
	int child = parent * 2 + 1;
	
	while (child < n)
	{
		if (child + 1 < n && arr[child + 1] > arr[child])
		{
			child += 1;
		}

		if(arr[child] > arr[parent])
		{
			Swap(&arr[child], &arr[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapSort(int* arr, int n)
{
	//建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(arr, n, i);
	}

	//排序
	int end = n - 1;
	while (end >= 0)
	{
		Swap(&arr[end], &arr[0]);
		AdjustDown(arr, end, 0);
		end--;
	}
}

void QuickSortNonR(int* arr, int left, int right)
{
	ST st;
	STInit(&st);

	//右左区间入栈
	STPush(&st, right);
	STPush(&st, left);

	while (!STEmpty(&st))
	{
		int begin = STtop(&st);
		STPop(&st);

		int end = STtop(&st);
		STPop(&st);

		//单趟
		int keyi = begin;
		int prev = begin;
		int cur = begin + 1;

		while (cur <= end)
		{
			if (arr[cur] < arr[keyi] && cur != ++prev)
				Swap(&arr[cur], &arr[prev]);

			cur++;
		}
		Swap(&arr[prev], &arr[keyi]);
		keyi = prev;

		// [begin,keyi-1] keyi [keyi+1,end]

		if (keyi + 1 < end)
		{
			STPush(&st, end);
			STPush(&st, keyi + 1);
		}

		if (begin < keyi - 1)
		{
			STPush(&st, keyi - 1);
			STPush(&st, begin);
		}
	}

	STDestroy(&st);
}

void _MergeSort(int* arr, int begin, int end, int* tmp)
{
	//当区间内只有一个元素时，认为有序，递归结束，开始归并
	if (begin == end)
		return;

	int mid = (begin + end) >> 1;

	//[begin,mid] [mid+1,end]
	_MergeSort(arr, begin, mid, tmp);
	_MergeSort(arr, mid + 1, end, tmp);

	//归并过程，取小的尾插到临时数组
	int begin1 = begin, end1 = mid; //左区间
	int begin2 = mid + 1, end2 = end; //右区间
	int i = begin;

	while (begin1 <= end1 && begin2 <= end2)
	{
		if(arr[begin1] < arr[begin2])
		{
			tmp[i++] = arr[begin1++];
		}
		else
		{
			tmp[i++] = arr[begin2++];
		}
	}

	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}

	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}

	memcpy(arr + begin, tmp + begin, sizeof(int) * (end - begin + 1));
}

void MergeSort(int* arr, int n)
{
	//申请临时数组
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail");
		return;
	}

	_MergeSort(arr, 0, n - 1, tmp);

	free(tmp);
	tmp = NULL;
}

void MergeSortNonR(int* arr, int n)
{

}

void CountSort(int* arr, int n)
{
	int max = arr[0];
	int min = arr[0];

	for (int i = 0; i < n; i++)
	{
		if (arr[i] > max)
			max = arr[i];

		if (arr[i] < min)
			min = arr[i];
	}

	int range = max - min + 1;

	int* count = (int*)calloc(range, sizeof(int));
	if (count == NULL)
	{
		perror("calloc fail");
		return;
	}

	//统计个数
	for (int j = 0; j < n; j++)
	{
		count[arr[j] - min]++;
	}

	//排序
	int j = 0;
	for (int i = 0; i < range; i++)
	{
		while (count[i]--)
		{
			arr[j++] = i + min;
		}
	}
	
	free(count);
	count = NULL;
}