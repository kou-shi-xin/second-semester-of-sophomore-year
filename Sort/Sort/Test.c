#define _CRT_SECURE_NO_WARNINGS 

#include "Sort.h"

void TestInsertSort()
{
	int arr[] = { 5,3,9,6,2,4,7,1,8 };
	int sz = sizeof(arr) / sizeof(int);

	InsertSort(arr, sz);

	Print(arr, sz);
}

void TestBubbleSort()
{
	int arr[] = { 5,3,9,6,2,4,7,1,8 };
	int sz = sizeof(arr) / sizeof(int);

	BubbleSort(arr, sz);

	Print(arr, sz);
}

void TestShellSort()
{
	int arr[] = { 5,3,9,6,2,-1,4,7,1,0,8 };
	int sz = sizeof(arr) / sizeof(int);

	ShellSort(arr, sz);

	Print(arr, sz);
}

void TestSelectSort()
{
	int arr[] = { 5,3,9,6,2,-1,4,7,1,0,8 };
	int sz = sizeof(arr) / sizeof(int);

	SelectSort(arr, sz);

	Print(arr, sz);
}

void TestQuickSort1()
{
	int arr[] = { 5,3,9,6,2,-1,4,7,1,0,8 };
	int sz = sizeof(arr) / sizeof(int);

	QuickSort1(arr, 0, sz - 1);

	Print(arr, sz);
}

void TestQuickSort2()
{
	int arr[] = { 5,3,9,6,2,-1,4,7,1,0,8 };
	int sz = sizeof(arr) / sizeof(int);

	QuickSort2(arr, 0, sz - 1);

	Print(arr, sz);
}

void TestHeapSort()
{
	int arr[] = { 5,3,9,6,2,-1,4,7,1,0,8 };
	int sz = sizeof(arr) / sizeof(int);

	HeapSort(arr, sz);

	Print(arr, sz);
}

void QuickSortNonR_Test()
{
	int arr[] = { 5,3,9,6,2,-1,4,7,1,0,8 };
	int sz = sizeof(arr) / sizeof(int);

	QuickSortNonR(arr, 0, sz - 1);

	Print(arr, sz);
}

void TestMergeSort()
{
	int arr[] = { 5,3,9,6,2,-1,4,7,1,0,8 };
	int sz = sizeof(arr) / sizeof(int);

	MergeSort(arr, sz);

	Print(arr, sz);
}

void TestCountSort()
{
	int arr[] = { 5,0,4,5,-1,-2,-1,6,8,0,4,9,9,4 };
	int sz = sizeof(arr) / sizeof(int);

	CountSort(arr, sz);

	Print(arr, sz);
}

int main()
{
	srand((unsigned int)time(NULL));

	TestCountSort();

	return 0;
}