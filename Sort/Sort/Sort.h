#pragma once

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

void Print(int* arr, int n);

void Swap(int* p1, int* p2);

void InsertSort(int* arr, int n);

void BubbleSort(int* arr, int n);

void ShellSort(int* arr, int n);

void SelectSort(int* arr, int n);

void HeapSort(int* arr, int n);

//Hoare法
void QuickSort1(int* arr, int left, int right);

//前后指针法
void QuickSort2(int* arr, int left, int right);

//快排非递归
void QuickSortNonR(int* arr, int left, int right);

void MergeSort(int* arr, int n);

//归并非递归
void MergeSortNonR(int* arr, int n);

void CountSort(int* arr, int n);