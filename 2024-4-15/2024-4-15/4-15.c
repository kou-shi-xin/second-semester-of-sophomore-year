#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

//int main()
//{
//	printf("%s\n", __FILE__);
//	printf("%d\n", __LINE__);
//	printf("%s\n", __DATE__);
//	printf("%s\n", __TIME__);
//
//	return 0;
//}


//#define  MAX 10;
//
//int main()
//{
//	printf("%d\n", MAX);
//
//	return 0;
//}


//int main()
//{
//	printf("hello""world\n");
//	printf("helloworld\n");
//
//	return 0;
//}

//#define Print(n,format)    printf("the value of " #n " is "format"\n",n);
//
//int main()
//{
//	int a = 1;
//	Print(a, "%d");
//	//printf("the value of a is %d\n", a);
//
//	int b = 20;
//	Print(b, "%d");
//	//printf("the value of b is %d\n", b);
//
//	float f = 5.6f;
//	Print(f, "%f");
//	//printf("the value of f is %f\n", f);
//
//	return 0;
//}


//宏定义 
//#define GENERIC_MAX(type) \
//type type##_max(type x, type y)\
//{ \
// return (x>y?x:y); \
//}
//
//GENERIC_MAX(int)  //替换到宏体内后int##_max ?成了新的符号 int_max做函数名 
//GENERIC_MAX(float)  //替换到宏体内后float##_max ?成了新的符号 float_max做函数名 
//
//int main()
//{
//	//调?函数 
//	int m = int_max(2, 3);
//	printf("%d\n", m);
//	float fm = float_max(3.5f, 4.5f);
//	printf("%f\n", fm);
//
//	return 0;
//
//}


//#define MAX 100
//
//int main()
//{
//	printf("%d\n", MAX);
//#undef  MAX
//	printf("%d\n", MAX);
//
//	return 0;
//}

//#define M 10
//
//int main()
//{
//#if M>0
//	printf("hehe\n");
//#endif
//
//	return 0;
//}


#define M 1

int main()
{
#if M==0
	printf("haha\n");
#elif M==1
	printf("hehe\n");
#elif M==2
	printf("heihei");
#else M==3
	printf("ok\n");
#endif

	return 0;
}