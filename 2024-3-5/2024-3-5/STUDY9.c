#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//
////使用者自己定义的比较整型的函数
//int cmp_int(const void* p1, const void* p2)
//{
//	return *(int*)p1 - *(int*)p2;
//}
//
//void print_int(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//void test1()
//{
//	int arr[] = { 5,4,8,9,6,3,2,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	print_int(arr, sz);
//
//}
//
//int main()
//{
//	test1();
//
//	return 0;
//}

//
//int cmp_char(const void* p1, const void* p2)
//{
//	return *(char*)p1 - *(char*)p2;
//}
//
//void Print_char(char arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%c ", arr[i]);
//	}
//	printf("\n");
//}
//
//void test2()
//{
//	char arr[] = { 'd','r','a','w' };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_char);
//	Print_char(arr, sz);
//}
//
//int main()
//{
//	test2();
//
//	return 0;
//}

//
//int cmp_chars(const void* p1, const void* p2)
//{
//	return (*(char*)p1 - *(char*)p2);
//}
//
//void print_chars(char arr[][20], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%s\n", arr[i]);
//	}
//}
//
//void test3()
//{
//	char arr[5][20] = { "hello world","apple","banana" };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_chars);
//	print_chars(arr, sz);
//}
//
//int main()
//{
//	test3();
//
//	return 0;
//}


//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//void print_struct(struct Stu* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%s %d\n", arr[i].name, arr[i].age);
//	}
//}
//
//int cmp_name(const void* p1, const void* p2)
//{
//	//比较两个字符串，用strcmp函数
//	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
//}
//
//void test4()
//{
//	struct Stu arr[3] = { {"zhangsan",24},{"lisi",18},{"wangwu",49} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_name);
//	print_struct(arr, sz);
//}
//
//int main()
//{
//	test4();
//
//	return 0;
//}


//struct Stu
//{
//	char name[20];
//	int age;
//};
//
//void print_struct(struct Stu* arr, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%s %d\n", arr[i].name, arr[i].age);
//	}
//}
//
//int cmp_age(const void* p1, const void* p2)
//{
//	return ((struct Stu*)p1)->age - ((struct Stu*)p2)->age;
//}
//
//void test5()
//{
//	struct Stu arr[3] = { {"zhangsan",24},{"lisi",18},{"wangwu",49} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_age);
//	print_struct(arr, sz);
//}
//
//	int main()
//	{
//		test5();
//
//		return 0;
//	}

//
//void Swap(char* buf1, char* buf2, size_t width)
//{
//	assert(buf1 && buf2);
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//
//}
//
//void my_qsort(void* base, size_t sz, size_t width, int (*cmp)(const void* p1, const void* p2))
//{
//	//趟数
//		int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//每趟两两之间的比较
//			int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
//			{
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
//			}
//		}
//	}
//}
//
//void Print_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//int cmp_int(const void* p1, const void* p2)
//{
//	return *(int*)p1 - *(int*)p2;
//}
//
//void test1()
//{
//	int arr[] = { 2,5,6,9,8,7,4,1 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	my_qsort(arr, sz, sizeof(arr[0]), cmp_int);
//	Print_arr(arr, sz);
//}
//
//int main()
//{
//	test1();//排序整型
//
//	return 0;
//}



//int main() {
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);//2  3
//    int arr[10][10] = { 0 };
//    int i = 0;
//    int j = 0;
//    for (i = 0; i < n; i++)
//    {
//        for (j = 0; j < m; j++)
//        {
//            scanf("%d", &arr[i][j]);
//        }
//    }
//
//    for (i = 0; i < m; i++)//3
//    {
//        for (j = 0; j < n; j++)//2
//        {
//            printf("%d ", arr[j][i]);
//        }
//        printf("\n");
//    }
//
//    return 0;
//}

//int main() {
//    int a = 0;
//    while (scanf("%d", &a) != EOF) { // 注意 while 处理多个 case
//        int i = 0;
//        int j = 0;
//        for (i = 0; i < a; i++)
//        {
//            for (j = 0; j < a; j++)
//            {
//                if ((i == 0) || (i == a - 1) || (j == 0) || (j == a - 1))
//                    printf("* ");
//                else
//                    printf("  ");
//            }
//            printf("\n");
//        }
//    }
//    return 0;
//}


//#include <stdio.h>
//#include <string.h>
//
//void Reverse(int arr[], int left, int right)
//{
//    while (left < right)
//    {
//        int tmp = arr[left];
//        arr[left] = arr[right];
//        arr[right] = tmp;
//        left++;
//        right--;
//    }
//}
//
//void print_arr(int arr[], int k)
//{
//    int i = 0;
//    for (i = 0; i < k; i++)
//    {
//        printf("%d ", arr[i]);
//    }
//}
//
//int main() {
//    int arr[20] = { 0 };
//    int i = 0;
//    for (i = 0; i < 10; i++)
//    {
//        scanf("%d", &arr[i]);
//    }
//   // int sz = sizeof(arr) / sizeof(arr[0]);
//    //int len = strlen(arr);
//    int left = 0;
//    int right = 9;
//    Reverse(arr, left, right);
//    print_arr(arr, 10);
//
//    return 0;
//}


//#include <stdio.h>

//int main()
//{
//	int n = 0;
//	int m = 0;
//	int arr1[1000] = { 0 };
//	int arr2[1000] = { 0 };
//	scanf("%d %d", &n, &m);
//	//输入
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", &arr1[i]);
//	}
//	for (int i = 0; i < m; i++)
//	{
//		scanf("%d", &arr2[i]);
//	}
//	//实现合并
//	int i = 0;
//	int j = 0;
//	while (i < n && j<m)
//	{
//		if (arr1[i] < arr2[j])
//		{
//			printf("%d ", arr1[i]);
//			i++;
//		}
//		else
//		{
//			printf("%d ", arr2[j]);
//			j++;
//		}
//	}
//
//	while (j < m)
//	{
//		printf("%d ", arr2[j]);
//		j++;
//	}
//
//	while (i < n)
//	{
//		printf("%d ", arr1[i]);
//		i++;
//	}
//
//	return 0;
//}


//int main() {
//    int n = 0;
//    int m = 0;
//    scanf("%d %d", &n, &m);
//    int arr1[1000] = { 0 };
//    int arr2[1000] = { 0 };
//    int i = 0;
//    for (i = 0; i < n; i++)
//    {
//        scanf("%d", &arr1[i]);
//    }
//    int j = 0;
//    for (j = 0; j < m; j++)
//    {
//        scanf("%d", &arr2[j]);
//    }
//    //合并数组
//    while (i < n && j < m)
//    {
//        if (arr1[i] < arr2[j])
//        {
//            printf("%d ", arr1[i]);
//            i++;
//        }
//        else {
//            printf("%d ", arr2[j]);
//            j++;
//        }
//    }
//    while (j < m)
//    {
//        printf("%d ", arr2[j]);
//        j++;
//    }
//    while (i < n)
//    {
//        printf("%d ", arr1[i]);
//        i++;
//    }
//    return 0;
//}


//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	int sum = 0;
//	float aver = 0;
//	for (i = 0; i < 10; i++)
//	{
//		scanf("%d", &arr[i]);
//		sum += arr[i];
//	}
//	aver = sum / 10.0;
//	printf("%f", aver);
//
//	return 0;
//}

//void Reverse(int* arr1, int* arr2, int p1, int p2 ,int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		int tmp = arr1[p1];
//		arr1[p1] = arr2[p2];
//		arr2[p2] = tmp;
//		p1++;
//		p2++;
//	}
//}
//
//void Print(int* arr1, int* arr2, int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//	printf("\n");
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//}
//int main()
//{
//	int arr1[5] = { 0 };
//	int arr2[5] = { 0 };
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	for (int i = 0; i < sz; i++)
//	{
//		scanf("%d", &arr1[i]);
//	}
//	for (int i = 0; i < sz; i++)
//	{
//		scanf("%d", &arr2[i]);
//	}
//	int p1 = 0;
//	int p2 = 0;
//	Reverse(arr1, arr2, p1, p2, sz);
//	Print(arr1, arr2, sz);
//
//	return 0;
//}

//int bin_search(int arr[], int left, int right, int key)
//{
//	while (left <= right)
//	{
//		int mid = left + (right - left) / 2;
//		if (arr[mid] < key)
//		{
//			left = mid + 1;
//		}
//		else if (arr[mid] > key)
//		{
//			right = mid - 1;
//		}
//		else
//			return mid;
//	}
//	return -1;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9 };
//	int k = 7;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;
//	int right = sz - 1;
//	int ret = bin_search(arr, left, right, k);
//	if (ret == -1)
//	{
//		printf("找不到了\n");
//	}
//	else
//		printf("下标是：%d\n", ret);
//
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	for (int i = 1; i <= n; i++)
//	{
//		for (int j = 1; j <= i; j++)
//		{
//			printf("%d*%d=%-5d", j, i, j * i);
//		}
//		printf("\n");
//	}
//
//	return 0;
//}

//void Is_leap(int year)
//{
//	if (((year % 4 == 0) && (year % 100 != 0)) || (year % 100 == 0))
//	{
//		printf("是闰年\n");
//	}
//	else
//	{
//		printf("不是闰年\n");
//	}
//}
//
//int main()
//{
//	int year = 0;
//	while (scanf("%d", &year) != EOF)
//	{
//		Is_leap(year);
//	}
//	return 0;
//}

//int Is_prime(int k)
//{
//	for (int i = 2; i < k; i++)
//	{
//		if (k % i == 0)
//		{
//			return 0;
//			break;
//		}
//	}
//	return k;
//}
//int main()
//{
//	for (int i = 100; i <= 200; i++)
//	{
//		int k = i;
//		int ret = Is_prime(k);
//		if (ret)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}


//实现函数init() 初始化数组为全0
//实现print()  打印数组的每个元素
//实现reverse()  函数完成数组元素的逆置。

//void Print(int arr[], int sz)
//{
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//void Init(int arr[], int sz)
//{
//	int i = 0;
//	for ( i = 0; i < sz; i++)
//	{
//		arr[i] = 0;
//	}
//}
//
//void Reverse(int arr[], int left, int right)
//{
//    while (left < right)
//    {
//        int tmp = arr[left];
//        arr[left] = arr[right];
//        arr[right] = tmp;
//        left++;
//        right--;
//    }
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("逆置前：\n");
//	Print(arr, sz);
//	int left = 0;
//	int right = sz - 1;
//	Reverse(arr, left, right);
//	printf("逆置后：\n");
//	Print(arr, sz);
//	//把数组置为0
//	Init(arr, sz);
//	printf("置0后：\n");
//	Print(arr, sz);
//
//	return 0;
//}