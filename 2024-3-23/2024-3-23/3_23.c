#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <assert.h>
#include <string.h>

//模拟实现memcpy

//void* my_memcpy(void* dest,const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//
//	return ret;
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7 };
//	int arr2[20] = { 0 };
//
//	my_memcpy(arr2, arr1, 5 * sizeof(int));
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	printf("\n");
//
//	return 0;
//}


//模拟实现memmove

//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	void* ret = dest;
//
//	while (num--)
//	{
//		if (dest < src)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//
//		}
//		else
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//
//	return dest;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//
//	my_memmove(arr, arr + 3, sizeof(int) * 4);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//
//	return 0;
//}


//写一个函数判断当前机器是大端还是小端，如果是小端返回1，如果是大端返回0.

//int Check_sys()
//{
//	int n = 1;
//
//	int a = *(char*)&n;
//	if (a == 1)
//		return 1;
//	else
//		return 0;
//}
//
//int main()
//{
//
//	int ret = Check_sys();
//	if (ret)
//		printf("小端\n");
//	else
//		printf("大端\n");
//
//	return 0;
//}


//int main()
//{
//	char arr1[20] = "abcd";
//	char arr2[] = "vfg";
//
//	char* ret = strncpy(arr1, arr2, 1);
//	//strcmp(arr1, arr2);
//	printf("%s\n", arr1);
//
//	return 0;
//}

//char* my_strncpy(char* dest, const char* src, size_t n)
//{
//	assert(dest && src);
//
//	char* ret = dest;
//
//	while (n--)
//	{
//		*dest++ = *src++;
//	}
//	*dest = '\0';
//
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "abcddef";
//	char arr2[] = "gbnh";
//
//	char* ret = my_strncpy(arr1, arr2, 5);
//	printf("%s\n", ret);
//
//	return 0;
//}

//char* my_strncat(char* dest, const char* src, size_t n)
//{
//	assert(dest && src);
//	char* ret = dest;
//
//	while (*dest!='\0')
//	{
//		dest++;
//	}
//
//	while (n--)
//	{
//		*dest++ = *src++;
//		
//	}
//
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "hello ";
//	char arr2[] = "world";
//
//	char* ret = my_strncat(arr1, arr2, 3);
//	printf("%s\n", ret);
//
//	return 0;
//}



//int Check_sys()
//{
//	union Un
//	{
//		char a;
//		int i;
//	}u;
//	u.i = 1;
//
//	return u.a;
//}
//
//int main()
//{
//	int ret = Check_sys();
//	if (ret)
//		printf("小端\n");
//	else
//		printf("大端\n");
//
//	return 0;
//}

//#include <math.h>
//
//int main()
//{
//
//
//    for (int i = 10000; i < 99999; i++)
//    {
//        int sum = 0;
//        int n = 1;
//
//        while (n <= 4)
//        {
//
//            int mul = 0;
//            int k = i;
//            int c = (int)pow(10, n);
//            int a = k % (int)pow(10, n);
//            int b = k / (int)pow(10, n);
//            n++;
//            mul = a * b;
//            sum += mul;
//
//        }
//
//        if (sum == i)
//            printf("%d ", i);
//    }
//
//    return 0;
//}


