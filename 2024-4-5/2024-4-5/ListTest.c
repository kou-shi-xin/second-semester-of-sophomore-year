#define _CRT_SECURE_NO_WARNINGS 

#include "List.h"

void TestList()
{
	LNode* plist = ListInit();

	ListPushBack(plist, 1);
	ListPushBack(plist, 2);
	ListPushBack(plist, 3);
	ListPushBack(plist, 4);
	ListPrint(plist);

	ListPushFront(plist, 0);
	ListPrint(plist);

	//ListPopBack(plist);
	//ListPrint(plist);

	ListPopFront(plist);
	ListPrint(plist);

	LNode* pos = ListFind(plist, 4);
	if (pos)
	{
		//printf("�ҵ���\n");
        //ListInsert(pos, 20);
		//ListErase(pos);
		//ListPrint(plist);

	}

	ListDestory(plist);
}

int main()
{
	TestList();

	return 0;
}