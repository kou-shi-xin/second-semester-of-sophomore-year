#define _CRT_SECURE_NO_WARNINGS 

#include "SList.h"

void SListTest()
{
	SLNode* plist = NULL;

	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPrint(plist);

	SListPushFront(&plist, 0);
	SListPrint(plist);

	/*SListPopFront(&plist);
	SListPrint(plist);*/

	SLNode* pos = SListFind(plist, 4);
	if (pos != NULL)
	{
		//SListInsert(&plist, pos, 20);
		SListErase(&plist, pos);

	}
	SListPrint(plist);


}

int main1()
{
	SListTest();

	return 0;
}