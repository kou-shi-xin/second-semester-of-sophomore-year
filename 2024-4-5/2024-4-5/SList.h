#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SLDataType;

typedef struct SListNode
{
	SLDataType data;
	struct SListNode* next;
}SLNode;


void SListPushBack(SLNode** pphead, SLDataType x);
void SListPushFront(SLNode** pphead, SLDataType x);
void SListPrint(SLNode* phead);

void SListPophBack(SLNode** pphead); 
void SListPopFront(SLNode** pphead);

SLNode* SListFind(SLNode* phead, SLDataType x);

void SListInsert(SLNode** pphead, SLNode* pos, SLDataType x);
void SListErase(SLNode** pphead, SLNode* pos);


