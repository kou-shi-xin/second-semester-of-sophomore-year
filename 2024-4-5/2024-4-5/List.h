#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int LDataType;

typedef struct ListNode
{
	LDataType data;
	struct ListNode* next;
	struct ListNode* prev;
}LNode;

LNode* ListInit();

void ListDestory(LNode* phead);

void ListPrint(LNode* phead);

void ListPushBack(LNode* phead, LDataType x);

void ListPushFront(LNode* phead, LDataType x);

void ListPopBack(LNode* phead);

void ListPopFront(LNode* phead);


LNode* ListFind(LNode* phead, LDataType x);

void ListInsert(LNode* pos, LDataType x);

void ListErase(LNode* pos);
