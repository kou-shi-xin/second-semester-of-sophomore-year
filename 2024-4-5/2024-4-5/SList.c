#define _CRT_SECURE_NO_WARNINGS 

#include "SList.h"

void SListPrint(SLNode* phead)
{
	SLNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");

}

//SLNode* BuyNode(SLDataType x)
//{
//	SLNode* newnode = (SLNode*)malloc(sizeof(SLNode));
//	if (newnode == NULL)
//	{
//		perror("malloc fail!\n");
//		return;
//	}
//
//	newnode->next = NULL;
//	newnode->data = x;
//
//	return newnode;
//}

void SListPushBack(SLNode** pphead, SLDataType x)
{
	SLNode* newnode = BuyNode(x);

	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		SLNode* tail = *pphead;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}

		tail->next = newnode;
	}
}


void SListPushFront(SLNode** pphead, SLDataType x)
{
	SLNode* newnode = BuyNode(x);

	SLNode* cur = *pphead;
	*pphead = newnode;;
	newnode->next = cur;
}

void SListPophBack(SLNode** pphead)
{
	if (*pphead == NULL)
	{
		printf("链表内无数据删除！\n");
		return;
	}
	//一个节点
	if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else//多个节点
	{
		SLNode* tail = *pphead;
		SLNode* prev = NULL;
		while (tail->next != NULL)
		{
			prev = tail;
			tail = tail->next;
		}

		free(tail);
		prev->next = NULL;
	}
}


void SListPopFront(SLNode** pphead)
{
		SLNode* next = (*pphead)->next;
		free(*pphead);
		*pphead = next;
	
}

SLNode* SListFind(SLNode* phead, SLDataType x)
{
	SLNode* cur = phead;
	while (cur)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

void SListInsert(SLNode** pphead, SLNode* pos, SLDataType x)
{
	SLNode* newnode = BuyNode(x);

	if (*pphead == pos)
	{
		SListPushFront(pphead, x);
	}
	else
	{
		SLNode* prev = NULL;
		SLNode* tail = *pphead;

		while (tail->next != pos)
		{
			tail = tail->next;
		}

		prev = tail;
		prev->next = newnode;
		newnode->next = pos;
	}
	
}

void SListErase(SLNode** pphead, SLNode* pos)
{
	if (pos == *pphead)
	{
		SListPopFront(pphead);
	}
	else
	{
		SLNode* cur = *pphead;
		while (cur->next != pos)
		{
			cur = cur->next;
		}

		cur->next = pos->next;
		free(pos);

	}
}