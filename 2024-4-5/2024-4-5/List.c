#define _CRT_SECURE_NO_WARNINGS 

#include "List.h"

LNode* BuyNode(LDataType x)
{
	LNode* newnode = (LNode*)malloc(sizeof(LNode));
	if (newnode == NULL)
	{
		perror("mallloc fail!\n");
		return;
	}

	newnode->next = NULL;
	newnode->prev = NULL;
	newnode->data = x;

	return newnode;
}


LNode* ListInit()
{
	LNode* phead = BuyNode(0);

	phead->next = phead;
	phead->prev = phead;
	phead->data = 0;

	return phead;
}

void ListDestory(LNode* phead)
{
	assert(phead);
	LNode* cur = phead->next;
	while (cur != phead)
	{
		LNode* next = cur->next;
		free(cur);
		cur = next;
	}
}

void ListPrint(LNode* phead)
{
	assert(phead);

	LNode* cur = phead->next;
	while (cur != phead)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

void ListPushBack(LNode* phead, LDataType x)
{
	assert(phead);

	//LNode* newnode = BuyNode(x);

	//LNode* tail = phead->prev;
	//tail->next = newnode;
	//newnode->prev = tail;
	//newnode->next = phead;
	//phead->prev = newnode;

	ListInsert(phead, x);
}

void ListPushFront(LNode* phead, LDataType x)
{
	assert(phead);

	//LNode* newnode = BuyNode(x);

	//LNode* cur = phead->next;
	//phead->next = newnode;
	//newnode->prev = phead;
	//newnode->next = cur;
	//cur->prev = newnode;

	ListInsert(phead->next, x);
}

void ListPopBack(LNode* phead)
{
	assert(phead);

	//不能把哨兵位节点销毁
	assert(phead->next != phead);

	//LNode* tail = phead->prev;
	//LNode* prev = tail->prev;

	//prev->next = phead;
	//phead->prev = prev;

	//free(tail);
	//tail = NULL;

	ListErase(phead->prev);
}

void ListPopFront(LNode* phead)
{
	assert(phead);
	//LNode* cur = phead->next->next;
	//free(phead->next);

	//phead->next = cur;
	//cur->prev = phead;

	ListErase(phead->next);
}


LNode* ListFind(LNode* phead, LDataType x)
{
	assert(phead);

	LNode* cur = phead->next;
	while (cur != phead)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}

	return NULL;
}

void ListInsert(LNode* pos, LDataType x)
{
	LNode* newnode = BuyNode(x);

	LNode* first = pos->prev;
	first->next = newnode;
	newnode->prev = first;

	newnode->next = pos;
	pos->prev = newnode;
}

void ListErase(LNode* pos)
{
	assert(pos);

	LNode* first = pos->prev;
	LNode* second = pos->next;

	free(pos);
	first->next = second;
	second->prev = first;
}
