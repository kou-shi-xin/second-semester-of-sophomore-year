#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <assert.h>
#include <string.h>

//模拟实现strlen
//size_t my_strlen(char* str)
//{
//	assert(str);
//	size_t count = 0;
//	while (*str++)
//	{
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	char* p = "abcdef";
//	size_t n = my_strlen(p);
//	printf("%zd\n", n);
//
//	return 0;
//}


//模拟实现strcpy
//char* my_strcpy(char* dest, char* scr)
//{
//	assert(dest && scr);
//	char* ret = dest;
//	while (*scr)
//	{
//		*dest++ = *scr++;
//	}
//	*dest = *scr;
//	return ret;
//}
//
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = { 0 };
//	char* p = my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//
//	return 0;
//}

// int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] =  "mnop";
//	strcat(arr2, arr1);
//	printf("%s\n", arr2);
//
//	return 0;
//}

//模拟实现strcat
//char* my_strcat(char* dest, char* scr)
//{
//	assert(dest && scr);
//	char* ret = dest;
//	//1.找到\0
//	while (*dest!='\0')
//	{
//		*dest++;
//	}
//	//2.追加
//	while (*scr)
//	{
//		*dest++ = *scr++;
//	}
//	*dest = *scr;
//	return ret;
//}
//
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = "mnop";
//	char* p = my_strcat(arr2, arr1);
//	printf("%s\n", arr2);
//
//	return 0;
//}
//


//int main()
//{
//	int arr[] = { 1,2,3,4,5,1,2,3,4 };
//	int num = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (int i = 0; i < sz; i++)
//	{
//		num ^= arr[i];
//	}
//	printf("%d\n", num);
//
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,1,2,3,4,6 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int num = 0;
//	//1.整体异或，结果就是两个不同数字的异或结果  5^6
//	for (int i = 0; i < sz; i++)
//	{
//		num ^= arr[i];
//	}
//	//2.找到5^6倒数第k位为1
//	int k = 0;
//	for (int i = 0; i < 32; i++)
//	{
//		if (((num>> k) & 1) == 1)
//		{
//			k = i;
//			break;
//		}
//	}
//	//3.根据倒数第k位为1或0，把全部数字分开，再分别异或
//	int p1 = 0;
//	int p2 = 0;
//	for (int i = 0; i < sz; i++)
//	{
//		if (((arr[i] >> k) & 1) == 1)
//		{
//			p1 ^= arr[i];
//		}
//		else
//		{
//			p2 ^= arr[i];
//		}
//	}
//	printf("%d %d", p1, p2);
//
//	return 0;
//}