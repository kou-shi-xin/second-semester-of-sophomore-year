#define _CRT_SECURE_NO_WARNINGS 

#include "SeqList.h"

void SeqListInit(SL* ps)
{
	ps->a = (SQDataType*)malloc(sizeof(SQDataType) * 4);
	ps->capacity = 4;
	ps->size = 0;

}

void SeqListDestory(SL* ps)
{
	free(ps->a);
	ps->a = NULL;
	ps->capacity = 0;
	ps->size = 0;
}

void CheakCapacity(SL* ps)
{
	if (ps->size == ps->capacity)
	{
		SQDataType* tmp = (SQDataType*)realloc(ps->a, ps->capacity * 2 * sizeof(SQDataType));
		if (tmp == NULL)
		{
			printf("realloc fail!\n");
			return;
		}
		else
		{
			ps->a = tmp;
			ps->capacity *= 2;

		}
	}
}

void SeqListPushBack(SL* ps, SQDataType x)
{
	assert(ps);
	CheakCapacity(ps);

	ps->a[ps->size] = x;
	ps->size++;
}

void SeqListPrint(SL* ps)
{
	assert(ps->size > 0);

	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);

	}
	printf("\n");

}

void SeqListPushFront(SL* ps, SQDataType x)
{
	assert(ps);
	CheakCapacity(ps);

	int end = ps->size;
	while (end > 0)
	{
		ps->a[end] = ps->a[end - 1];
		end--;
	}
	ps->a[end] = x;
	ps->size++;
}

void SeqListPopBack(SL* ps)
{
	assert(ps->size > 0);

	ps->size--;
}

void SeqListPopFront(SL* ps)
{
	assert(ps->size > 0);

	int start = 0;
	while (start < ps->size)
	{
		ps->a[start] = ps->a[start + 1];
		start++;
	}

	ps->size--;
}

void SeqListInsert(SL* ps, int pos, SQDataType x)
{
	assert(ps && pos < ps->size);
	CheakCapacity(ps);

	int end = ps->size;
	while (end > pos)
	{
		ps->a[end] = ps->a[end - 1];
		end--;
	}

	ps->a[pos] = x;
	ps->size++;
}

void SeqListErase(SL* ps, int pos)
{
	assert(ps && pos < ps->size);

	int start = pos;
	while (start < ps->size)
	{
		ps->a[start] = ps->a[start + 1];
		start++;
	}
	ps->size--;
}

int SeqListFind(SL* ps, SQDataType x)
{
	assert(ps);
	for (int i = 0; i < ps->size; i++)
	{
		if (ps->a[i] == x)
		{
			return i;
		}
	}
	return - 1;
}