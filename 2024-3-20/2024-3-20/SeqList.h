#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SQDataType;

typedef struct SeqList
{
	SQDataType* a;
	int size;
	int capacity;

}SL;


void SeqListInit(SL* ps);

void SeqListDestory(SL* ps);

void SeqListPushBack(SL* ps, SQDataType x);

void SeqListPushFront(SL* ps, SQDataType x);

void SeqListPrint(SL* ps);

void SeqListPopBack(SL* ps);

void SeqListPopFront(SL* ps);

void SeqListInsert(SL* ps, int pos, SQDataType x);

void SeqListErase(SL* ps, int pos);

int SeqListFind(SL* ps, SQDataType x);


