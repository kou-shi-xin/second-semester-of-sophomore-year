#pragma once

#include <stdio.h>
#include <stdlib.h>

typedef int SLTDataType;

typedef struct SListNode
{
	SLTDataType data;
	struct SListNode * next;

}SLNode;


//打印数据
void SListPrint(SLNode* phead);

//尾插
void SListPushBack(SLNode** pphead, SLTDataType x);

//头插
void SListPushFront(SLNode** pphead, SLTDataType x);

//尾删
void SListPopBack(SLNode** pphead);

//头删
void SListPopFront(SLNode** pphead);

//查找 
SLNode* SListFind(SLNode* phead, SLTDataType x);

//在pos位置处插入数据
void SListInsert(SLNode** pphead, SLNode* pos, SLTDataType x);

//在pos位置处删除数据
void SListErase(SLNode** pphead, SLNode* pos);

//销毁链表
void SListDestory(SLNode* phead);