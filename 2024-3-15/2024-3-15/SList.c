#define _CRT_SECURE_NO_WARNINGS 

#include "SList.h"

void SListPrint(SLNode* phead)
{
	SLNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

SLNode* BuySListNode(SLTDataType x)
{
	SLNode* newnode = (SLNode*)malloc(sizeof(SLNode));
	if (newnode == NULL)
	{
		printf("malloc fail!\n");
		return;
	}
	newnode->data = x;
	newnode->next = NULL;

	return newnode;
	
}

void SListPushBack(SLNode** pphead, SLTDataType x)
{
	SLNode* newnode = BuySListNode(x);
	

	//1.如果表中没有一个节点
	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		//2.有两个节点以上，找尾，链接新节点
		SLNode* tail = *pphead;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}
		tail->next = newnode;
	}
}


void SListPushFront(SLNode** pphead, SLTDataType x)
{
	SLNode* newnode = BuySListNode(x);
	newnode->next = *pphead;
	*pphead = newnode;
}

void SListPopBack(SLNode** pphead)
{
	//链表内无数据
	if (*pphead == NULL)
	{
		printf("链表为空!\n");
		return;
	}
	//如果只有一个结点
	else if ((*pphead)->next == NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	else
	{
		SLNode* prev = NULL;
		SLNode* cur = *pphead;
		//记住前一个位置
		while (cur->next != NULL)
		{
			prev= cur;
			cur = cur->next;
		}
		free(cur);
		prev->next = NULL;
	}
}

void SListPopFront(SLNode** pphead)
{
	//链表为空
	if (*pphead == NULL)
	{
		printf("链表为空!\n");
		return;
	}
	//只有一个结点
	else if ((*pphead)->next==NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	//有多个结点
	else
	{
		SLNode* cur = NULL;
		cur = (*pphead)->next;
		free(*pphead);
		*pphead = cur;
	}
}


SLNode* SListFind(SLNode* phead, SLTDataType x)
{
	SLNode* cur = phead;
	while (cur!=NULL)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}

	return NULL;
}

void SListInsert(SLNode** pphead, SLNode* pos, SLTDataType x)
{
	SLNode* newnode = BuySListNode(x);

	//当在第一个结点前插入时，相当于头插
	if ((*pphead)->next == NULL)
	{
		SListPushFront(pphead, x);
	}
	else
	{
		SLNode* prev = NULL;
		SLNode* cur = *pphead;
		while (cur->next != pos)
		{
			cur = cur->next;
		}
		prev = cur;
		prev->next = newnode;
		newnode->next = pos;
	}
}

void SListErase(SLNode** pphead, SLNode* pos)
{
	//当要删除第一个结点时，相当于头删
	if (pos == *pphead)
	{
		SListPopFront(pphead);
	}
	else
	{
		SLNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}
		prev->next = pos->next;
		free(pos);
	}

}

void SListDestory(SLNode* phead)
{
	free(phead);
	phead = NULL;
}