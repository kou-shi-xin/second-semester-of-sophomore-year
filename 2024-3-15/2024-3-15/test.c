#define _CRT_SECURE_NO_WARNINGS 

#include "SList.h"

void SListTest()
{
	SLNode* plist = NULL;

	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPrint(plist);

	SLNode* pos = SListFind(plist, 3);
	if (pos != NULL)
	{
		SListErase(&plist, pos);
	}
	SListPrint(plist);

}

int main()
{
	SListTest();

	return;
}