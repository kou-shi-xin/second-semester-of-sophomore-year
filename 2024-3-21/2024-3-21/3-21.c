#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

//struct ListNode* detectCycle(struct ListNode* head) {
//    struct ListNode* slow = head, * fast = head;
//
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//
//        //一个指针从相遇点开始走，一个指针从head开始走，它们会在入口点相遇
//        if (slow == fast)
//        {
//            struct ListNode* meet = slow;
//            while (meet != head)
//            {
//                head = head->next;
//                meet = meet->next;
//            }
//
//            return meet;
//        }
//    }
//
//    return NULL;
//}
//
//
//struct ListNode* middleNode(struct ListNode* head) {
//    struct ListNode* slow = head, * fast = head;
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//    }
//
//    return slow;
//}
//
//
//struct ListNode* reverseList(struct ListNode* head) {
//    struct ListNode* newhead = NULL;
//    struct ListNode* cur = head;
//
//    if (cur == NULL)
//        return cur;
//
//    while (cur)
//    {
//        struct ListNode* next = cur->next;
//        cur->next = newhead;
//        newhead = cur;
//        cur = next;
//    }
//    return newhead;
//}
//
//
//bool hasCycle(struct ListNode* head) {
//    struct ListNode* slow = head;
//    struct ListNode* fast = head;
//
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//
//        if (slow == fast)
//            return true;
//    }
//
//    return false;
//}
//
//struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
//    struct ListNode* head = NULL;
//    struct ListNode* tail = NULL;
//
//    if (list1 == NULL)
//        return list2;
//    if (list2 == NULL)
//        return list1;
//
//    while (list1 && list2)
//    {
//        if (list1->val < list2->val)
//        {
//            if (tail == NULL)
//            {
//                tail = head = list1;
//            }
//            else
//            {
//                tail->next = list1;
//                tail = tail->next;
//            }
//            list1 = list1->next;
//        }
//        else {
//            if (tail == NULL)
//            {
//                tail = head = list2;
//            }
//            else
//            {
//                tail->next = list2;
//                tail = tail->next;
//            }
//            list2 = list2->next;
//        }
//    }
//
//    if (list1)
//        tail->next = list1;
//
//    if (list2)
//        tail->next = list2;
//
//    return head;
//}
//
//
//void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {
//    int end1 = m - 1;
//    int end2 = n - 1;
//    int end = m + n - 1;
//
//    while (end1 >= 0 && end2 >= 0)
//    {
//        if (nums1[end1] < nums2[end2])
//        {
//            nums1[end] = nums2[end2];
//            end--;
//            end2--;
//        }
//        else
//        {
//            nums1[end] = nums1[end1];
//            end1--;
//            end--;
//        }
//    }
//
//    while (end2 >= 0)
//    {
//        nums1[end] = nums2[end2];
//        end--;
//        end2--;
//    }
//}
//
//int removeElement(int* nums, int numsSize, int val) {
//    int src = 0;
//    int des = 0;
//    while (src < numsSize)
//    {
//        if (nums[src] != val)
//        {
//            nums[des++] = nums[src++];
//        }
//        else
//        {
//            src++;
//        }
//    }
//    return des;
//}
#include <assert.h>

char* my_strstr(const char* str1, const char* str2)
{
	assert(str1 && str2);
	const char* s1 = NULL;
	const char* s2 = NULL;
	const char* cur = str1;//记录被查找字符的当前位置

	//若是查找空字符串，则直接返回
	if (*str2 == '\0')
		return (char*)str1;

	while (*cur)
	{
		s1 = cur;
		s2 = str2;

		//当两个字符不为空，并且相等时，接着下一对比较
		while (*s1 != '\0' && *s2 != '\0' && *s1 == *s2)
		{
			s1++;
			s2++;
		}
		//当要查找的字串全部查找完了，则说明找到了
		if (*s2 == '\0')
			return (char*)cur;

		cur++;//当前位置循环一遍后没有，再从下一个字符继续下一轮查找
	}

	return NULL;
}

int main()
{
	char arr1[] = "abbcdefg";
	char arr2[] = "bbc";

	char* ret = my_strstr(arr1, arr2);
	if (ret == NULL)
		printf("找不到\n");
	else
		printf("%s\n", ret);

	return 0;
}