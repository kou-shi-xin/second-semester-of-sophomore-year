#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "Queue.h"


typedef char BTDatdType;

typedef struct BinaryTreeNode
{
	struct BinaryTreeNode* left;
	struct BinaryTreeNode* right;
	BTDatdType data;
}BTNode;


//前序遍历
void PrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	else
	{
		printf("%c ", root->data);
		PrevOrder(root->left);
		PrevOrder(root->right);
	}
}

//中序遍历
void InOrder(BTNode*root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	else
	{
		InOrder(root->left);
		printf("%c ", root->data);
		InOrder(root->right);
	}
}

//后序遍历
void PosOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}
	else
	{
		PosOrder(root->left);
		PosOrder(root->right);
		printf("%c ", root->data);

	}
}

//计算节点的个数
//方法1：局部变量  传址调用  遍历计数
//int TreeNodeSize(BTNode* root, int* psize)
//{
//	if (root == NULL)
//	{
//		return 0;
//	}
//	else
//	{
//		(*(psize))++;
//	}
//
//	TreeNodeSize(root->left, psize);
//	TreeNodeSize(root->right, psize);
//
//}

//方法2
int TreeNodeSize(BTNode* root)
{
	return root == NULL ? 0 : TreeNodeSize(root->left) 
		                    + TreeNodeSize(root->right)+1;
}


//计算叶节点的个数
int TreeLeapSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	if (root->left == NULL && root->right == NULL)
		return 1;
	
	return TreeLeapSize(root->left) + TreeLeapSize(root->right);

}


//层序遍历：广度优先遍历
//void LevelOrder(BTNode* root)
//{
//	Queue q;
//	QueueInit(&q);
//
//	if (root)
//		QueuePush(&q, root);
//
//	while (!QueueEmpty(&q))
//	{
//		BTNode* front = QueueFront(&q);
//		QueuePop(&q);
//		printf("%c ", front->data);
//
//		if (front->left)
//			QueuePush(&q, front->left);
//
//		if (front->right)
//			QueuePush(&q, front->right);
//	}
//	printf("\n");
//
//	QueueDestory(&q);
//}


//销毁树：后序遍历销毁
void DestoryTree(BTNode* root)
{
	if (root == NULL)
		return;

	DestoryTree(root->left);
	DestoryTree(root->right);

	free(root);
	root = NULL;
}


int main()
{
	//开辟节点
	BTNode* A = (BTNode*)malloc(sizeof(BTNode));
	if (A == NULL)
	{
		perror("malloc A");
		return 1;
	}
	//初始化
	A->data = 'A';
	A->left = NULL;
	A->right = NULL;

	BTNode* B = (BTNode*)malloc(sizeof(BTNode));
	if (B == NULL)
	{
		perror("malloc B");
		return 1;
	}
	//初始化
	B->data = 'B';
	B->left = NULL;
	B->right = NULL;

	BTNode* C = (BTNode*)malloc(sizeof(BTNode));
	if (C == NULL)
	{
		perror("malloc C");
		return 1;
	}
	//初始化
	C->data = 'C';
	C->left = NULL;
	C->right = NULL;

	BTNode* D = (BTNode*)malloc(sizeof(BTNode));
	if (D == NULL)
	{
		perror("malloc D");
		return 1;
	}
	//初始化
	D->data = 'D';
	D->left = NULL;
	D->right = NULL;

	BTNode* E = (BTNode*)malloc(sizeof(BTNode));
	if (E == NULL)
	{
		perror("malloc E");
		return 1;
	}
	//初始化
	E->data = 'E';
	E->left = NULL;
	E->right = NULL;

	//链接各个节点
	A->left = B;
	A->right = C;
	B->left = D;
	B->right = E;

	PrevOrder(A);
	printf("\n");

	InOrder(A);
	printf("\n");

	PosOrder(A);
	printf("\n");

	/*int size = 0;
	int ret = TreeNodeSize(A, &size);
	printf("%d\n", size);

	size = 0;
	ret = TreeNodeSize(B, &size);
	printf("%d\n", size);*/

	printf("Asize:%d\n", TreeNodeSize(A));

	printf("Bsize:%d\n", TreeNodeSize(B));

	printf("LeapSize:%d\n", TreeLeapSize(A));

	//LevelOrder(A);

	DestoryTree(A);

	return 0;
}