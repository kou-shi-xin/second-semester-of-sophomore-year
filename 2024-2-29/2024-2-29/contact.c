#define _CRT_SECURE_NO_WARNINGS 

#include "contact.h"

void IntiContact(Contact* pc)
{
	memset(pc->date, 0, sizeof(PeoInfo));
	pc->count = 0;
}

void  AddContact(Contact* pc)
{
	assert(pc);
	if (pc->count == 100)
	{
		printf("通讯录已满，无法增加!\n");
		return;
	}
	printf("请输入要增加人的姓名:>");
	scanf("%s", pc->date[pc->count].name);
	printf("请输入要增加人的年龄:>");
	scanf("%d", &(pc->date[pc->count].age));
	printf("请输入要增加人的性别:>");
	scanf("%s", pc->date[pc->count].sex);
	printf("请输入要增加人的电话:>");
	scanf("%s", pc->date[pc->count].tele);
	printf("请输入要增加人的地址:>");
	scanf("%s", pc->date[pc->count].addr);
	pc->count++;

	printf("增加成功!\n");
}

void ShowContact(const Contact* pc)
{
	assert(pc);
	int i = 0;
	printf("%10s\t%3s\t%5s\t%13s\t%10s\n", "姓名", "年龄", "性别", "电话", "地址");

	for (i = 0; i < pc->count; i++)
	{
		printf("%10s\t%3d\t%5s\t%13s\t%10s\n", 
			pc->date[i].name,
			pc->date[i].age, 
			pc->date[i].sex, 
			pc->date[i].tele,
			pc->date[i].addr);
	}
}

//查找函数
static int FindByName(Contact* pc, char name[])
{
	int i = 0;
	for (i = 0; i < pc->count; i++)
	{
		if (strcmp(pc->date[i].name, name)==0)
		{
			return i;
		}
	}
	return -1;
}

void DelContact(Contact* pc)
{
	assert(pc);
	//判断有没有联系人
	if (pc->count == 0)
	{
		printf("通讯录为空，无法删除。\n");
		return;
	}

	char name[MAX_NAME] = { 0 };
	printf("请输入要删除人的姓名:>");
	scanf("%s", name);

	//1.查找
	int ret = FindByName(pc, name);
	if (ret == -1)
	{
		printf("该联系人不存在，无法删除!\n");
		return;
	}
	//2.删除
	int i = 0;
	for (i = ret; i < pc->count - 1; i++)
	{
		pc->date[i] = pc->date[i + 1];
	}
	pc->count--;
	printf("删除成功!\n");
}

void SearchContact(const Contact* pc)
{
	assert(pc);
	char name[MAX_NAME] = { 0 };
	printf("请输入要查找人的姓名:>");
	scanf("%s", name);

	//1.查找
	int ret = FindByName(pc, name);
	if (ret == -1)
	{
		printf("该联系人不存在!\n");
		return;
	}
	//2.打印
	printf("%10s\t%3s\t%5s\t%13s\t%10s\n", "姓名", "年龄", "性别", "电话", "地址");
	printf("%10s\t%3d\t%5s\t%13s\t%10s\n",
		pc->date[ret].name,
		pc->date[ret].age,
		pc->date[ret].sex,
		pc->date[ret].tele,
		pc->date[ret].addr);
}

void ModifyContact(Contact* pc)
{
	assert(pc);
	char name[MAX_NAME] = { 0 };
	printf("请输入要修改人的姓名:>");
	scanf("%s", name);

	//1.查找
	int ret = FindByName(pc, name);
	if (ret == -1)
	{
		printf("该联系人不存在!\n");
		return;
	}
	//2.修改
	printf("请输入修改后人的姓名:>");
	scanf("%s", pc->date[ret].name);
	printf("请输入修改后人的年龄:>");
	scanf("%d", &(pc->date[ret].age));
	printf("请输入修改后人的性别:>");
	scanf("%s", pc->date[ret].sex);
	printf("请输入修改后人的电话:>");
	scanf("%s", pc->date[ret].tele);
	printf("请输入修改后人的地址:>");
	scanf("%s", pc->date[ret].addr);
	
	printf("修改成功!\n");
}
	
int cmp_name(const void* e1, const void* e2)
{
	return strcmp(((PeoInfo*)e1)->name, ((PeoInfo*)e2)->name);
}

void SortContact( Contact* pc)
{
	assert(pc);
	qsort(pc->date, pc->count, sizeof(pc->date[0]), cmp_name);
	printf("排序成功!\n");
}
