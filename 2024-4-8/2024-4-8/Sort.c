#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>

//把数组分成区间
void _MergrSort(int* arr, int left, int right, int* tmp)
{
	if (left >= right)
		return;

	int mid = (left + right) >> 1;

	//假设[left mid] [mid+1 right]有序，就可以归并
	_MergrSort(arr, left, mid, tmp);
	_MergrSort(arr, mid + 1, right, tmp);

	int begin1 = left;
	int end1 = mid;
	int begin2 = mid + 1;
	int end2 = right;
	int index = left;

	while (begin1 <= end1 && begin2 <= end2)
	{
		if (arr[begin1] < arr[begin2])
		{
			tmp[index++] = arr[begin1++];
		}
		else
		{
			tmp[index++] = arr[begin2++];

		}
	}

	//当其中有一个区间结束时，另一个有序区间的数据直接拷贝进临时数组里
	while (begin1 <= end1)
	{
		tmp[index++] = arr[begin1++];
	}

	while (begin2 <= end2)
	{
		tmp[index++] = arr[begin2++];
	}

	//最后把临时数组里面的数据拷贝回原数组
	for (int i = left; i <= right; i++)
	{
		arr[i] = tmp[i];
	}
}

void MergrSort(int* arr, int sz)
{
	int* tmp = (int*)malloc(sizeof(int) * sz);
	if (tmp == NULL)
	{
		perror("malloc");
		return;
	}
	_MergrSort(arr, 0, sz - 1, tmp);

	free(tmp);
	tmp = NULL;
}

void PrintArr(int* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

int main()
{
	int arr[] = { 5,4,2,9,8,4,3,1,2,0,-9 };
	int sz = sizeof(arr) / sizeof(arr[0]);

	MergrSort(arr, sz);
	PrintArr(arr, sz);

	return 0;
}