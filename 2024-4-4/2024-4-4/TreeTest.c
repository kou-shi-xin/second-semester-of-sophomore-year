#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>

#include "Queue.h"

typedef char BTDataType;

//定义二叉树的结构
typedef struct BinaryTreeNode
{
	BTDataType data;//存放的数据
	struct BinaryTreeNode* left;//左子树
	struct BinaryTreeNode* right;//右子树
}BTNode;


//前序遍历
void PrevOrder(BTNode* root)//根节点
{
	if (root == NULL)
	{
		printf("NULL ");
		return; 
	}

	printf("%c ", root->data);
	PrevOrder(root->left);
	PrevOrder(root->right);
}


//中序遍历
void InOrder(BTNode* root)//根节点
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	InOrder(root->left);
	printf("%c ", root->data);
	InOrder(root->right);

}


//后序遍历
void PostOrder(BTNode* root)//根节点
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	PostOrder(root->left);
	PostOrder(root->right);
	printf("%c ", root->data);

}

//计算一棵二叉树的总节点
//1.遍历递归计数，定义局部变量size，传地址计数
//void TreeSize(BTNode* root,int *psize)
//{
//	if (root == NULL)
//	{
//		return;
//	}
//	else
//	{
//		(*psize)++;
//	}
//
//	TreeSize(root->left, psize);
//	TreeSize(root->right, psize);
//
//}

//2.递归
//int TreeSize(BTNode* root)
//{
//	return root == NULL ? 0 : TreeSize(root->left) + TreeSize(root->right) + 1;
//}

//计算叶子节点的个数
int TreeLeafSize(BTNode* root)
{
	if (root == NULL)
		return 0;

	if (root->left == NULL && root->right == NULL)//是叶节点
		return 1;

	return TreeLeafSize(root->left) + TreeLeafSize(root->right);
}

void LealOrder(BTNode* root)
{
	Queue q;
	QueueInit(&q);

	if (root)
		QueuePush(&q, root);

	while (!QueueEmpty(&q))
	{
		BTNode* front = QueueFront(&q);//取出队头
		QueuePop(&q);
		printf("%c ", front->data);

		if (front->left)//左不为空，入左节点
			QueuePush(&q, front->left);

		if (front->right)//右不为空，入右节点
			QueuePush(&q, front->right);
	}
	printf("\n");

	QueueDestory(&q);
}

//计算二叉树的最大深度
int MaxDepth(BTNode* root)
{
	if (root == NULL)
		return 0 ;

	int leftdepth = MaxDepth(root->left);
	int rightdepth = MaxDepth(root->right);

	return leftdepth > rightdepth ? leftdepth + 1 : rightdepth + 1;

}

//销毁二叉树
void DestoryTree(BTNode* root)
{
	if (root == NULL)
		return;

	DestoryTree(root->left);
	DestoryTree(root->right);

	free(root);
	root = NULL;
}


void TreeTest()
{
	//1.开辟节点和初始化
	BTNode* A = (BTNode*)malloc(sizeof(BTDataType));
	if (A == NULL)
	{
		perror("malloc fail\n");
		return;
	}
	A->data = 'A';
	A->left = NULL;
	A->right = NULL;

	BTNode* B = (BTNode*)malloc(sizeof(BTDataType));
	if (B == NULL)
	{
		perror("malloc fail\n");
		return;
	}
	B->data = 'B';
	B->left = NULL;
	B->right = NULL;

	BTNode* C = (BTNode*)malloc(sizeof(BTDataType));
	if (C == NULL)
	{
		perror("malloc fail\n");
		return;
	}
	C->data = 'C';
	C->left = NULL;
	C->right = NULL;

	BTNode* D = (BTNode*)malloc(sizeof(BTDataType));
	if (D == NULL)
	{
		perror("malloc fail\n");
		return;
	}
	D->data = 'D';
	D->left = NULL;
	D->right = NULL;

	BTNode* E = (BTNode*)malloc(sizeof(BTDataType));
	if (E == NULL)
	{
		perror("malloc fail\n");
		return;
	}
	E->data = 'E';
	E->left = NULL;
	E->right = NULL;
	

	//2.链接各个节点
	A->left = B;
	A->right = C;
	B->left = D;
	B->right = E;

	//3.进行输出
	PrevOrder(A);
	printf("\n");

	InOrder(A);
	printf("\n");

	PostOrder(A);
	printf("\n");

	/*int Asize = 0;
	TreeSize(A, &Asize);
	printf("Asize:%d\n", Asize);

	int Bsize = 0;
	TreeSize(B, &Bsize);
	printf("Bsize:%d\n", Bsize);*/

	/*printf("Asize:%d\n", TreeSize(A));
	printf("Bsize:%d\n", TreeSize(B));*/

	//printf("LeafSize:%d\n", TreeLeafSize(A));

	LealOrder(A);

	//printf("MaxDepth = %d\n", MaxDepth(A));
}

int main()
{

	TreeTest();

	return 0;
}