#define _CRT_SECURE_NO_WARNINGS 

//移除元素
int removeElement(int* nums, int numsSize, int val) {

    int src = 0;
    int dst = 0;

    while (src < numsSize)
    {
        //当src指向的值不等于val时，放进dst中，再两者均++
        if (nums[src] != val)
        {
            nums[dst++] = nums[src++];
        }
        else
        {
            //当src指向的值等于val时，不放进dst中，只让src++
            src++;
        }
    }

    //此时dst就是元素的个数
    return dst;

}


//合并两个有序数组
void merge(int* nums1, int nums1Size, int m, int* nums2, int nums2Size, int n) {

    int end1 = m - 1;
    int end2 = n - 1;
    int end = m + n - 1;

    //从两个数组的最后一个元素开始遍历，依次比较
    while (end1 >= 0 && end2 >= 0)
    {
        //看哪个更大，把大的数放到nums1的最后面，再往前走
        if (nums1[end1] > nums2[end2])
        {
            nums1[end--] = nums1[end1--];
        }
        else
        {
            nums1[end--] = nums2[end2--];
        }
    }

    //若最后nums1数组中有剩余，就已经处理好了
    //若最后nums2数组中有剩余，则要依次放进nums1数组中

    while (end2 >= 0)
    {
        nums1[end--] = nums2[end2--];
    }

}


//反转链表
//方法1：定义三个指针，进行迭代
typedef struct ListNode ListNode;
struct ListNode* reverseList(struct ListNode* head) {

    if (head == NULL)
        return NULL;

    ListNode* n1, * n2, * n3;
    n1 = NULL, n2 = head, n3 = n2->next;

    while (n2)
    {
        n2->next = n1;
        n1 = n2;
        n2 = n3;

        if (n3)
            n3 = n3->next;
    }

    return n1;
}

//方法2：头插法
typedef struct ListNode ListNode;
struct ListNode* reverseList(struct ListNode* head) {

    if (head == NULL)
        return NULL;

    ListNode* newHead, * newTail;
    newHead = newTail = NULL;
    ListNode* pcur = head;

    //一个一个拿下来头插
    while (pcur)
    {
        ListNode* next = pcur->next;
        pcur->next = newHead;
        newHead = pcur;
        pcur = next;

    }

    return newHead;
}


//链表的中间节点
//快慢指针法
typedef struct ListNode ListNode;
struct ListNode* middleNode(struct ListNode* head) {
    ListNode* slow = head;
    ListNode* fast = head;

    while (fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;
    }

    return slow;

}


//合并两个有序链表
typedef struct ListNode ListNode;
struct ListNode* mergeTwoLists(struct ListNode* list1, struct ListNode* list2) {
    ListNode* n1, * n2;
    n1 = list1;
    n2 = list2;

    if (n1 == NULL)
        return n2;
    if (n2 == NULL)
        return n1;

    //创建哨兵位节点
    ListNode* phead = (ListNode*)malloc(sizeof(ListNode));
    ListNode* newHead, * newTail;
    newHead = newTail = phead;

    while (n1 && n2)
    {
        //比较两个链表中数据的大小，谁小就先尾插到新链表
        if (n1->val < n2->val)
        {
            newTail->next = n1;
            n1 = n1->next;
            newTail = newTail->next;
        }
        else
        {
            newTail->next = n2;
            n2 = n2->next;
            newTail = newTail->next;
        }
    }

    if (n1)
        newTail->next = n1;
    if (n2)
        newTail->next = n2;

    ListNode* ret = newHead->next;
    free(newHead);

    return ret;

}


//移除链表元素
typedef struct ListNode ListNode;
struct ListNode* removeElements(struct ListNode* head, int val) {
    if (head == NULL)
        return NULL;

    //创建一个新链表
    ListNode* newHead, * newTail;
    newHead = newTail = NULL;
    ListNode* pcur = head;

    //遍历原链表，找不为val的节点尾插
    while (pcur)
    {
        ListNode* next = pcur->next;
        if (pcur->val != val)
        {
            //没有一个节点
            if (newHead == NULL)
            {
                newHead = newTail = pcur;
            }
            else
            {
                //有一个节点以上
                newTail->next = pcur;
                newTail = newTail->next;
            }
        }

        pcur = next;
    }

    if (newTail)
        newTail->next = NULL;

    return newHead;

}


//链表的分割
typedef struct ListNode ListNode;
struct ListNode* partition(struct ListNode* head, int x) {
    if (head == NULL)
        return NULL;

    ListNode* lessHead, * lessTail, * greaterHead, * greaterTail;
    ListNode* pcur = head;

    //创建哨兵位节点，方便尾插
    lessHead = lessTail = (ListNode*)malloc(sizeof(ListNode));
    greaterHead = greaterTail = (ListNode*)malloc(sizeof(ListNode));
    lessTail->next = NULL;
    greaterTail->next = NULL;

    while (pcur)
    {
        if (pcur->val < x)
        {
            lessTail->next = pcur;
            lessTail = lessTail->next;
        }
        else
        {
            greaterTail->next = pcur;
            greaterTail = greaterTail->next;
        }

        pcur = pcur->next;
    }

    lessTail->next = greaterHead->next;
    greaterTail->next = NULL;

    return lessHead->next;

}


//判断是否有环
typedef struct ListNode ListNode;
bool hasCycle(struct ListNode* head) {

    ListNode* slow, * fast;
    slow = fast = head;

    while (fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;
        if (fast == slow)
            return true;
    }

    return false;
}


//相交链表
typedef struct ListNode ListNode;
struct ListNode* getIntersectionNode(struct ListNode* headA, struct ListNode* headB) {

    ListNode* TailA, * TailB;
    TailA = headA;
    TailB = headB;

    //找尾同时计算长度
    int lenA = 0;
    int lenB = 0;
    while (TailA->next)
    {
        TailA = TailA->next;
        lenA++;
    }

    while (TailB->next)
    {
        TailB = TailB->next;
        lenB++;
    }

    //不相交
    if (TailA != TailB)
    {
        return NULL;
    }

    //求出长度差
    int gap = abs(lenA - lenB);

    //判断哪个是长链表
    ListNode* longList = headA;//先默认A是长链表
    ListNode* shortList = headB;

    if (lenA < lenB)
    {
        shortList = headA;
        longList = headB;
    }

    //让长链表走长度差步
    while (gap--)
    {
        longList = longList->next;
    }

    //最后长短链表一起走，找交点
    while (longList != shortList)
    {
        longList = longList->next;
        shortList = shortList->next;
    }

    return longList;

}


//链表的回文结构
struct ListNode* middleNode(struct ListNode* head) {
    struct ListNode* slow = head;
    struct ListNode* fast = head;

    while (fast && fast->next)
    {
        slow = slow->next;
        fast = fast->next->next;
    }

    return slow;

}


struct ListNode* reverseList(struct ListNode* head) {

    if (head == NULL)
        return NULL;

    struct ListNode* n1, * n2, * n3;
    n1 = NULL, n2 = head, n3 = n2->next;

    while (n2)
    {
        n2->next = n1;
        n1 = n2;
        n2 = n3;

        if (n3)
            n3 = n3->next;
    }

    return n1;
}


class PalindromeList {
public:
    bool chkPalindrome(ListNode* A) {

        struct ListNode* mid = middleNode(A);
        struct ListNode* rHead = reverseList(mid);

        struct ListNode* curA = A;
        struct ListNode* curR = rHead;

        while (curA && curR)
        {
            if (curA->val != curR->val)
            {
                return false;
            }
            else
            {
                curA = curA->next;
                curR = curR->next;
            }
        }
        return true;

    }


};


//环形链表的约瑟夫问题
typedef struct ListNode ListNode;

//创建节点
ListNode* BuyNode(int x)
{
    ListNode* newnode = (ListNode*)malloc(sizeof(ListNode));
    if (newnode == NULL)
    {
        exit(-1);
    }

    newnode->val = x;
    newnode->next = NULL;

    return newnode;
}

//1.创建循环链表
ListNode* Createnodecircle(int n)
{
    ListNode* phead = BuyNode(1);
    ListNode* ptail = phead;
    for (int i = 2; i <= n; i++)
    {
        ptail->next = BuyNode(i);
        ptail = ptail->next;
    }
    //尾连头，成环
    ptail->next = phead;

    return ptail;

}

int ysf(int n, int m) {
    // write code here
    ListNode* prev = Createnodecircle(n);
    ListNode* pcur = prev->next;
    //开始数数
    int count = 1;
    while (pcur->next != prev->next)
    {
        if (count == m)
        {
            prev->next = pcur->next;
            free(pcur);
            pcur = prev->next;
            count = 1;
        }
        else
        {
            prev = pcur;
            pcur = pcur->next;
            count++;
        }
    }

    return pcur->val;
}


//链表的倒数第k个节点
typedef struct ListNode ListNode;
struct ListNode* FindKthToTail(struct ListNode* pHead, int k) {
    ListNode* fast, * slow;
    fast = slow = pHead;

    //fast先走k步
    while (k--)
    {
        //K大于链表长度时，直接返回NULL
        if (fast == NULL)
        {
            return NULL;
        }

        fast = fast->next;
    }

    //再两者一起走
    while (fast)
    {
        fast = fast->next;
        slow = slow->next;
    }

    return slow;
}
