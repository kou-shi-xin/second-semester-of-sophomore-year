#define _CRT_SECURE_NO_WARNINGS 

//#include <stdio.h>
//#include <stdlib.h>
//
////定义二叉树节点
//typedef struct TreeNode
//{
//    char data;
//    struct TreeNode* left;
//    struct TreeNode* right;
//}TreeNode;
//
////创建二叉树
//TreeNode* createTree(char* str, int* index)
//{
//    if (str[*index] == '\0' || str[*index] == ')')
//    {
//        (*index)++;
//        return NULL;
//    }
//
//    TreeNode* node = (TreeNode*)malloc(sizeof(TreeNode));
//    if (node == NULL)
//    {
//        exit(-1);
//    }
//    node->data = str[*index];
//    (*index)++;
//
//    if (str[*index] == '(')
//    {
//        (*index)++;
//        node->left = createTree(str, index);
//    }
//    else
//    {
//        node->left = NULL;
//    }
//
//    if (str[*index] == '(')
//    {
//        (*index)++;
//        node->right = createTree(str, index);
//    }
//    else
//    {
//        node->right = NULL;
//    }
//
//    (*index)++;
//    return node;
//}
//
//// 销毁二叉树
//void destroyTree(TreeNode * root)
//{
//    if (root == NULL)
//    {
//        return;
//    }
//
//    destroyTree(root->left);
//    destroyTree(root->right);
//    free(root);
//}
//
//
//// 查找节点
//void findNode(TreeNode * root, char target)
//{
//    if (root == NULL)
//    {
//        printf("结点不存在\n");
//        return;
//    }
//    if (root->data == target)
//    {
//        //printf("结点 %c 存在\n",target);
//        printf("(3)%c结点:", target);
//        if (root->left)
//        {
//            printf("左孩子为%c ", root->left->data);
//        }
//        if (root->right)
//        {
//            printf("右孩子为%c\n",root->right->data);
//        }
//        return;
//    }
//    findNode(root->left, target);
//    findNode(root->right, target);
//}
//
//// 求二叉树的高度
//int treeHeight(TreeNode * root)
//{
//    if (root == NULL)
//    {
//        return 0;
//    }
//
//    int leftHeight = treeHeight(root->left);
//    int rightHeight = treeHeight(root->right);
//    return (leftHeight > rightHeight ? leftHeight : rightHeight) + 1;
//}
//
////输出二叉树(前序遍历)
//void printTree(TreeNode* root)
//{
//    if (root == NULL)
//    {
//        return;
//    }
//
//    printf("%c", root->data);
//    if (root->left || root->right)
//    {
//        printf("(");
//        printTree(root->left);
//        printf(",");
//        printTree(root->right);
//        printf(")");
//    }
//}
//
//int main()
//{
//    char str[100];
//    char target;
//
//    scanf("%s", str);
//    getchar();
//
//    target = getchar();
//    //scanf("%c",&target);
//    getchar();
//
//    int index = 0;
//    TreeNode* root = createTree(str, &index);
//
//    printf("(1)创建二叉树\n");
//    printf("(2)输出二叉树:");
//    printTree(root);
//    printf("\n");
//
//    findNode(root, target);
//
//    printf("(4)二叉树b的高度:%d\n", treeHeight(root));
//
//    destroyTree(root);
//    printf("(5)释放二叉树");
//
//    return 0;
//}

//A(B(D,E(H(J,K(L,M(,N))))),C(F,G(,I)))
//H

//A(B(D,E(G)),C(,F))
//C

//#include <iostream>
//#include <stdlib.h>
//#include <assert.h>
//#include <stack>
//#include <queue>
//using namespace std;
//
//template<class T>
//struct BTreeNode {
//	T data;
//
//	BTreeNode<T>* leftNode;
//	BTreeNode<T>* rightNode;
//};
//
////typedef BTreeNode<int> BTINT;
//
//template<class T>
//class BTree 
//{
//	BTreeNode<T>* m_root = nullptr;
//	BTreeNode<T>* getNewNode(T val) 
//	{
//		BTreeNode<T>* newNode = new (std::nothrow) BTreeNode<T>();
//		newNode->data = val;
//		if (newNode == nullptr) 
//		{
//			cerr << "申请新的内存失败！" << endl;
//			this->destroyTree(m_root);
//			exit(1);
//		}
//		return newNode;
//	}
//public:
//	BTree() :m_root(nullptr) {}
//	~BTree() {}
//	BTreeNode<T>* const getRootNode() 
//	{
//		return m_root;
//	}
//	void initBinaryTree(const string& desStr) 
//	{
//		//根据广义表字符串初始构造二叉树
//		if (desStr == "")
//			return;
//		BTreeNode<T>* lastNode = nullptr;
//		stack<BTreeNode<T>*> nodeStack;
//		bool isLeft = true;
//		int curIt = 0;
//		char ch = desStr.at(curIt);
//		while (ch != '\0') 
//		{
//			switch (ch) 
//			{
//			case '(':
//				nodeStack.push(lastNode);
//				isLeft = true;
//				break;
//			case ')':
//				isLeft = false;
//				if (nodeStack.empty() != true)
//					nodeStack.pop();
//				break;
//			case ',':
//				isLeft = false;
//				break;
//			default:
//				lastNode = this->getNewNode(ch);
//				if (m_root == nullptr) 
//				{
//					m_root = lastNode;
//				}
//				else 
//				{
//					BTreeNode<T>* parentNode = nodeStack.top();
//					if (isLeft == true) 
//					{
//						parentNode->leftNode = lastNode;
//					}
//					else 
//					{
//						parentNode->rightNode = lastNode;
//					}
//				}
//			}
//			++curIt;
//			if (curIt == desStr.length()) 
//			{
//				break;
//			}
//			ch = desStr.at(curIt);
//		}//while end
//	}
//
//	void levelOrder(BTreeNode<T>* pCurNode) const 
//	{
//		if (pCurNode == nullptr)
//			return;
//		BTreeNode<T>* curNode = pCurNode;
//		queue < BTreeNode<T>*> que;
//		que.push(curNode);
//		while (que.empty() == false) 
//		{
//			curNode = que.front();
//			que.pop();
//			cout << curNode->data << " ";
//			if (curNode->leftNode != nullptr)que.push(curNode->leftNode);
//			if (curNode->rightNode != nullptr)que.push(curNode->rightNode);
//		}
//	}
//
//	void preOrder(BTreeNode<T>* pCurNode) const 
//	{
//		//前序遍历
//		if (pCurNode == nullptr)
//			return;
//		cout << pCurNode->data << " ";
//		preOrder(pCurNode->leftNode);
//		preOrder(pCurNode->rightNode);
//	}
//
//	void inOrder(BTreeNode<T>* pCurNode) const 
//	{
//		//中序遍历
//		if (pCurNode == nullptr)
//			return;
//		inOrder(pCurNode->leftNode);
//		cout << pCurNode->data << " ";
//		inOrder(pCurNode->rightNode);
//	}
//	void postOrder(BTreeNode<T>* pCurNode) const 
//	{
//		//后序遍历
//		if (pCurNode == nullptr)
//			return;
//		postOrder(pCurNode->leftNode);
//		postOrder(pCurNode->rightNode);
//		cout << pCurNode->data << " ";
//	}
//
//	void destroyTree(BTreeNode<T>* pCurNode) 
//	{
//		//后序遍历方式，销毁整颗树
//		if (pCurNode == nullptr)
//			return;
//		destroyTree(pCurNode->leftNode);
//		destroyTree(pCurNode->rightNode);
//		delete pCurNode;
//		pCurNode = nullptr;
//	}
//
//};
//
//int main() {
//
//	BTree<char>* binaryTree = new BTree<char>();
//	binaryTree->initBinaryTree("A(B(D,E(H(J,K(L,M(,N))))),C(F,G(,I)))");
//
//	cout << "层次遍历" << ":";
//	binaryTree->levelOrder(binaryTree->getRootNode());
//	cout << endl;
//
//	cout << "前序遍历" << ":";
//	binaryTree->preOrder(binaryTree->getRootNode());
//	cout << endl;
//
//	cout << "中序遍历" << ":";
//	binaryTree->inOrder(binaryTree->getRootNode());
//	cout << endl;
//
//	cout << "后序遍历" << ":";
//	binaryTree->postOrder(binaryTree->getRootNode());
//	cout << endl;
//	
//	binaryTree->destroyTree(binaryTree->getRootNode());
//	cout << endl;
//	//system("pause");
//
//	return 0;
//}

#include <stdio.h>
#include <stdlib.h>

const int MaxSize = 50;

typedef char ElementType;
typedef struct bitnode
{
    ElementType data;
    struct bitnode* left, * right;
} bitnode, * bitree;

void CreateTree(bitree& b, char str[])
{
    char ch;
    bitree stack[MaxSize], p = NULL;
    int top = -1, k = 0, j = 0;

    while ((ch = str[j++]) != '\0')
    {
        switch (ch)
        {
        case '(':
            top++;
            stack[top] = p;
            k = 1;
            break;
        case ',':
            k = 2;
            break;
        case ')':
            top--;
            break;
        default:
            p = (bitree)malloc(sizeof(bitnode));
            p->data = ch;
            p->left = p->right = NULL;

            if (b == NULL)
                b = p;
            else
            {
                switch (k)
                {
                case 1:
                    stack[top]->left = p;
                    break;
                case 2:
                    stack[top]->right = p;
                    break;
                }
            }
        }
    }
}


void PrinTree(bitree b)
{
    if (b)
    {
        printf("%c", b->data);
        if (b->left != NULL || b->right != NULL)
        {
            printf("(");
            PrinTree(b->left);
            if (b->right != NULL)
                printf(",");
            PrinTree(b->right);
            printf(")");
        }
    }
}

void FreeTree(bitree b)
{
    if (b != NULL)
    {
        FreeTree(b->left);
        FreeTree(b->right);

        free(b);
        b = NULL;
    }        
}

void PreOrder(bitree b)//先序遍历
{
    if (b != NULL)
    {
        printf("%c ", b->data);
        PreOrder(b->left);
        PreOrder(b->right);
    }
}

void InOrder(bitree b)//中序遍历
{
    if (b != NULL)
    {
        InOrder(b->left);
        printf("%c ", b->data);
        InOrder(b->right);
    }
}

void PostOrder(bitree b)//后序遍历 
{
    if (b != NULL)
    {
        PostOrder(b->left);
        PostOrder(b->right);
        printf("%c ", b->data);
    }
}

void LevelOrder(bitree b) {//层次遍历
    bitnode* p;
    bitnode* qu[MaxSize];
    int front, rear;
    front = rear = 0;
    rear++;
    qu[rear] = b;
    while (front != rear) {
        front = (front + 1) % MaxSize;
        p = qu[front];
        printf("%c ", p->data);
        if (p->left != NULL) {
            rear = (rear + 1) % MaxSize;
            qu[rear] = p->left;
        }
        if (p->right != NULL) {
            rear = (rear + 1) % MaxSize;
            qu[rear] = p->right;
        }
    }
}

int main() {
    char str[] = "A(B(D,E(H(J,K(L,M(,N))))),C(F,G(,I)))";
    bitree root = NULL;

    CreateTree(root, str);

    printf("层次遍历:");
    LevelOrder(root);

    printf("\n前序遍历:");
    PreOrder(root);

    printf("\n中序遍历:");
    InOrder(root);

    printf("\n后序遍历:");
    PostOrder(root);

    FreeTree(root);

    return 0;
}

