#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

void Swap(int* x, int* y)
{
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

void AdjustDown(int* arr, int sz, int parent)
{
	int child = parent * 2 + 1;//默认是左孩子大

	while (child < sz)
	{
		if (arr[child + 1] > arr[child])
		{
			//如果右孩子大，更新为右孩子
			child += 1;
		}

		//把左右孩子里大的与父亲比较
		if (child + 1 < sz && arr[child] > arr[parent])
		{
			Swap(&arr[child], &arr[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapSort(int* arr, int sz)
{
	//建堆
	for (int i = (sz - 1 - 1) / 2; i >= 0; i--)
	{
		//从最后一个非叶子节点开始调整
		AdjustDown(arr, sz, i);
	}

	//排序
	int end = sz - 1;
	while (end > 0)
	{
		Swap(&arr[0], &arr[end]);
		AdjustDown(arr, end, 0);
		end--;

	}
}

void Print(int* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

int main()
{
	int arr[] = { 45,21,12,14,3,5,0,98 };
	int sz = sizeof(arr) / sizeof(int);

	//排升序
	HeapSort(arr,sz);
	Print(arr,sz);

	return 0;
}