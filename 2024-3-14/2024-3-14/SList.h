#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>

typedef int SLTDataType;

struct SListNode
{
	SLTDataType data;
	struct SListNode* next;
};

typedef struct SListNode SLTNode;

//尾插
void SListPushBack(SLTNode** pphead, SLTDataType x);

//头插
void SListPushFront(SLTNode** pphead, SLTDataType x);

//打印数据
void SListPrint(SLTNode* phead);

//尾删
void SListPopBack(SLTNode** pphead);


//头删
void SListPopFront(SLTNode** pphead);


//查找
SLTNode* SListFind(SLTNode* pphead, SLTDataType x);

//在pos位置处插入数据
void SListInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x);


//在pos位置处删除数据
void SListEarse(SLTNode** pphead, SLTNode* pos);
