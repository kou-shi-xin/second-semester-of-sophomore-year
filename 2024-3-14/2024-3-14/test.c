#define _CRT_SECURE_NO_WARNINGS 

#include "SList.h"

void SListTest()
{
	SLTNode* plist = NULL;
	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPrint(plist);

	//在3前面插入有一个30，先找到3的前一个的位置，再插入
	SLTNode* pos = SListFind(plist, 1);
	if (pos)
	{
		SListEarse(&plist,pos);
	}
	SListPrint(plist);

}

int main()
{
	SListTest();

	return 0;
}