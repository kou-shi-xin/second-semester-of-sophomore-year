#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <assert.h>
#include <ctype.h>
#include <stdlib.h>

//模拟实现atoi函数

int my_atoi(const char* str)
{
	assert(str);

	if (*str == '\0')
		return 0;

	//过滤前导空格
	while (isspace(*str))
	{
		str++;
	}

	//判断正负号
	int flag = 1;
	if (*str == '+')
	{
		flag = 1;
		str++;
	}
	else if (*str == '-')
	{
		flag = -1;
		str++;
	}

	//判断是否是数字字符
	long long ret = 0;

	while (*str!='\0')
	{
		if (isdigit(*str))
		{
			ret = ret * 10 + (*str - '0') * flag;

			//判断字符串转换后是否超过整型最大值，是否低于整型最小值
			if (ret > INT_MAX)
				return INT_MAX;
			if (ret < INT_MIN)
				return INT_MIN;
		}
		else
		{
			return (int)ret;
		}

		str++;

	}
	return (int)ret;
}

int main()
{
	char* str = "- 1234";

	int ret = my_atoi(str);
	printf("%d ", ret);

	return 0;
}