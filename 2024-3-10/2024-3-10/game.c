#define _CRT_SECURE_NO_WARNINGS 

#include "game.h"

void InitBoard(char arr[ROWS][COLS], int rows, int cols,char set)
{
	for (int i = 0; i < ROWS; i++)
	{
		for (int j = 0; j < COLS; j++)
		{
			arr[i][j] = set;
		}
	}
}

void DisplayBoard(char arr[ROWS][COLS], int row, int col)
{
	printf("------扫雷游戏------\n");
	//打印行号
	for (int i = 0; i <= col; i++)
	{
		printf("%d ", i);
	}
	printf("\n");
	for (int i = 1; i <= row; i++)
	{
		//打印列号
		printf("%d ", i);
		for (int j = 1; j <= col; j++)
		{
			printf("%c ", arr[i][j]);
		}
		printf("\n");
	}
}

void SetMine(char mine[ROWS][COLS], int row, int col)
{
	int count = EASY_COUNT;
	
	while (count)
	{
		int x = rand() % row + 1;
		int y = rand() % col + 1;
		if (mine[x][y]=='0')
		{
			mine[x][y] = '1';
			count--;
		}
	}
}

int FindMineCount(char mine[ROWS][COLS], int x, int y)
{
	int count = 0;
	for (int i = x - 1;i <= x + 1; i++)
	{
		for (int j = y - 1; j <= y + 1; j++)
		{
			count += (mine[i][j] - '0');//(易错点)
		}
	}
	return count;
}


void FindMine(char mine[ROWS][COLS], char show[ROWS][COLS], int row, int col)
{
	int x = 0;
	int y = 0;
	int win = 0;
	while (win<row*col- EASY_COUNT)
	{
		printf("请输入坐标:>");
		scanf("%d %d", &x, &y);
		//判断坐标的有效性
		if (x >= 1 && x <= row && y >= 1 && y <= col)
		{
			//判断是否已经排查过了
			if (show[x][y] == '*')
			{
				if (mine[x][y] == '1')
				{
					printf("很遗憾，你被炸死了!\n");
					DisplayBoard(mine, ROW, COL);
					break;
				}
				else
				{
					//不是雷，就要统计周围的雷的个数
					int count = FindMineCount(mine, x, y);
					show[x][y] = count + '0';  //(易错点)
					DisplayBoard(show, ROW, COL);
					win++;

				}
			}
			else
			{
				printf("该坐标已经被排查，请重新输入。\n");
			}
		}
		else
		{
			printf("坐标非法，请重新输入。\n");
		}
	}
	if (win == row * col - EASY_COUNT) //(易错点)
	{
		printf("恭喜你，扫雷成功!\n");
		DisplayBoard(mine, ROW, COL);
	}
}
