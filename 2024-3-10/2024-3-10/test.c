#define _CRT_SECURE_NO_WARNINGS 

#include "game.h"

void menu()
{
	printf("******************\n");
	printf("*****  1.play ****\n");
	printf("*****  0.exit ****\n");
	printf("******************\n");
}

void game()
{
	//建立棋盘
	//布置雷的信息的棋盘
	char mine[ROWS][COLS] = { 0 };

	//显示雷的个数的棋盘
	char show[ROWS][COLS] = { 0 };

	//初始化棋盘
	//mine棋盘初始化为‘0’
	InitBoard(mine, ROWS, COLS,'0');

	//show棋盘初始化为‘*’
	InitBoard(show, ROWS, COLS,'*');

	//布置雷
	SetMine(mine, ROW, COL);
	DisplayBoard(mine, ROW, COL);

	//打印棋盘
	DisplayBoard(show, ROW, COL);

	//排查雷
	FindMine(mine, show, ROW, COL);

}

void test()
{
	srand((unsigned int)time(NULL));
	int input = 0;
	do
	{
		menu();
		printf("请选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏!\n");
			break;
		default:
			printf("选择错误，请重新选择。\n");
			break;
		}

	} while (input);
}

int main()
{
	test();

	return 0;
}