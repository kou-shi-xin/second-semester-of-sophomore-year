#define _CRT_SECURE_NO_WARNINGS 

#include "SList.h"

void SListPrint(SLTNode* phead)
{
	SLTNode* cur = phead;
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		cur = cur->next;//箭头有解引用的作用，这一步是让cur找到下一个数据
	}
	printf("NULL\n");
}

SLTNode* BuySListNode(SLTDataType x)
{
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));//先开辟新结点
	if (newnode == NULL)
	{
		printf("malloc fail!\n");
		return;
	}
	//初始化
	newnode->data = x;
	newnode->next = NULL;

	return newnode;
}

void SListPushBack(SLTNode** pphead, SLTDataType x)
{
	SLTNode* newnode = BuySListNode(x);
	if (newnode == NULL)
	{
		printf("malloc fail!\n");
		return;
	}
	newnode->data = x;
	newnode->next = NULL;

	if (*pphead == NULL)
	{
		*pphead = newnode;
	}
	else
	{
		//找到尾节点的指针
		SLTNode* tail = *pphead;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}

		//尾节点，链接新节点
		tail->next = newnode;
	}

}


void SListPushFront(SLTNode** pphead, SLTDataType x)
{
	SLTNode* newnode = BuySListNode(x);

	newnode->next = *pphead;
	*pphead = newnode;
}


void SListPopFront(SLTNode** pphead)
{
	//先把下一个节点保存起来
	SLTNode* next = (*pphead)->next;

	//再释放头部节点
	free(*pphead); 

	//再把下一个数据的节点还给头部
	*pphead = next;
}


void SListPopBack(SLTNode** pphead)
{
	//1.判断链表是否为空
	if (*pphead == NULL)
	{
		printf("链表为空!\n");
		return;
	}

	//2.只有一个节点时
	else if ((*pphead)->next==NULL)
	{
		free(*pphead);
		*pphead = NULL;
	}
	//3.有一个以上的节点
	else
	{
		SLTNode* prev = NULL;//定义一个“小跟班”指针
		SLTNode* tail = *pphead;

		//让tail找到尾
		while (tail->next != NULL)
		{
			prev = tail;
			tail = tail->next;
		}

		//找到尾后释放
		free(tail);
		//把前一个节点置为空，此时变成了最后一个节点
		prev->next = NULL;
	}
}

SLTNode* SListFind(SLTNode* phead, SLTDataType x)
{
	SLTNode* cur = phead;
	while (cur)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}

	return NULL;
}

void SListInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x)
{
	//要在第一个节点前插入时，就相当于头插
	if (pos == *pphead)
	{
		SListPushFront(pphead, x);
	}

	else
	{
		SLTNode* newnode = BuySListNode(x);
		SLTNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}

		prev->next = newnode;
		newnode->next = pos;
	}
}


void SListErase(SLTNode** pphead, SLTNode* pos)
{
	//只有一个节点时，相当于头删
	if (pos == *pphead)
	{
		SListPopFront(pphead);
	}
	else
	{
		//大于1个节点时找到pos的前一个节点
		SLTNode* prev = *pphead;
		while (prev->next != pos)
		{
			prev = prev->next;
		}

		prev->next = pos->next;
		free(pos);
	}
}