#define _CRT_SECURE_NO_WARNINGS 

#include "SList.h"

void TestSList()
{
	SLTNode* plist = NULL;//plist为头指针，这时链表里啥也没有，初始化为NULL

	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	SListPushBack(&plist, 4);
	SListPrint(plist);

	//想在3的前面插入30，先用查找函数找到位置，再用插入函数
	SLTNode* pos = SListFind(plist, 1);
	if (pos)
	{
		SListErase(&plist, pos);
	}
	SListPrint(plist);

}

int main()
{
	TestSList();

	return 0;
}