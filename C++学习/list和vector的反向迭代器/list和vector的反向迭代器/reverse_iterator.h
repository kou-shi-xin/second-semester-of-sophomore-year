#pragma once

namespace bit
{
	// 将以下代码适配到vector和list中做反向迭代器，理解反向迭代器的原理
	// 适配器 -- 复用
	template<class Iterator, class Ref, class Ptr>
	struct Reverse_iterator
	{
		Iterator _it;
		typedef Reverse_iterator<Iterator, Ref, Ptr> Self;

		Reverse_iterator(Iterator it)
			:_it(it)
		{}

		Ref operator*()
		{
			Iterator tmp = _it;
			return *(--tmp);
		}

		Ptr operator->()
		{
			return &(operator*());
		}

		Self& operator++()
		{
			--_it;
			return *this;
		}

		Self& operator--()
		{
			++_it;
			return *this;
		}

		bool operator!=(const Self& s)
		{
			return _it != s._it;
		}
	};

	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		// 反向迭代器适配支持
		typedef Reverse_iterator<iterator, T&, T*> reverse_iterator;
		typedef Reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator;

		const_reverse_iterator rbegin() const
		{
			// list_node<int>*
			return const_reverse_iterator(end());
		}

		const_reverse_iterator rend() const
		{
			return const_reverse_iterator(begin());
		}

		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}

		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}

		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		const_iterator begin() const
		{
			return _start;
		}

		const_iterator end() const
		{
			return _finish;
		}
		// ...
	};

	namespace std
	{
		template<class T>
		struct list_node
		{
			list_node<T>* _next;
			list_node<T>* _prev;
			T _data;

			list_node(const T& data() = T())
				:_next(nullptr)
				, _prev(nullptr)
				, _data(data)
			{}
		};

		template<class T>
		class list
		{
		public:

			typedef list_node<T> Node;

			typedef __list_iterator<T, T&, T*> iterator;
			typedef __list_iterator<T, const T&, const T*> const_iterator;

			// 反向迭代器适配支持
			typedef Reverse_iterator<iterator, T&, T*> reverse_iterator;
			typedef Reverse_iterator<const_iterator, const T&, const T*> const_reverse_iterator;

			const_iterator begin() const
			{
				// list_node<int>*
				return const_iterator(_head->_next);
			}

			const_iterator end() const
			{
				return const_iterator(_head);
			}

			iterator begin()
			{
				return iterator(_head->_next);
				//return _head->_next;
			}

			iterator end()
			{
				return iterator(_head);
			}

			const_reverse_iterator rbegin() const
			{
				// list_node<int>*
				return const_reverse_iterator(end());
			}

			const_reverse_iterator rend() const
			{
				return const_reverse_iterator(begin());
			}

			reverse_iterator rbegin()
			{
				return reverse_iterator(end());
			}

			reverse_iterator rend()
			{
				return reverse_iterator(begin());
			}
			//...
		/*private:
			Node* _head;*/
		};
	}
}
