#pragma once

#include <assert.h>
#include <iostream>
using namespace std;

namespace bit
{
	//定义节点结构体

	//为什么定义节点结构体时使用struct而不是class?
	//1.其实用class也可以，但是class与struct默认的访问限定不同
	//当没有声明公有，私有时，struct内容默认是公有，class内容默认的私有
	//所以用class要加上public
	//2.当我们用class没有加上public，也没有实例化对象时，
	// 编译不会报错(报私有成员的错误)，因为模版是不会被细节编译的。
	//只有当我们实例化出对象，模版才会被编译，并且类的实例化并不是
	//对所有成员函数都实例化，而是调用哪个成员函数就实例化哪个。
	//这叫做按需实例化

	template <class T>
	struct ListNode
	{
		ListNode<T>* _next;
		ListNode<T>* _prev;

		T _data;

		//可用匿名对象初始化。如果T是自定义类型，则调用其默认构造
		//并且T是内置类型也升级成了有默认构造的概念了
		ListNode(const T& data = T())
			:_next(nullptr)
			,_prev(nullptr)
			,_data(data)
		{}
	};

	//下面的两个迭代器类只有*和->的返回值不同，两个类冗余
	//可以通过模板，给不同的模板参数，让编译器自己实例化两个类
	template <class T,class Ref,class Ptr>
	struct ListIterator
	{
		typedef ListNode<T> Node;
		typedef ListIterator<T, Ref, Ptr> Self;//名字变得简短

		Node* _node;//定义一个节点指针

		ListIterator(Node* node)
			:_node(node)
		{}

		//前置:返回之后的值
		//++it;//返回与自己一样的类型
		Self& operator++()
		{
			_node = _node->_next;
			return *this;
		}

		Self& operator--()
		{
			_node = _node->_prev;
			return *this;
		}

		//后置:返回之前的值
		Self operator++(int)
		{
			Self tmp(*this);
			_node = _node->_next;
			return tmp;
		}

		Self operator--(int)
		{
			Self tmp(*this);
			_node = _node->_prev;
			return tmp;
		}

		Ref operator*()
		{
			return _node->_data;
		}

		Ptr operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const Self& it)
		{
			return _node != it._node;
		}

		bool operator==(const Self& it)
		{
			return _node == it._node;
		}
	};

	//注意：每个template定义的模板参数都只能供当前函数或当前类使用
	//template <class T>
	////用一个类把Node*封装，就可以重载运算符，
	//// 使得用起来像内置类型，但会转换成函数调用，继而控制Node*的行为
	//class ListIterator
	//{
	//	typedef ListNode<T> Node;
	//	typedef ListIterator<T> Self;//名字变得简短

	//public:
	//	Node* _node;//定义一个节点指针

	//	ListIterator(Node* node)
	//		:_node(node)
	//	{}

	//	//注意:这个迭代器类不要写析构函数，因为这里的节点不是迭代器的
	//	//是链表的，不用把它释放。我们使用begin，end返回节点给迭代器，
	//	//是借助迭代器修改，访问数据，所以我们不需要释放。

	//	//这里也可以说明当一个类中包含指针，并且该指针指向的空间是
	//	//动态开辟的，不一定要释放。

	//	//这里也不需要写拷贝构造进行深拷贝，因为这里要的就是浅拷贝。
	//	//begin返回了第一个节点的迭代器给it，这里就是用默认生成的拷贝构造
	//	//浅拷贝给it,那这两个迭代器就指向同一个节点，所以这里用默认的拷贝构造
	//	//和赋值拷贝就可以了。

	//	//前置:返回之后的值
	//	//++it;//返回与自己一样的类型
	//	Self& operator++()
	//	{
	//		_node = _node->_next;
	//		return *this;
	//	}

	//	Self& operator--()
	//	{
	//		_node = _node->_prev;
	//		return *this;
	//	}

	//	//后置:返回之前的值
	//	Self operator++(int)
	//	{
	//		Self tmp(*this);
	//		_node = _node->_next;
	//		return tmp;
	//	}

	//	Self operator--(int)
	//	{
	//		Self tmp(*this);
	//		_node = _node->_prev;
	//		return tmp;
	//	}

	//	T& operator*()
	//	{
	//		return _node->_data;
	//	}

	//	T* operator->()
	//	{
	//		return &_node->_data;
	//	}

	//	bool operator!=(const Self& it)
	//	{
	//		return _node != it._node;
	//	}

	//	bool operator==(const Self& it)
	//	{
	//		return _node == it._node;
	//	}
	//};

	//template <class T>
	//class ListConstIterator
	//{
	//	typedef ListNode<T> Node;
	//	typedef ListConstIterator<T> Self;//名字变得简短

	//public:
	//	Node* _node;//定义一个节点指针

	//	ListConstIterator(Node* node)
	//		:_node(node)
	//	{}

	//	//前置:返回之后的值
	//	//++it;//返回与自己一样的类型
	//	Self& operator++()
	//	{
	//		_node = _node->_next;
	//		return *this;
	//	}

	//	Self& operator--()
	//	{
	//		_node = _node->_prev;
	//		return *this;
	//	}

	//	//后置:返回之前的值
	//	Self operator++(int)
	//	{
	//		Self tmp(*this);
	//		_node = _node->_next;
	//		return tmp;
	//	}

	//	Self operator--(int)
	//	{
	//		Self tmp(*this);
	//		_node = _node->_prev;
	//		return tmp;
	//	}

	//	// 所以我们要再定义一个类，使用const控制*和->的返回值就可以
	//	const T& operator*()
	//	{
	//		return _node->_data;
	//	}

	//	const T* operator->()
	//	{
	//		return &_node->_data;
	//	}

	//	bool operator!=(const Self& it)
	//	{
	//		return _node != it._node;
	//	}

	//	bool operator==(const Self& it)
	//	{
	//		return _node == it._node;
	//	}
	//};

	template <class T>
	class list
	{
		typedef ListNode<T> Node;
	public:
		//物理空间不是连续的，不符合迭代器的行为，无法遍历
		//typedef Node* iterator;

		//规范命名
		//typedef ListIterator<T> iterator;
		//typedef ListConstIterator<T> const_iterator;

		typedef ListIterator<T, T&, T*> iterator;
		typedef ListIterator<T, const T&, const T*> const_iterator;

		iterator begin()
		{
			//iterator it(_head->_next);
			//return it;
			return iterator(_head->_next);//使用匿名对象
		}

		iterator end()
		{
			return iterator(_head);
		}

		const_iterator begin()const
		{
			return const_iterator(_head->_next);
		}

		const_iterator end()const
		{
			return const_iterator(_head);
		}

		//空初始化，申请哨兵位头节点
		void empty_init()
		{
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;
		}

		list()
		{
			empty_init();
		}

		//拷贝构造:直接复用尾插，前提要有哨兵位头节点
		//lt2(lt1)
		list(const list<T>& lt)
		{
			empty_init();

			//注意：使用范围for时加上const和&
			for (const auto& e : lt)
			{
				push_back(e);
			}
		}

		//initializer_list 构造
		list(initializer_list<T> il)
		{
			empty_init();

			for (const auto& e : il)
			{
				push_back(e);
			}
		}

		//赋值拷贝
		//lt1 = lt3
		list<T>& operator=( list<T> lt)
		{
			swap(_head, lt._head);

			return *this;
		}

		//析构：销毁整个链表
		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}

		//清空当前数据 留头节点，其余节点释放
		void clear()
		{
			auto it = begin();
			while (it != end())
			{
				//返回删除节点的下一个节点的迭代器
				it = erase(it);
			}
		}

		//尾插:end的下一个位置
		void push_back(const T& x)
		{
			//Node* newnode = new Node(x);//申请节点并且初始化
			//Node* tail = _head->_prev;

			////链接节点
			//tail->_next = newnode;
			//newnode->_prev = tail;
			//_head->_prev = newnode;
			//newnode->_next = _head;

			insert(end(), x);
		}

		//尾删
		void pop_back()
		{
			erase(--end());
		}

		//头插:在begin前面插入
		void push_front(const T& x)
		{
			insert(begin(), x);
		}

		//头删
		void pop_front()
		{
			erase(begin());
		}

		//注意:链表的insert没有迭代器失效问题，因为没有扩容的概念
		//pos位置的节点不会改变。
		//但是STL库里insert也给了返回值，返回的是新插入位置的迭代器。
		iterator insert(iterator pos, const T& x)
		{
			Node* cur = pos._node;//找到当前节点
			Node* newnode = new Node(x);//申请节点
			Node* prev = cur->_prev;//找到前一个节点

			//prev newnode cur 进行链接
			newnode->_next = cur;
			cur->_prev = newnode;
			prev->_next = newnode;
			newnode->_prev = prev;

			return iterator(newnode);
		}

		//注意:链表的erase后有迭代器失效问题，pos失效了，
		// 因为pos指向的节点被释放了。
		//所以也有返回值，返回的是删除节点的下一个节点的迭代器
		iterator erase(iterator pos)
		{
			assert(pos != end());//防止删除头节点

			Node* cur = pos._node;//找到当前节点
			Node* prev = cur->_prev;//找到前一个节点
			Node* next = cur->_next;//找到后一个节点

			prev->_next = next;
			next->_prev = prev;

			delete cur;

			return iterator(next);
		}

	private:
		Node* _head;
	};

	//const迭代器的使用
	//引用传参，一般建议加const，就出现了一个const链表
	void Func(const list<int>& lt1)
	{
		//const迭代器不是在普通迭代器前面加const,即不是const iterator
		//const 迭代器目的:本身可以修改，指向的内容不能修改，类似const T* p
		//所以我们要再定义一个类，控制*和->的返回值就可以
		//const list<int>::iterator it = it.begin();//err 这样使it本身也不能++了

		list<int>::const_iterator it = lt1.begin();
		while (it != lt1.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;
	}

	void test_list1()
	{
		list<int> lt1;
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);

		Func(lt1);

		list<int>::iterator it = lt1.begin();
		while (it != lt1.end())
		{
			*it += 10;

			cout << *it << " ";
			++it;
		}
		cout << endl;

		for (auto e : lt1)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	struct Pos
	{
		int _row;
		int _col;

		Pos(int row = 0,int col = 0)
			:_row(row)
			,_col(col)
		{}
	};

	void test_list2()
	{
		list<Pos> lt1;
		lt1.push_back(Pos(100, 100));//使用匿名对象
		lt1.push_back(Pos(200, 200));
		lt1.push_back(Pos(300, 300));

		list<Pos>::iterator it = lt1.begin();//这里的it是Pos*,是结构体指针
		while (it != lt1.end())
		{
			//cout << *it << " ";//err
			//因为这里的*it返回的是Pos自定义类型，而访问自定义类型需要
			//重载流插入，这里并没有重载，所以报错。

			//cout << (*it)._row << ":" << (*it)._col << endl;//ok
			//*it就是Pos结构体，再用.操作符访问成员

			cout << it->_row << ":" << it->_col << endl;//ok
			//cout << it.operator->()->_row << ":" << it.operator->()->_col << endl;//ok
			//其实这里严格来说是两个箭头,第一个运算符重载的调用it.operator->()返回的是Pos*
			//第二个箭头才是原生指针，Pos*再用箭头访问。为了可读性，省略了一个->。

			++it;
		}
		cout << endl;
	}

	void test_list3()
	{
		list<int> lt1;
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);
		Func(lt1);

		lt1.push_front(10);
		lt1.push_front(20);
		lt1.push_front(30);
		Func(lt1);

		lt1.pop_back();
		lt1.pop_back();
		Func(lt1);

		lt1.pop_front();
		lt1.pop_front();
		Func(lt1);
	}

	void test_list4()
	{
		list<int> lt1;
		lt1.push_back(1);
		lt1.push_back(2);
		lt1.push_back(3);
		lt1.push_back(4);
		Func(lt1);

		list<int> it2(lt1);

		lt1.push_back(6);
		Func(lt1);
		Func(lt1);

		list<int> lt3;
		lt3.push_back(10);
		lt3.push_back(20);
		lt3.push_back(30);
		lt1 = lt3;
		Func(lt1);
		Func(lt3);
	}

	void test_list5()
	{
		list<int> lt1 = { 1,2,3,4,5,6 };
		Func(lt1);
	}
}
