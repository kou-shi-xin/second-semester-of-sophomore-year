#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
using namespace std;

//析构与构造的顺序问题

class A
{
public:
	A()
	{
		cout << "A()" << endl;
	}

	~A()
	{
		cout << "~A()" << endl;
	}

private:
	int _a1;
	int _a2;
};

//如果是全局变量，则进入main函数之前就构造了
A aa0;

void f2()
{
	//静态局部变量，则在第一次调用它时才构造，第二次调用时不会构造了
	//说明它只初始化一次
	static A aa3;
}

int main()
{
	//aa1和aa2谁先构造，谁先析构呢？
	//aa1和aa2在栈帧中，它们满足"后进先出"的顺序，即后定义的先析构
	//构造顺序：aa1 aa2
	//析构顺序：aa2 aa1
	A aa1;
	A aa2;

	f2();
	cout << "1111111" << endl;
	cout << "1111111" << endl;
	f2();

	return 0;
}