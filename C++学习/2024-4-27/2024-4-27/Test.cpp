#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
using namespace std;

//const成员函数

class Date
{
public:
	Date(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	//d1.Print(&d1); d1对象类型是 const Date*  只读
	//Date* const this  可读可写  权限放大了
	//void Print()  
	//{
	//	cout << _year << "-" << _month << "-" << _day << endl;
	//}

	void Print() const //const Date* const this,const修饰*this，本质改变this的类型
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}

private:
	int _year;
	int _month;
	int _day;
};

//int main()
//{
//	const Date d1(2024, 4, 25);
//
//	d1.Print();
//
//	//Date d2(2024, 5, 1);//d2是非const对象
//	//d2.Print();//调用Print就是权限的缩小
//
//	//所以const成员函数是有优势的，不管是成员对象，还是非成员对象，都可以调用
//
//	return 0;
//}


//取地址及const取地址操作符重载
//取出的是this指针的地址
//我们不显式实现，编译器会默认自动实现
//这两个默认成员函数一般不显式定义，除非不想让该类型的对象被取地址，
// 就可以自己定义，返回指定地址（如空地址或是假地址） 
class A
{
public:
	A* operator&()
	{
		cout << "A* operator&()" << endl;
		return this;
	}

	const A* operator&() const
	{
		cout << "const A* operator&() const" << endl;
		return this;
	}

private:
	int _a = 1;
	int _b = 2;
	int _c = 3;
};

int main()
{
	A aa1;
	const A aa2;

	cout << &aa1 << endl;
	cout << &aa2 << endl;

	return 0;
}