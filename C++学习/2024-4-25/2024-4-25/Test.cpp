#define _CRT_SECURE_NO_WARNINGS 

#include <stdbool.h>

#include <iostream>
using namespace std;

// .*运算符的用法：调用成员函数的指针
//class OB
//{
//public:
//	void func()
//	{
//		cout << "void func()" << endl;
//	}
//};
//
////重新定义成员函数指针类型
////注意：typedef void(*)()  PtrFunc; 是错误写法
//typedef  void (OB::* PtrFunc)();
//
//int main()
//{
//	//成员函数要加&才能取到函数指针
//	PtrFunc fp = &OB::func; //定义成员函数指针fp 指向函数func
//
//	OB tmp;//定义OB类对象tmp
//
//	(tmp.*fp)();
//
//	return 0;
//}


//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
////private:
//	int _year;  // 年
//	int _month; // 月
//	int _day;   // 日
//};

//先比年，年大就大，年相等比月，月大就大，年月相等比日，日大就大
//bool Compare(const Date& dt1, const Date& dt2)
//
//bool operator>(const Date& dt1, const Date& dt2)
//{
//	if (dt1._year > dt2._year)
//	{
//		return true;
//	}
//	else if (dt1._year == dt2._year)
//	{
//		if (dt1._month > dt2._month)
//		{
//			return true;
//		}
//	}
//	else if (dt1._month == dt2._month)
//	{
//		if (dt1._day > dt2._day)
//		{
//			return true;
//		}
//	}
//
//	return false;
//}
//
//int main()
//{
//	Date d1(2024, 4, 9);
//	Date d2(2024, 4, 12);
//
//	//日期类的比较
//	//大于
//	//cout << Compare(d1, d2) << endl;
//
//	//显式调用，一般不这样
//	//cout << operator>(d1, d2) << endl;
//
//	//可以像内置类型一样直接写，编译器会转换成 operator>(d1, d2)这个调用
//   //注意：括号不要漏写
//	cout << (d1 > d2) << endl;
//
//	return 0;
//}


//通过上面的例子我们知道，如果把运算符重载成全局，就要把 private 屏蔽，
//否则无法访问私有成员，但是我们一般把成员变量设置为私有
//如何解决呢？

//1.提供这些函数的get和set
//2.使用友元 （后面会讲）
//3.重载为成员函数（一般使用这种）

/////////////////////////////////////////////////////////////////////////////
//class Date
//{
//public:
//	Date(int year , int month , int day )
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	//d3.operator== (d4)
//	//d3传给了隐含的this指针，d是d4的别名
//	 bool operator== (const Date & d)
//	 {
//		 return _year == d._year
//			 && _month == d._month
//			 && _day == d._day;
//
//	 }
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;  // 年
//	int _month; // 月
//	int _day;   // 日
//};

//但是如果全局重载（没有private时)和成员函数同时存在，编译不会报错，
// 调用时会默认调成员函数，相当于全局重载没有任何意义了。
//int main()
//{
//	Date d3(2024, 4, 12);
//	Date d4(2024, 4, 15);
//
//	//显式调用
//	//cout << d3.operator== (d4) << endl;
//
//	//转换调用 等价于d3.operator== (d4) 汇编代码
//	//注意：如果是两个操作数，从左到右的参数是对应的，不能颠倒位置
//	cout << (d3 == d4) << endl;
//
//	return 0;
//}

////////////////////////////////////////////////////////////////////
//赋值运算符重载
class Date
{
public:
	Date(int year = 2024, int month = 4, int day = 21)
	{
		//cout << "Date(int year = 2024, int month = 4, int day = 21)" << endl;

		_year = year;
		_month = month;
		_day = day;
	}

	//拷贝构造
	Date(const Date& d)
	{
		//检测是否调用拷贝构造，方便观察
		cout << "Date(const Date& d)" << endl;

		_year = d._year;
		_month = d._month;
		_day = d._day;
	}

	//会带返回值:目的是为了连续拷贝
	////d1 = d2 = d3; //比如这里，d是d3的别名，从右往左，先d2 = d3,返回值是d2
	Date& operator=(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;

		//这里的d2的地址是this ，*this就是d2
		return *this;
	}

	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}

	~Date()
	{
		cout << "~Date()" << endl;
		_year = -1;
		_month = -1;
		_day = -1;
	}

private:
	int _year;  // 年
	int _month; // 月
	int _day;   // 日
};

int main()
{
	Date d1(2024, 4, 12);

	//拷贝构造
	//一个已经存在的对象，拷贝给另一个要创建初始化的对象
	Date d2 = d1;

	Date d3(2024, 5, 1);

	//赋值拷贝/赋值重载
	//一个已经存在的对象，拷贝赋值给另一个已经存在的对象
	//d1 = d3;

	//连续赋值
	d1 = d2 = d3;

	return 0;
}

//Date func()
//{
//	Date d;
//	return d;
//}
//
//int main()
//{
//	func();
//
//	return 0;
//}

