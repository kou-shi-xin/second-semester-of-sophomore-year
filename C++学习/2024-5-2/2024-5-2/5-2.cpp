#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
using namespace std;

#include <stdbool.h>

class Date
{
public:
    Date(int year = 0, int month = 0, int day = 0)
    {
        _year = year;
        _month = month;
        _day = day;
    }

    friend Date GetDate(int n);

    int GetMonthDay(int year, int month)
    {
        static int MonthDay[13] = { 0, 31, 28, 31, 30, 31, 30, 31,31, 30, 31, 30, 31 };

        if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
        {
            return 29;
        }
        else {
            return MonthDay[month];
        }
    }

    bool operator<(const Date& d)
    {
        if (_year < d._year)
        {
            return true;
        }
        else if (_year == d._year)
        {
            if (_month < d._month)
            {
                return true;
            }
            else if (_month == d._month)
            {
                if (_day < d._day)
                {
                    return true;
                }
            }
        }
        return false;
    }

    bool operator==(const Date& d)
    {
        return _year == d._year &&
            _month == d._month &&
            _day == d._day;
    }

    bool operator!=(const Date& d)
    {
        return !(*this == d);
    }

    Date& operator+=(int day)
    {
        _day += day;

        while (_day > GetMonthDay(_year, _month))
        {
            _day -= day;
            ++_month;
            if (_month == 13)
            {
                ++_year;
                _month = 1;
            }
        }
        return *this;
    }

    Date& operator++()
    {
        *this += 1;
        return *this;
    }

   /* Date& operator++()
    {
        _day++;
        if (_day > GetMonthDay(_year, _month))
        {
            _month++;
            _day = 1;
            if (_month == 13)
            {
                _month = 1;
                _year++;
            }
        }
        return *this;
    }*/

    int operator-(const Date& d)
    {
        Date max = *this;
        Date min = d;

        if (*this < d)
        {
            max = d;
            min = *this;
        }

        int n = 0;
        while (max != min)
        {
            ++n;
            ++min;
        }
        return n;
    }

private:
    int _year;
    int _month;
    int _day;
};

Date GetDate(int n)
{
    Date d;
    d._year = n / 10000;
    d._day = n % 100;
    d._month = (n - d._year * 10000) / 100;

    return d;
}

int main()
{
    int d1, d2;
    while (cin >> d1 >> d2)
    {

        Date D1 = GetDate(d1);
        Date D2 = GetDate(d2);

        cout << (D1 - D2) << endl;
    }

}


//#include <iostream>
//using namespace std;
//
//int main() {
//    int year, month, day;
//
//    //这个时间相对于这年1月1日的天数
//    int month_days[13] = { 0 ,31, 59, 90 ,120 ,151, 181, 212, 243 ,273 ,304, 334, 365 };
//
//    int days = 0;
//    while (cin >> year >> month >> day)
//    {
//        days = month_days[month - 1] + day;
//
//        if (month > 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
//        {
//            days += 1;
//        }
//        cout << days << endl;
//    }
//
//    return 0;
//}
// 64 位输出请用 printf("%lld")


//#include <regex>
//class Sum
//{
//public:
//    //调用n次构造
//    Sum()
//    {
//        _ret += _i;
//        _i++;
//    }
//
//    //要拿到私有的静态成员变量，就要用静态成员函数
//    static int GetRet()
//    {
//        return _ret;
//    }
//
//private:
//    static int _i;
//    static int _ret;
//};
//
//int Sum::_i = 1;
//int Sum::_ret = 0;
//
//class Solution {
//public:
//    int Sum_Solution(int n) {
//        //定义变长数组
//        Sum arr[n];
//
//        return Sum::GetRet();
//    }
//};


