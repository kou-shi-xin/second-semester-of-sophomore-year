#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
using namespace std;

#include <stdbool.h>



//拷贝构造是构造函数的一种，是一种特殊的构造

//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	//参数只有一个，必须是类类型对象的引用。
//	// 使用传值的方式语法上会引发无穷递归
//	//Date d2(d1); 
//	//Date(Date& d)
//	//{
//	//	cout << "Date(Date& d)" << endl;
//
//	//	//d是d1的别名，隐含的this就是d2,相当于把d1的值拷贝给d2
//	//	_year = d._year;
//	//	_month = d._month;
//	//	_day = d._day;
//	//}
//
//	//用指针就不会形成无穷递归，但此时它就是一个普通构造了，不是拷贝构造。
//	//感觉怪怪的，所以一般用引用
//	//Date(Date* d)
//	//{
//	//	//d是d1的别名，隐含的this就是d2,相当于把d1的值拷贝给d2
//	//	_year = d->_year;
//	//	_month = d->_month;
//	//	_day = d->_day;
//	//}
//
//	//一般会在参数列表加const,防止发生下面的情况
//	/*Date(Date& d)
//	{
//		d._year = _year  ;
//		d._month = _month ;
//		d._day = _day ;
//	}*/
//
//	//加上const之后是权限的缩小
//	//Date d3(d2);
//	//Date(const Date& d)//d是d2的别名，d只读,d2可读可写
//	//{
//	//	_year = d._year  ;
//	//	_month =d. _month ;
//	//	_day = d._day ;
//	//}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;  // 年
//	int _month; // 月
//	int _day;   // 日
//};

//int main()
//{
//	Date d1(2024,4,21);
//	
//	Date d2(d1);
//	d2.Print();
//
//	return 0;
//}

//int main()
//{
//	Date d2(2024, 4, 21);
//	//d2.Print();
//	//Date d3(&d2);
//	//d3.Print();
//
//	Date d3(d2);//这句代码也等价于Date d3 = d2;也是拷贝构造的写法
//	d3.Print();
//
//	return 0;
//}

//说明自定义类型传值传参要调用拷贝构造，内置类型就会直接拷贝.
//如何让它不调用拷贝构造呢？
//1.传指针
//2.传引用

//void func(Date d)
//void func(Date* d)
// 
//void func(Date& d)//d是d1的别名
//{
//	d.Print();
//	//d->Print();
//}
//
//int main()
//{
//	Date d1(2024, 4, 12);
//	//调用func之前会先进入拷贝构造函数
//	func(d1);
//	//func(&d1);
//
//	return 0;
//}

//class Time
//{
//public:
//	Time()
//	{
//		_hour = 1;
//		_minute = 1;
//		_second = 1;
//	}
//	Time(const Time& t)
//	{
//		_hour = t._hour;
//		_minute = t._minute;
//		_second = t._second;
//		cout << "Time::Time(const Time&)" << endl;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};

//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	// 基本类型(内置类型)
//	int _year = 1970;
//	int _month = 1;
//	int _day = 1;
//	// 自定义类型
//	Time _t;
//};

//int main()
//{
//	Date d1(2024, 4, 23);
//
//	// 用已经存在的d1拷贝构造d2，此处会调用Date类的拷贝构造函数
//	// 但Date类并没有显式定义拷贝构造函数，则编译器会给Date类生成一个默认的拷贝构造函数
//		Date d2(d1);
//		d2.Print();
//
//	return 0;
//}

//typedef int DataType;
//class Stack
//{
//public:
//	Stack(size_t capacity = 3)
//	{
//		_array = (DataType*)malloc(sizeof(DataType) * capacity);
//		if (NULL == _array)
//		{
//			perror("malloc申请空间失败!!!");
//			return;
//		}
//		_capacity = capacity;
//		_size = 0;
//	}
//
//	void Push(DataType data)
//	{
//		// CheckCapacity();
//		_array[_size] = data;
//		_size++;
//	}
//
//	//实现栈的深拷贝
//	//Stack st2 = st1;
//	Stack(const Stack& st)
//	{
//		_array = (DataType*)malloc(sizeof(DataType) * st._capacity);
//		if (NULL == _array)
//		{
//			perror("malloc申请空间失败!!!");
//			return;
//		}
//
//		//开辟好空间后再把值拷贝进去
//		memcpy(_array, st._array, sizeof(DataType) * st._size);
//
//		_size = st._size;
//		_capacity = st._capacity;
//	}

	//注意：如果没有显示写析构函数，编译器也会自动生成。
	//自动生成的析构对内置类型不做处理，自定义类型才会去调用它的析构
//	~Stack()
//	{
//		if (_array)
//		{
//			free(_array);
//			_array = NULL;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//
//private:
//
//	//比如这里，没有显示写_array就不会free
//	//但是也不会报错，因为内存泄漏不会报错
//	DataType* _array;
//	int _capacity;
//	int _size;
//};


//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	//加上const之后是权限的缩小
//	//Date d3(d2);
//	//Date(const Date& d)//d是d2的别名，d只读,d2可读可写
//	//{
//	//	_year = d._year;
//	//	_month = d._month;
//	//	_day = d._day;
//	//}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;  // 年
//	int _month; // 月
//	int _day;   // 日
//};

//没有写拷贝构造时，编译器会自动默认生成拷贝构造，并且完成拷贝。
//那是不是可以不用写拷贝构造呢？再试试 栈：
// 
//运行结果：完成了拷贝，但程序崩溃！
//原因：当栈调用默认生成的拷贝构造函数时，这种函数进行的是浅拷贝(值拷贝)
//      本质是按字节进行拷贝的。这可能会导致两个对象指向同一块空间，如_array,
//      当st1 和st2生命周期结束时，两个对象会分别析构，就相当于释放了两次。 
//      常见的数据结构，栈，队列，链表，树等都有这个问题。 

//解决方案：
// 深拷贝：当有指针指向资源时，会开辟建立一块和要拷贝的一模一样的空间，形状，再进行拷贝

//int main()
//{
//	Stack st1;
//	st1.Push(1);
//	st1.Push(1);
//	st1.Push(1);
//	
//	Stack st2 = st1;
//
//	return 0;
//}


// .*运算符的用法：调用成员函数的指针
//class OB
//{
//public:
//	void func()
//	{
//		cout << "void func()" << endl;
//	}
//};
//
////重新定义成员函数指针类型
////注意：typedef void(*)()  PtrFunc; 是错误写法
//typedef  void (OB::* PtrFunc)();
//
//int main()
//{
//	//成员函数要加&才能取到函数指针
//	PtrFunc fp = &OB::func; //定义成员函数指针fp 指向函数func
//
//	OB tmp;//定义OB类对象tmp
//
//	(tmp.*fp)();
//
//	return 0;
//}


//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
////private:
//	int _year;  // 年
//	int _month; // 月
//	int _day;   // 日
//};

//先比年，年大就大，年相等比月，月大就大，年月相等比日，日大就大
//bool Compare(const Date& dt1, const Date& dt2)

//bool operator>(const Date& dt1, const Date& dt2)
//{
//	if (dt1._year > dt2._year)
//	{
//		return true;
//	}
//	else if (dt1._year == dt2._year)
//	{
//		if (dt1._month > dt2._month)
//		{
//			return true;
//		}
//	}
//	else if (dt1._month == dt2._month)
//	{
//		if (dt1._day > dt2._day)
//		{
//			return true;
//		}
//	}
//
//	return false;
//}

//int main()
//{
//	Date d1(2024, 4, 9);
//	Date d2(2024, 4, 12);
//
//	//日期类的比较
//	//大于
//	//cout << Compare(d1, d2) << endl;
//
//	//显式调用，一般不这样
//	//cout << operator>(d1, d2) << endl;
//
//	//可以像内置类型一样直接写，编译器会转换成 operator>(d1, d2)这个调用
//	cout << (d1 > d2) << endl;
//
//	return 0;
//}


//通过上面的例子我们知道，如果把运算符重载成全局，就要把 private 屏蔽，
//否则无法访问私有成员，但是我们一般把成员变量设置为私有
//如何解决呢？

//1.提供这些函数的get和set
//2.使用友元 （后面会讲）
//3.重载为成员函数

/////////////////////////////////////////////////////////////////////////////
class Date
{
public:
	Date(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	//这个会报错
	/*bool operator>(const Date& dt1, const Date& dt2)
	{
		if (dt1._year > dt2._year)
		{
			return true;
		}
		else if (dt1._year == dt2._year)
		{
			if (dt1._month > dt2._month)
			{
				return true;
			}
		}
		else if (dt1._month == dt2._month)
		{
			if (dt1._day > dt2._day)
			{
				return true;
			}
		}

		return false;
	}*/

	bool operator>( const Date& d)
	{
		if (_year > d._year)
		{
			return true;
		}
		else if (_year == d._year)
		{
			if (_month > d._month)
			{
				return true;
			}
		}
		else if (_month == d._month)
		{
			if (_day > d._day)
			{
				return true;
			}
		}

		return false;
	}

	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}

private:
	int _year;  // 年
	int _month; // 月
	int _day;   // 日
};

//如果直接把函数重载为成员函数，会报错。
//原因：因为成员函数的第一个参数默认为隐含的this
