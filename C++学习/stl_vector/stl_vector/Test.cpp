#define _CRT_SECURE_NO_WARNINGS 

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;


void test_vector1()
{
	//vector<char> 
	//string
	//区别：vector底层的顺序表是针对所有类型的数组。
	// vector<char> 没有串的概念，每次只能插入一个字符，末尾没有\0
	//string专门针对字符数组，可以插入串，末尾有\0

	vector<int> v1;
	v1.push_back(1);
	v1.push_back(2);
	v1.push_back(3);
	v1.push_back(4);
	v1.push_back(5);
	v1.push_back(6);

	//方式1：下标+[]
	for (size_t i = 0; i < v1.size(); i++)
	{
		cout << v1[i] << " ";
	}
	cout << endl;

	//方式2：迭代器
	vector<int>::iterator it1 = v1.begin();
	while (it1 != v1.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;

	//方式3：范围for
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

}

void test_vector2()
{
	vector<string> v1;//此时v1指向的就是对象数组

	//有名对象
	string s1("张三");
	v1.push_back(s1);

	//匿名对象
	v1.push_back(string("李四"));

	//直接单参数隐式类型转换
	v1.push_back("王五");

	//对名字进行修改
	v1[1] += "你好";

	//加const &
	//范围for的底层其实是*it赋值给e
	//而这里的*it是string类，每次赋值都是深拷贝
	for (const auto& e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector3()
{
	//sort算法的使用
	vector<int> v1;

	v1.push_back(1);
	v1.push_back(25);
	v1.push_back(12);
	v1.push_back(9);
	v1.push_back(0);
	v1.push_back(40);

	//默认排升序
	greater<int> gt1;//降序 >
	less<int> gt2;//升序 <

	//重载了operator()
	cout << gt1(3, 4) << endl;
	cout << gt1.operator()(3, 4) << endl;
	
	//全部排序
	//有名对象使用
	//sort(v1.begin(), v1.end(),gt1);

	//匿名对象使用
	sort(v1.begin(), v1.end(), greater<int>());

	//去头去尾排
	//sort(v1.begin() + 1, v1.end() - 1);

	//对前一半排序
	//sort(v1.begin(), v1.begin() + v1.size() / 2);

	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector4()
{
	vector<int> v1;

	v1.push_back(1);
	v1.push_back(25);
	v1.push_back(12);
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

	v1.insert(v1.begin(), 0);//头插
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

	v1.erase(v1.begin());//头删
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

	v1.insert(v1.begin() + 2, 50);//在中间位置插
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

	int x;
	cin >> x;
	//没有x就不插入，有x在它前面插入
	//find是算法里的函数 
	vector<int>::iterator pos = find(v1.begin(), v1.end(), x);
	if (pos != v1.end())
	{
		v1.insert(pos, 1000);
	}
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	test_vector4();

	return 0;
}