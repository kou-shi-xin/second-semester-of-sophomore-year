#pragma once

#include <assert.h>
#include <vector>
#include <iostream>
using namespace std;

namespace bit
{
	template<class T>
	class vector
	{
	public:
		//正向迭代器
		typedef T* iterator;
		typedef T* const_iterator;

		//正向
		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		const_iterator begin()const
		{
			return _start;
		}

		const_iterator end()const
		{
			return _finish;
		}

		//计算有效数据个数
		size_t size()const
		{
			return _finish - _start;
		}

		//计算当前容量
		size_t capacity()const
		{
			return _end_of_storage - _start;
		}

		//预留空间(扩容)
		void reserve(size_t n)
		{
			//由于开空间后_start指向改变，所以要提前记录原来的有效数据个数
			//以便在新空间中更新_finish的位置
			size_t  old_size = size();

			if (n > capacity())
			{
				T* tmp = new T[n];//开新空间

				if (_start)
				{
					//memcpy对任意类型都是浅拷贝
					//当vector的类型是自定义类型时，如string
					//都会面临隐藏的浅拷贝问题

					//当把原空间里的"2222"拷贝进新空间后
					//delete会先对原空间的每个对象调用析构函数，再把原空间销毁
					//但是此时tmp仍指向那块空间，变成野指针了！
					
					//memcpy(tmp, _start, sizeof(T) * old_size);
					for (size_t i = 0; i < old_size; i++)
					{
						//此时这里的赋值调用的是string的赋值，肯定是深拷贝
						tmp[i] = _start[i];
					}

					delete[] _start;//释放旧空间
				}

				_start = tmp;//改变指向，指向新空间

				//在新空间里更新_finish，_end_of_storage的位置
				_finish = _start + old_size;
				_end_of_storage = _start + n;
			}
		}

		//尾插数据
		void push_back(const T& x)
		{
			if (_finish == _end_of_storage)
			{
				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);
			}

			*_finish = x;
			++_finish;
		}

		//尾删
		void pop_back()
		{
			//断言，确保有数据可删
			assert(size() > 0);
			--_finish;
		}

		//插入数据
		//void insert(iterator pos, const T& x)
		iterator insert(iterator pos, const T& x)
		{
			//断言，判断下标的有效性
			assert(pos >= _start && pos <= _finish);

			//判断是否扩容
			if (_finish == _end_of_storage)
			{
				size_t len = pos - _start;

				size_t newcapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapacity);

				//insert函数的内部迭代器失效问题：类似于野指针问题
				//扩容后pos位置失效，需要重新计算pos
				pos = _start + len;
			}
			//挪动数据再插入
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				--end;
			}

			*(end + 1) = x;
			++_finish;

			//返回新插入那个位置的迭代器
			return pos;
		}

		//void erase(iterator pos)
		iterator erase(iterator pos)
		{
			assert(pos >= _start && pos < _finish);

			iterator end = pos + 1;
			while (end != _finish)
			{
				*(end - 1) = *end;
				++end;
			}
			--_finish;

			//返回删除位置的下一个位置的迭代器
			return pos;
		}

		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_end_of_storage, v._end_of_storage);
		}

		//[]运算符
		T& operator[](size_t pos)
		{
			//断言，避免下标越界
			assert(pos < size());

			return _start[pos];
		}

		//强制生成默认构造
		vector() = default;

		//拷贝构造
		//v2(v1);
		vector(const vector<T>& v)
		{
			//提前预开空间，避免边尾插边扩容
			reserve(v.capacity());

			for (auto e : v)
			{
				//this->push_back(e);
				push_back(e);
			}
		}

		//赋值拷贝
		//v3 = v1;
		vector<T>& operator=(const vector<T> v)
		{
			swap(v);
			return *this;
		}

		//迭代器区间的初始化
		//一个类模版的成员函数可以写成函数模版
		//目的 -- 支持任意容器的迭代器区间初始化
		template <class InputIterator>
		vector(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		//用n个val构造
		//T()是缺省值，注意这里不能给0，因为T可能为自定义类型
		//当T为内置类型的时候，也ok
		//因为C++对内置类型进行了升级，也有构造，为了兼容模版
		vector(size_t n, const T& val = T())
		{
			reserve(n);

			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		vector(int n, const T& val = T())
		{
			reserve(n);

			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		//initializer_list的构造
		vector(initializer_list<T> il)
		{
			reserve(il.size());

			for (auto e:il)
			{
				push_back(e);
			}
		}
			
		~vector()
		{
			if (_start)
			{
				delete[] _start;
				_start = _finish = _end_of_storage = nullptr;
			}
		}

	private:
		iterator _start = nullptr;//指向开始位置的指针
		iterator _finish = nullptr;//指向最后一个位置的下一个位置的指针
		iterator _end_of_storage = nullptr;//容量
	};


	void vector_test1()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);

		for (size_t i = 0; i < v1.size(); i++)
		{
			cout << v1[i] << " ";
		}
		cout << endl;

		v1.pop_back();

		for (size_t i = 0; i < v1.size(); i++)
		{
			cout << v1[i] << " ";
		}
		cout << endl;

		/*auto it1 = v1.begin();
		while (it1 != v1.end())
		{
			cout << *it1 << " ";
			it1++;
		}
		cout << endl;

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;*/
	}

	void vector_test2()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);

		for (size_t i = 0; i < v1.size(); i++)
		{
			cout << v1[i] << " ";
		}
		cout << endl;

		int x;
		cin >> x;
		vector<int>::iterator it = find(v1.begin(), v1.end(), x);

		 it = v1.insert(it, 10);
		//insert以后it这个实参会不会失效呢？ 
		//在扩容的时候一定会导致迭代器失效。
		// 因为虽然在insert内部形参修正了，但是形参的改变不影响实参
		
		//建议失效后的迭代器不要访问，除非使用返回值。
		cout << *it << endl;

		for (size_t i = 0; i < v1.size(); i++)
		{
			cout << v1[i] << " ";
		}
		cout << endl;
	}

	void vector_test3()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);

		for (size_t i = 0; i < v1.size(); i++)
		{
			cout << v1[i] << " ";
		}
		cout << endl;

		/*v1.erase(v1.begin());
		for (size_t i = 0; i < v1.size(); i++)
		{
			cout << v1[i] << " ";
		}
		cout << endl;

		//int x;
		//cin >> x;
		//std::vector<int>::iterator it = find(v1.begin(), v1.end(), x);

		////使用库里的erase，erase之后，迭代器是否失效问题？
		//v1.erase(it);
		//cout << *it << endl;

		/*for (size_t i = 0; i < v1.size(); i++)
		{
			cout << v1[i] << " ";
		}
		cout << endl;*/

		//删除偶数
		vector<int>::iterator it = v1.begin();
		while (it != v1.end())
		{
			if (*it % 2 == 0)
			{
				it = v1.erase(it);
			}
			else
			{
				++it;
			}
		}

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void vector_test4()
	{
		vector<int> v1;
		v1.push_back(1);
		v1.push_back(2);
		v1.push_back(3);
		v1.push_back(4);
		v1.push_back(5);

		vector<int> v2(v1);
		for (auto e : v2)
		{
			cout << e  <<" ";
		}
		cout << endl;

		vector<int> v3 = v1;
		for (auto e : v2)
		{
			cout << e << " ";
		}
		cout << endl;

		//使用迭代器区间初始化
		vector<int> v4(v1.begin()+1, v1.end()-1);
		for (auto e : v4)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void vector_test5()
	{
		int i = 0;
		int j(1);
		int k = int();
		int x = int(3);

		//初始化3个空
		vector<string> v1(3);

		//初始化为3个xx
		vector<string> v2(3, "xx");
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		for (auto e : v2)
		{
			cout << e << " ";
		}
		cout << endl;

		//初始化为3个1
		//vector<int> v3(3, 1);//err 非法的间接寻址.参数匹配问题
		//vector<int> v3(3u, 1);//ok

		//如果非要这样传参数，就需要再重载一个构造
		vector<int> v3(3, 1);
		for (auto e : v3)
		{
			cout << e << " ";
		}
		cout << endl;

		//C++11新增的类模板initializer_list，方便初始化
		// 它的内部其实有两个指针，一个指向第一个值的位置，一个指向最后一个值的下一个位置
		// 并且支持迭代器
		
		//支持被花括号括起的任意个数的值给initializer_list
		//auto il1 = { 1,3,4,5,6,7 };
		//initializer_list<int> il2 = { 1,2,3 };

		//这里的隐式类型转换不一样，参数个数不固定
		vector<int> v4 = { 7,8,9,4,5 };//隐式类型转换
		vector<int> v5({ 4,5,6 });//直接构造
	}

	//隐藏的浅拷贝问题
	void vector_test6()
	{
		vector<string> v1;
		v1.push_back("2222222");
		v1.push_back("2222222");
		v1.push_back("2222222");
		v1.push_back("2222222");
		v1.push_back("2222222");

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;
	}
}

