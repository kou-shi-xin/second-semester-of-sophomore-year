#define _CRT_SECURE_NO_WARNINGS 

#include <assert.h>
#include <algorithm>

#include <iostream>
#include <string>
using namespace std;

void test_string1()
{
	//string()
	string s1;//无参默认构造

	//string(const char* s)
	string s2("hello world");//用字符串构造

	//string(const string& s) 
	string s3(s2);//拷贝构造

	//string (const string& str, size_t pos, size_t len = npos);
	//从str中的pos下标位置，拷贝len个字符
	string s4(s2, 3, 5);
	string s5(s2, 3);//不传第三个参数，默认拷贝到结尾，缺省参数npos是整型最大值


	cout << s1 << endl;
	cout << s2 << endl;
	cout << s3 << endl;
	cout << s4 << endl;
	cout << s5 << endl;

	//cin >> s1;
	//cout << s1 << endl;
}


void test_string2()
{
	//构造
	string s1("hello world");
	//隐式类型转换
	string s2 = "hello world";

	const string& s3 = "hello world";
}

//大概模拟string类说明几个问题
//class string
//{
//public:
//	//引用返回
// //1.减少拷贝
// //2.修改返回对象  //s1[0] = 'x';
//	char& operator[](int i)
//	{
//		assert(i < _size);
//		return _str[i];//返回的是第i个字符的别名
//     //用引用返回：_str[i]出了作用域还在，因为_str开辟在堆上
//     //它返回的是堆上的一个字符的引用别名
//	}
//
//private:
//	char* _str;
//	int _size;
//	int _capacity;
//};


//用下标进行遍历--字符串+顺序表
void test_string3()
{
	string s1("hello world");

	//计算字符个数,不算\0
	cout << s1.size() << endl;

	//改变字符串的内容
	for (int i = 0; i < s1.size(); i++)
	{
		s1[i]++;
	}

	//s1[0] = 'x';

	//越界检查
	//s1[20];//err 直接崩溃

	//遍历字符串并打印
	for (int i = 0; i < s1.size(); i++)
	{
		//string::operator[]
		//cout << s1.operator[](i) << endl;
		cout << s1[i]<<" ";
	}
	cout << endl;

	const string s2("hello");
	//会自动匹配const[]重载,不能修改
	//s2[0] = 'd';//err
}

//用迭代器进行遍历(类似指针的遍历)--字符串+顺序表+链表+二叉树
void test_string4()
{
	string s1("hello world");

	auto it1 = s1.begin();
	//string::iterator it1 = s1.begin();//begin返回开始位置的迭代器
	while (it1 != s1.end()) //end返回结束位置的下一个位置的迭代器
	{
		cout << *it1 << " ";
		++it1;
	}
	cout << endl;

	const string s2("hello world");
	string::const_iterator it2 = s2.begin();
	while (it2 != s1.end())
	{
		//*it2 += s;//err 指向的内容不能修改 本身可以++
		cout << *it2 << " ";
		++it2;
	}
	cout << endl;
}

//用范围for进行遍历--字符串+顺序表+链表+二叉树
void test_string5()
{
	string s1("hello world");
	//自动取出s1中的数据赋给e，自动判断结束，自动++。其实底层就是迭代器。
	for (auto e : s1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_string6()
{
	string s1("hello world");
	cout << s1 << endl;

	//按字典序排序(按ASCII码值排序)
	//用排序函数sort [first last)左闭右开，last传的不是有效数据
	//sort(s1.begin(), s1.end());

	//第一个和最后一个不参与排序
	//sort(++s1.begin(), --s1.end());

	//前5个排序  [0,5)
	sort(s1.begin(), s1.begin() + 5);
	cout << s1 << endl;
}

void test_string7()
{
	string s1("hello world");
	cout << s1 << endl;

	s1.push_back('x');//一个字符一个字符尾插
	cout << s1 << endl;

	s1.append(" yyyyyyy!!");//尾插一个字符串
	cout << s1 << endl;

	string s2("22222");

	//直接尾插
	s1 += 'aaa';
	s1 += 'd';
	s1 += s2;

	cout << s1 << endl;
}

void test_string8()
{
	string s1("hello world");
	cout << s1 << endl;

	//变相赋值，会把原来的字符串改掉
	s1.assign("111111");
	cout << s1 << endl;

	//效率不高 O（N）
	//头插一个字符串
	string s2("hello world");
	s2.insert(0, "aaaaaa");//下标位置  字符串
	cout << s2 << endl;

	//头插一个字符
	s2.insert(0, 1, 'f');//下标位置  插入的个数 字符
	cout << s2 << endl;
}

void test_string9()
{
	string s1("hello world");
	cout << s1 << endl;

	//效率不高 O(N)
	//头删字符
	s1.erase(0, 2);//下标位置  字符个数
	cout << s1 << endl;

	//效率不高 O(N)
	//替换部分字符串
	string s2("hello world");
	s2.replace(5, 1, "%20");
	cout << s2 << endl;
}

void test_string10()
{
	//string str("hello world hello world");

	////计算字符个数
	//cout << str.size() << endl;
	
	////计算str的容量，capacity比实际空间少一个，有一个多的是预留给\0的
	//cout << str.capacity() << endl;

	string s;
	//一般用在知道需要多少空间，提前开好
	s.reserve(100);

	string s1("111111111");
	cout << s1.capacity() << endl;//15

	//扩容
	//reserve只影响capacity，不影响size，即不改变里面的数据
	s1.reserve(100);
	cout << s1.capacity() << endl;//111

	//缩容
	//传的值比当前的capacity小时，
	// vs一般不缩容，g++会缩
	s1.reserve(10);
	cout << s1.capacity() << endl;//15

}

void test_string11()
{
	string s1;
	//s1.resize(5, '0');//有效数据的个数  填充字符
	s1.resize(5);//不传填充字符时，会初始化默认字符\0,ascii为0
	s1[0] = '3';
	s1[1] = '4';
	s1[2] = '5';
	s1[3] = '6';
	s1[4] = '7';

	string s2("hello world");
	s2.resize(20, 'x');//此时会影响capacity+size，两个都会增加

	s2.resize(5);//capacity不变，有效字符个数size减少，输出为hello

}

void test_string12()
{
	//得到指定下标位置的数据
	//与operator[]的区别：
	//下标越界时，operator[]直接粗暴断言，at可以捕获异常
	string s1("hello world");
	cout << s1.at(0) << endl;
}

void test_string13()
{
	string file("string.cpp.zip");
	size_t pos = file.rfind('.');//倒着查找

	//从s1的pos位置开始往后取出n个字符的子串放入s2中，n不传时默认到结尾
	//string s2 = s1.substr(pos);
	//cout << s2 << endl;

	//string suffix(file.substr(pos, file.size() - pos));
	string suffix = file.substr(pos);
	cout << suffix << endl;

	// 取出url中的域名
	string url("http://www.cplusplus.com/reference/string/string/find/");
	size_t pos1 = url.find(':');
	string url1 = url.substr(0, pos1 - 0);
	cout << url1 << endl;//http

	size_t pos2 = url.find('/',pos1 + 3);
	string url2 = url.substr(pos1 + 3, pos2 - (pos1 + 3));
	cout << url2 << endl;//www.cplusplus.com

	string url3 = url.substr(pos2 + 1);
	cout << url3 << endl;//reference/string/string/find/
}

void test_string14()
{
	string s1 = "hello";
	string s2 = "world";

	string ret1 = s1 + s2;
	cout << ret1 << endl;

	string ret2 = s1 + "xxxxx";
	cout << ret2 << endl;

	//+运算符写成全局函数，由于这种情况
	string ret3 = "xxxxx" + s1;
	cout << ret3 << endl;

	//按字典序比较
	cout << (s1 < s2) << endl;
}

int main()
{
	test_string13();

	return 0;
}

//int main()
//{
//	string str;
//
//	//cin 遇到空格或者换行会停止提取
//	//cin >> str;
//
//	//获取一行包含空格的字符串
//	getline(cin, str);
//
//	int pos = str.rfind(' ');
//	cout << str.size() - (pos + 1) << endl;
//
//	return 0;
//}

//int main()
//{
//	string str;
//	//如何停止输入？
//	//ctrl+c
//	//ctrl+z+空格
//	while (cin >> str)
//	{
//		cout << str << endl;
//	}
//
//	return 0;
//}

//int main()
//{
//	int x = 0, y = 0;
//	cin >> x >> y;
//	//整形转字符串
//	string str = to_string(x + y);
//	cout << str << endl;
//
//	int aa = stoi(str);
//	cout << aa << endl;
//
//	return 0;
//}