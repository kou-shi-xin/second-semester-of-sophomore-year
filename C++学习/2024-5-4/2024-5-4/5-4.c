#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

//typedef struct {
//    int* a;
//    int front;//指向头
//    int rear;//指向尾的下一个
//    int k;//元素个数
//
//} MyCircularQueue;
//
//MyCircularQueue* myCircularQueueCreate(int k)
//{
//    MyCircularQueue* obj = (MyCircularQueue*)malloc(sizeof(MyCircularQueue));
//    //多开一个空间解决假溢出问题
//    obj->a = (int*)malloc(sizeof(int) * (k + 1));
//    //正是由于rear指向尾的下一个，所以才可以都初始化0
//    obj->front = 0;
//    obj->rear = 0;
//    obj->k = k;
//
//    return obj;
//}
//
//bool myCircularQueueIsFull(MyCircularQueue* obj) {
//
//    //注意：rear为右边界时，判断是否满要进行回绕
//    return (obj->rear + 1) % (obj->k + 1) == obj->front;
//}
//
//bool myCircularQueueIsEmpty(MyCircularQueue* obj) {
//    return obj->front == obj->rear;
//}
//
//bool myCircularQueueEnQueue(MyCircularQueue* obj, int value) {
//    //在尾部入数据
//    //满了,插入失败
//    if (myCircularQueueIsFull(obj))
//    {
//        return false;
//    }
//    else
//    {
//        //没满，放在rear的位置，rear++
//        //注意：rear为右边界时，要回绕
//        obj->a[obj->rear++] = value;
//        obj->rear %= (obj->k + 1);
//        return true;
//    }
//}
//
//bool myCircularQueueDeQueue(MyCircularQueue* obj) {
//    //这是队列，取出队头数据后，删除该位置上的
//    //为空，删除失败
//    if (myCircularQueueIsEmpty(obj))
//        return false;
//    else
//    {
//        obj->front++;
//        //front在右边界时，front%(k+1)进行回绕
//        obj->front %= (obj->k + 1);
//        return true;
//    }
//
//}
//
//int myCircularQueueFront(MyCircularQueue* obj) {
//    if (myCircularQueueIsEmpty(obj))
//        return -1;
//    else
//        return obj->a[obj->front];//不管何时，front都是指向队头
//}
//
//int myCircularQueueRear(MyCircularQueue* obj) {
//    if (myCircularQueueIsEmpty(obj))
//        return -1;
//
//    //一般情况下，rear-1就是队尾元素
//    //当rear在左边界，rear-1 + k+1，rear-1为负，向左越界，rear-1 + k+1回绕到右边界
//    //（rear-1 + k+1）%（k+1）避免在一般情况下时rear-1 + k+1越界
//    return obj->a[(obj->rear - 1 + obj->k + 1) % (obj->k + 1)];
//}
//
//void myCircularQueueFree(MyCircularQueue* obj) {
//    free(obj->a);
//    free(obj);
//}


//typedef char STQType;
//typedef struct Stack
//{
//    STQType* a;
//    int top;
//    int capacity;
//}ST;
//
//void STInit(ST* ps)
//{
//    ps->a = (STQType*)malloc(sizeof(STQType) * 4);
//    ps->top = 0;//指向栈顶元素的下一个位置
//    ps->capacity = 4;
//}
//
//void STDestroy(ST* ps)
//{
//    free(ps->a);
//    ps->a == NULL;
//    ps->top = 0;
//    ps->capacity = 0;
//}
////栈顶插入元素
//void STPush(ST* ps, STQType x)
//{
//    assert(ps);
//    //检查扩容
//    if (ps->top == ps->capacity)
//    {
//        ps->a = (STQType*)realloc(ps->a, sizeof(STQType) * ps->capacity * 2);
//        ps->capacity *= 2;
//    }
//
//    ps->a[ps->top++] = x;
//}
////获取栈顶元素
//STQType STtop(ST* ps)
//{
//    assert(ps && ps->top > 0);
//    return ps->a[ps->top - 1];
//}
////删除栈顶元素
//void STPop(ST* ps)
//{
//    assert(ps && ps->top > 0);
//    ps->top--;
//}
////判断栈是否为空，为空返回非零，不为空返回0
//bool STEmpty(ST* ps)
//{
//    assert(ps);
//    return ps->top == 0;
//}
//
////用两个栈实现队列
////一个栈专门入数据，一个栈专门出数据
//
//typedef struct {
//    ST pushst;
//    ST popst;
//
//} MyQueue;
//
//
//MyQueue* myQueueCreate() {
//    MyQueue* obj = (MyQueue*)malloc(sizeof(MyQueue));
//    //初始化队列
//    STInit(&obj->pushst);
//    STInit(&obj->popst);
//
//    return obj;//别忘记返回值
//}
//
//void myQueuePush(MyQueue* obj, int x) {
//    //void STPush(ST* ps, STQType x)
//    STPush(&obj->pushst, x);
//}
//
//int myQueuePeek(MyQueue* obj) {
//    if (STEmpty(&obj->popst))
//    {
//        //把pushst中的元素倒入popst中
//        while (!STEmpty(&obj->pushst))
//        {
//            int top = STtop(&obj->pushst);
//            STPop(&obj->pushst);
//            STPush(&obj->popst, top);
//        }
//    }
//
//    int top = STtop(&obj->popst);
//    return top;
//}
//
//int myQueuePop(MyQueue* obj) {
//    //删除队头数据
//    //如果Popst无数据，要先倒入再删
//    //如果有数据直接删
//    //所以直接复用myQueuePeek,既可以导数据，有可以获取队头元素
//    int front = myQueuePeek(obj);
//    STPop(&obj->popst);
//
//    return front;
//}
//
//bool myQueueEmpty(MyQueue* obj) {
//    return STEmpty(&obj->pushst) && STEmpty(&obj->popst);
//}
//
//void myQueueFree(MyQueue* obj) {
//    STDestroy(&obj->pushst);
//    STDestroy(&obj->popst);
//
//    free(obj);
//}

/**
 * Your MyQueue struct will be instantiated and called as such:
 * MyQueue* obj = myQueueCreate();
 * myQueuePush(obj, x);

 * int param_2 = myQueuePop(obj);

 * int param_3 = myQueuePeek(obj);

 * bool param_4 = myQueueEmpty(obj);

 * myQueueFree(obj);
*/

