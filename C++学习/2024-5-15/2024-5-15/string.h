#pragma once

#include <iostream>
#include <assert.h>
#include <stdbool.h>

using namespace std;

namespace bit
{
	class string
	{
	public:
		//迭代器
		typedef char* iterator;

		iterator begin();
		iterator end();

		//构造，在声明处全缺省
		string(const char* str = "");
		//析构
		~string();

		//拷贝构造
		string(const string& s);

		//赋值运算符重载
		string& operator=(const string& s);

		bool operator==(const string& s)const;
		bool operator!=(const string& s)const;

		size_t size()const;

		char& operator[](size_t pos)const;

		const char* c_str() const;

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};

	ostream& operator<< (ostream& os, const string& str);
	istream& operator>> (istream& is, string& str);
}
