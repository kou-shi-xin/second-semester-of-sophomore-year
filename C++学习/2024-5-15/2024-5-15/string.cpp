#define _CRT_SECURE_NO_WARNINGS 

#include "string.h"

namespace bit
{
	//构造，在声明处全缺省
	string::string(const char* str)
		:_size(strlen(str))
	{
		_str = new char[_size + 1];
		_capacity = _size;
		strcpy(_str, str);
	}

	//深拷贝
	//s2(s1);
	string::string(const string& s)
	{
		_str = new char[s._capacity + 1];
		strcpy(_str, s._str);
		
		_size = s._size;
		_capacity = s._capacity;
	}

	string::iterator string::begin()
	{
		return _str;
	}

	string::iterator string::end()
	{
		return _str + _size;
	}

	bool string::operator==(const string& s)const
	{
		return strcmp(_str, s._str) == 0;
	}

	bool string::operator!=(const string& s)const
	{
		return !(*this == s);
	}

	//s3 = s1;
	string& string::operator=(const string& s)
	{
		//开新空间，拷贝数据，释放原空间，改变指针指向
		if (this != &s)
		{
			char* tmp = new char[s._capacity + 1];
			strcpy(tmp, s._str);
			delete[] s._str;
			_str = tmp;
			_size = s._size;
			_capacity = s._capacity;

			return *this;
		}
	}

	const char* string::c_str() const
	{
		return _str;
	}

	size_t string::size()const
	{
		return _size;
	}

	char& string::operator[](size_t pos)const
	{
		for (size_t i = pos; i < _size; i++)
		{
			return _str[i];
		}
	}

	/*istream& operator>> (istream& is, string& str)
	{
		char ch = is.get();
		while (ch != ' ' && ch != '\n')
		{
			str += ch;
			ch = is.get();
		}

		return is;
	}*/

	ostream& operator<< (ostream& os, const string& str)
	{
		for (size_t i = 0; i < str.size(); i++)
		{
			os << str[i];
		}

		return os;
	}

	//析构
	string::~string()
	{
		delete[]  _str;
		_str = nullptr;
		_size = _capacity = 0;
	}
}