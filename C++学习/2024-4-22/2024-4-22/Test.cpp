#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
using namespace std;

//class Date
//{
//public:
//	/*Date()
//	{
//
//	}*/
//	//无参构造函数：类似Init函数的功能，无返回值，不需要写void 
//	/*Date()
//	{
//		_year = 1;
//		_month = 1;
//		_day = 1;
//	}*/
//
//	/*Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}*/
//
//	//全缺省构造函数
//	//注意：与无参的构造函数也是重载，但调用时会衬产生歧义
	/*Date(int year =1 , int month =1 , int day = 1 )
	{
		_year = year;
		_month = month;
		_day = day;
	}*/
//
//	/*void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}*/
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;  // 年
//	int _month; // 月
//	int _day;   // 日
//};


//int main()
//{
//	//没有传参的构造函数
//	//注意：这里的无参构造 对象前面没有括号，为了跟函数声明区分
//	//Date d1(); //err
//
//	Date d1;
//	d1.Print();
//
//	//Date d2( 2024, 4, 22);
//	//d2.Print();
//
//	return 0;
//}


class A
{
public:
	//err  当没有无参构造时，就会报错
	/*A(int a)
	{
		_a = 0;
		cout << "A()" << endl;
	}*/

	//如果连显示写的无参构造函数都没有，也会有编译器默认的无参构造
	//此时也要看这里有没有自定义类型(就像套娃),这里只有内置类型，所以还是不做处理，初始化随机值
	/*A()
	{
		_a = 0;
		cout << "A()" << endl;
	}*/

	A()
	{
		_a = 0;
		cout << "A()" << endl;
	}

private:
	int _a;

};

//当我们没有显示写时，编译器会自动生成构造函数
//但是这种构造函数会默认初始化成随机值，这是为什么呢？有这样的规定：
//内置类型/基本类型： int/char/double.....指针
//自定义类型：     class/struct...
//编译器自动生成的构造函数，对于内置成员变量，没有规定做不做处理(默认初始化为随机值)，有些编译器会处理
//对于自定义类型成员变量，才会调用它的无参构造(不传参就可以调用的那个构造)，如果没有无参构造函数就会报错。


//class Date
//{
//public:
//	
//
//	Date()
//	{
//	
//	}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	int _year;  // 年
//	int _month; // 月
//	int _day;   // 日
//
//	A _aa;
//};
//
//
//int main()
//{
//	Date d;
//	d.Print();
//
//	return 0;
//}


//class Date
//{
//public:
//
//	Date()
//	{
//		
//	}
//
//	/*Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}*/
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//private:
//	
//	int _year ;  // 年
//	int _month ; // 月
//	int _day ;   // 日
//};

//int main()
//{
//	Date d;
//	d.Print();
//
//	return 0;
//}

//struct Stack
//{
//	//成员函数
//	void Init(int n = 4)
//	{
//		arr = (int*)malloc(sizeof(int) * n);
//		if (nullptr == arr)
//		{
//			perror("malloc申请空间失败");
//			return;
//		}
//
//		capacity = n;
//		top = 0;
//
//	}
//
//	//成员变量
//	int* arr;
//	int capacity;
//	int top;
//};
//
//int main()
//{
//	Stack st1;
//	Stack st2;
//	Stack st3;
//
//	return 0;
//}

/////////////////////////////////////////////////////////////////////
typedef int DataType;
class Stack
{
public:
	Stack(size_t capacity = 3)
	{
		cout << "Stack(size_t capacity = 3)" << endl;

		_array = (DataType*)malloc(sizeof(DataType) * capacity);
		if (NULL == _array)
		{
			perror("malloc申请空间失败!!!");
			return;
		}
		_capacity = capacity;
		_size = 0;
	}

	void Push(DataType data)
	{
		// CheckCapacity();
		_array[_size] = data;
		_size++;
	}

	//注意：如果没有显示写析构函数，编译器也会自动生成。
	//自动生成的析构对内置类型不做处理，自定义类型才会去调用它的析构
	~Stack()
	{
		cout << "~Stack()" << endl;

		if (_array)
		{
			free(_array);
			_array = NULL;
			_capacity = 0;
			_size = 0;
		}
	}

private:

	//比如这里，没有显示写_array就不会free
	//但是也不会报错，因为内存泄漏不会报错
	DataType* _array;
	int _capacity;
	int _size;
};

//int main()
//{
//	Stack st;
//
//	return 0;
//}


//class Time
//{
//public:
//	~Time()
//	{
//		cout << "~Time()" << endl;
//	}
//private:
//	int _hour;
//	int _minute;
//	int _second;
//};
//
//class Date
//{
//private:
//
//	// 基本类型(内置类型)
//	int _year = 1970;
//	int _month = 1;
//	int _day = 1;
//
//	// 自定义类型
//	Time _t;
//};
//
//int main()
//{
//	Date d;
//	return 0;
//}
