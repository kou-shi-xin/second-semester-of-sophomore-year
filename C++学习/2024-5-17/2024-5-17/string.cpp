#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
using namespace std;

#include <assert.h>

//传统写法
//class mystring
//{
//public:
//	//构造函数
//	mystring(const char* str)
//		:_size(strlen(str))
//	{
//		_str = new char[_size + 1];
//		_capacity = _size;
//		strcpy(_str, str);
//	}
//
//	//拷贝构造
//	//s2(s1)
//	//s是s1，*this是s2
//	mystring(const mystring& s)
//	{
//		_str = new char[s._capacity + 1];
//		strcpy(_str, s._str);
//		_size = s._size;
//		_capacity = s._capacity;
//	}
//
//	//赋值运算符重载
//	//s3 = s1
//	//s1是s，s3是*this
//	mystring& operator=(const mystring& s)
//	{
//		//开新空间，拷贝数据，释放原空间，改变指针指向
//		char* tmp = new char[s._capacity + 1];
//		strcpy(tmp, s._str);
//		delete[] s._str;
//		_str = tmp;
//		_size = s._size;
//		_capacity = s._capacity;
//
//		return *this;
//	}
//
//	const char* c_str()const
//	{
//		return _str;
//	}
//
//	//析构函数
//	~mystring()
//	{
//		delete[] _str;
//		_str = nullptr;
//		_size = _capacity = 0;
//	}
//
//private:
//	char* _str;
//	size_t _size;
//	size_t _capacity;
//};


//现代写法：在这里效率上并没有提高多少，但是这种写法不仅适用于string类
//后面的链表，树也同样适用
//class mystring
//{
//public:
//	//构造函数
//	mystring(const char* str)
//		:_size(strlen(str))
//	{
//		_str = new char[_size + 1];
//		_capacity = _size;
//		strcpy(_str, str);
//	}
//
//	//拷贝构造
//	//s2(s1)
//	//s是s1，*this是s2
//	mystring(const mystring& s)
//	{
//		//复用构造函数，构造一个tmp，在用tmp对象和this交换
//		//如果不在声明时给缺省值，刚开始s2是随机值，tmp和s2交换后
//		//tmp就是随机值，tmp出了函数会调用析构函数，此时程序可能会崩溃
//		//所以好给缺省值
//		/*mystring tmp(s._str);
//		std::swap(_str, tmp._str);
//		std::swap(_size, tmp._size);
//		std::swap(_capacity, tmp._capacity);*/
//		mystring tmp(s._str);
//
//		//this->swap(tmp);
//		swap(tmp);
//	}
//
//	//赋值运算符重载
//	//s3 = s1
//	//s1是s，s3是*this
//	/*mystring& operator=(const mystring& s)
//	{
//		if (this != &s)
//		{
//			mystring tmp(s._str);
//			swap(tmp);
//		}
//
//		return *this;
//	}*/
//
//	//s3 = s1   这里的传参不能用引用
//	//传值传参会进行拷贝构造，s1会拷贝一份给tmp
//	//再让tmp与s3交换
//	mystring& operator=(mystring tmp)
//	{
//		swap(tmp);
//
//		return *this;
//	}
//
//	const char* c_str()const
//	{
//		return _str;
//	}
//
//	void swap(mystring& s)
//	{
//		std::swap(_str, s._str);
//		std::swap(_size, s._size);
//		std::swap(_capacity, s._capacity);
//	}
//
//	//析构函数
//	~mystring()
//	{
//		delete[] _str;
//		_str = nullptr;
//		_size = _capacity = 0;
//	}
//
//private:
//	char* _str = nullptr;
//	size_t _size = 0;
//	size_t _capacity = 0;
//};
//
//

//int main()
//{
//	mystring s1("hello world");
//	cout << s1.c_str() << endl;
//
//	mystring s2(s1);
//	cout << s2.c_str() << endl;
//
//	mystring s3 = s1;
//	cout << s3.c_str() << endl;
//	
//	return 0;
//}

