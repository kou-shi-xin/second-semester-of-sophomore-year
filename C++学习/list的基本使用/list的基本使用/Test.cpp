#define _CRT_SECURE_NO_WARNINGS 

#include <list>
#include <iostream>
using namespace std;

//list的构造
void test_list1()
{
	list<int> lt1; //构造空的list
	list<int> lt2(4, 100); //用4个100初始化

	list<int> lt3 = { 5,2,6,4,7 }; //用列表初始化
	list<int> lt4(lt3.begin(), lt3.end());//用lt3中的区间初始化

	list<int> lt5(lt3); //拷贝构造

	//用迭代器打印lt3中的元素
	list<int>::iterator it1 = lt3.begin();
	while (it1 != lt3.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;
}

//list迭代器的使用
void test_list2()
{
	list<int> lt1 = { 1,2,3,4,5,6 };

	//正向迭代器
	list<int>::iterator it1 = lt1.begin();
	while (it1 != lt1.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;

	//反向迭代器
	list<int>::reverse_iterator it2 = lt1.rbegin();
	while (it2 != lt1.rend())
	{
		cout << *it2 << " ";
		it2++;
	}
	cout << endl;

	//范围for
	for (const auto& e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;
}

//list的插入和删除
void test_list3()
{
	list<int> lt1 = { 1,2,3,4,5,6 };
	lt1.insert(lt1.begin()++, 30);
	lt1.insert(lt1.begin(), 10);

	//查找3，在3的前面插入20
	auto it1 = find(lt1.begin(), lt1.end(), 3);
	lt1.insert(it1, 20);
	for (const auto& e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;

	auto it2 = lt1.erase(it1);
	for (const auto& e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;
}

//list的头插，头删，尾插，尾删
void test_list4()
{
	list<int> lt1;
	lt1.push_back(0);
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	for (const auto& e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;

	lt1.pop_back();
	lt1.pop_back();
	for (const auto& e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;

	lt1.push_front(10);
	lt1.push_front(20);
	lt1.push_front(30);
	for (const auto& e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;

	lt1.pop_front();
	lt1.pop_front();
	lt1.pop_front();
	for (const auto& e : lt1)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	test_list4();

	return 0;
}