#pragma once

#include <iostream>
#include <assert.h>
#include <stdbool.h>

using namespace std;

//定义一个叫做bit的命名空间，隔离C++库里的string类
 namespace bit
{
	 class string
	 {
	 public:
		 //typedef实现二次封装
		 //由于string类是连续的空间，所以可以定义为原生指针
		 typedef char* iterator;

		 //const迭代器，指针指向的内容不能修改
		 typedef const char* const_iterator;

		 //实现迭代器,一定要实现为begin 和end
		 //迭代器屏蔽了底层细节，提供了一种简单通用的访问容器的方式
		 iterator begin();
		 iterator end();

		 const_iterator begin()const;
		 const_iterator end()const;

		// string();//无参构造
		
	    //有参与无参构造用全缺省进行合并,在声明处给缺省值
		 string(const char* str = "");//传参构造
		 ~string();
		 string(const string& s);

		 //赋值运算重载
		 //string& operator=(const string& s);

		 //现代写法
		 string& operator=(string tmp);


		 const char* c_str() const;

		 //用下标的方式遍历字符串
		 size_t size()const;
		 char& operator[](size_t pos);
		 const char& operator[](size_t pos)const;

		 //用于扩容，一般不缩容
		 void reserve(size_t n);

		 void push_back(char ch);//尾插一个字符
		 void append(const char* str);//尾插字符串

		 //用运算符重载实现尾插
		 string& operator+=(char ch);
		 string& operator+=(const char* str);

		 //在指定位置插入 
		 void insert(size_t pos, char ch);
		 void insert(size_t pos, const char* str);

		 //在指定位置删除长度为len
		 void erase(size_t pos = 0, size_t len = npos);

		 //从pos位置开始找字符或是字符串
		 size_t find(char ch, size_t pos =0);
		 size_t find(const char* str, size_t pos = 0);

		 //交换函数
		 void swap(string& s);

		 //从pos位置找一个子串
		 string substr(size_t pos = 0, size_t len = npos);

		 //字符串的比较
		 bool operator<(const string& s)const;
		 bool operator>(const string& s)const;
		 bool operator<=(const string& s)const;
		 bool operator>=(const string& s)const;
		 bool operator==(const string& s)const;
		 bool operator!=(const string& s)const;

		 //把当前数据清除，但是不清空间
		 void clear();

	 private:
		 char* _str = nullptr;
		 size_t _size = 0;//有效数据个数,指向最后一个有效数据的下一个位置\0
		 size_t _capacity = 0;//容量

		 //特例：静态成员变量只有无符号整形才可以在声明时给缺省值
		 //const static size_t npos = -1;//ok

		 //const static double d = 2.2;//err

		 const static size_t npos;
	 };

	 //流插入，流提取 
	 //不适合写成成员函数，涉及第一个参数的位置问题
	 istream& operator>> (istream& is, string& str);
	 ostream& operator<< (ostream& os, const string& str);
}