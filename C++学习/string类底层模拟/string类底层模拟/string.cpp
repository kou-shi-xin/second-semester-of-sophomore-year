#define _CRT_SECURE_NO_WARNINGS 

#include "string.h"

//进行函数定义与声明分离
//把它们放在bit空间域里，不同文件里的同一个bit会进行合并
namespace bit
{
	//静态成员变量相当于是全局变量，要在类外定义
	//但是这里不能在.h里定义，因为.h会在两个.cpp文件里展开
	//会产生重定义报错
	const size_t string::npos = -1;

	//string::string()
	//{
	//	//_str = nullptr;//err 防止对空指针的解引用
	//	//根据库中无参构造的特点，开一个空间
	//	_str = new char[1] {'\0'};
	//	_size = 0;
	//	_capacity = 0;
	//}

	string::iterator string::begin()
	{
		return _str;
	}

	string::iterator string::end()
	{
		return _str + _size;
	}

	string::string(const char* str)
		:_size(strlen(str))
	{
		//为了避免多次strlen计算，并且符合声明的顺序
		//只把_size放在初始化列表，其余放在函数体中
	
		_str = new char[_size + 1];//多开一个是给\0的
		_capacity = _size;
		strcpy(_str, str);//把初始化内容拷贝进空间
	}

	//要用深拷贝进行拷贝构造
	//s2(s1);把s1拷贝给s2,*this是s2,s是s1的别名
	//string::string(const string& s)
	//{
	//	//开一个和要拷贝的一样大小的空间
	//	_str = new char[s._capacity + 1];
	//	strcpy(_str, s._str);//把数据拷贝进新空间
	//	_size = s._size;
	//	_capacity = s._capacity;
	//}

	//现代写法
	string::string(const string& s)
	{
		//复用构造函数，构造一个tmp，在用tmp对象和this交换
		//如果不在声明时给缺省值，刚开始s2是随机值，tmp和s2交换后
		//tmp就是随机值，tmp出了函数会调用析构函数，此时程序可能会崩溃
		//所以好给缺省值
		string tmp(s._str);
		std::swap(_str, tmp._str);
		std::swap(_size, tmp._size);
		std::swap(_capacity, tmp._capacity);

		//string tmp(s._str);
		//swap(tmp);
	}

	//s1 = s3;//s1是*this,s是s3的别名
	//string& string::operator=(const string& s)
	//{
	//	//避免自己给自己赋值
	//	if (this != &s)
	//	{
	//		//开新空间，拷贝数据，释放原空间，改变指针指向
	//		char* tmp = new char[s._capacity + 1];
	//		strcpy(tmp, s._str);
	//		delete[] _str;
	//		_str = tmp;
	//		_size = s._size;
	//		_capacity = s._capacity;

	//		return *this;
	//	}
	//}

	//string& string::operator=(const string& s)
	//{
	//	//避免自己给自己赋值
	//	if (this != &s)
	//	{
	//		string tmp(s._str);
	//		swap(tmp);
	//	}
	//	return *this;
	//}

	//s3 = s1   这里的传参不能用引用
	//传值传参会进行拷贝构造，s1会拷贝一份给tmp
	//再让tmp与s3交换
	string& string::operator=(string tmp)
	{
		swap(tmp);
		return *this;
	}

	string::~string()
	{
		delete[] _str;//析构销毁资源
		_str = nullptr;//置空
		_size = _capacity = 0;//置0
	}

	const char* string::c_str()const
	{
		return _str;//返回字符串的首地址，用于打印数据
	}

	size_t string::size()const
	{
		return _size;
	}

	string::const_iterator string::begin()const
	{
		return _str;
	}

	string::const_iterator string::end()const
	{
		return _str + _size;
	}

	//_str是new出来的，出了这个函数不会销毁，可以用引用返回
	char& string::operator[](size_t pos)
	{
		assert(pos < _size);//防止越界
		return _str[pos];
	}

	const char& string::operator[](size_t pos)const
	{
		assert(pos < _size);//防止越界
		return _str[pos];
	}

	//用于扩容，一般不缩容
	void string::reserve(size_t n)
	{
		if (n > _capacity)
		{
			//手动扩容，手动释放
			char* tmp = new char[n + 1];
			strcpy(tmp, _str);
			delete[] _str;

			_str = tmp;
			_capacity = n;
		}
	}

	//尾插一个字符
	void string::push_back(char ch)
	{
		if (_size == _capacity)
		{
			//先计算容量，2倍增
			size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
			//再扩容
			reserve(newcapacity);
		}

		_str[_size] = ch;//覆盖\0的位置
		_str[_size + 1] = '\0';//补上\0
		++_size;
	}

	//尾插字符串
	void string::append(const char* str)
	{
		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}

		//strcat:从\0的位置开始追加，最后自动补上\0
		//strcat(_str, str);

		strcpy(_str + _size, str);
		_size += len;
	}

	string& string::operator+=(char ch)
	{
		push_back(ch);
		return *this;
	}

	string& string::operator+=(const char* str)
	{
		append(str);
		return *this;
	}

	//在指定位置插入 
	void string::insert(size_t pos, char ch)
	{
		assert(pos <= _size);

		if (_size == _capacity)
		{
			size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
			reserve(newcapacity);
		}

		//当pos为0，即头插时，程序崩溃！
		//因为end是无符号整形，减到0再减后会变成整形最大值(40多亿)
		//造成死循环
		/*size_t end = _size;
		while (end >= pos)
		{
			_str[end + 1] = _str[end];
			--end;
		}

		_str[pos] = ch;
		++_size;*/

		//把end的类型改为int，也不行。
		//因为当一个操作符两边的操作数类型不一样时，会产生隐式类型
		//转换，比如有符号与无符号，有符号会隐式转换成无符号类型。

		//解决方法1：把end的类型改为int,end指向最后一位有效位d的下一位，
		//把pos也强转为int类型
		/*int end = _size;
		while (end >= (int)pos)
		{
			_str[end + 1] = _str[end];
			--end;
		}

		_str[pos] = ch;
		++_size;*/

		//解决方法2：让end指向\0的下一位
		size_t end = _size + 1;
		while (end > pos)
		{
			_str[end] = _str[end - 1];
			--end;
		}

		_str[pos] = ch;
		++_size;
	}

	void string::insert(size_t pos, const char* str)
	{
		assert(pos <= _size);

		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}

		/*int end = _size;
		while (end >= (int)pos)
		{
			_str[end + len] = _str[end];
			end--;
		}*/

		size_t end = _size + len;
		while (end > pos+len-1)
		{
			_str[end] = _str[end - 1];
			end--;
		}

		memcpy(_str + pos, str, len);
		_size += len;
	}

	//在指定位置删除长度为len的字符串
	void string::erase(size_t pos, size_t len )
	{
		assert(pos  < _size);
		//当len大于前面的字符个数时，有多少删多少
		if (pos+len >= _size)
		{
			_str[pos] = '\0';
			_size = pos;
		}
		else
		{
			strcpy(_str + pos, _str + pos + len);
			_size -= len;
		}
	}

	//从pos位置开始找字符或是字符串
	size_t string::find(char ch, size_t pos)
	{
		for (size_t i = pos; i < _size; i++)
		{
			if (_str[i] == ch)
			{
				return i;
			}
		}

		return npos;
	}

	size_t string::find(const char* str, size_t pos)
	{
		//strstr：str存在时返回所在位置的指针
		const char* p = strstr(_str + pos, str);
		return p - _str;
	}

	//s1.swap(s3)
	void string::swap(string& s)
	{
		//调用库中的swap函数，交换内置类型
		//不直接交换数据，而是交换两块空间的指针
		std::swap(_str, s._str);
		std::swap(_size, s._size);
		std::swap(_capacity, s._capacity);
	}

	string string::substr(size_t pos, size_t len)
	{
		//len大于pos后面剩余的字符，有多少取多少
		if (len > _size - pos)
		{
			string sub(_str + pos);//直接构造子串返回
			return sub;
		}
		else
		{
			string sub;
			sub.reserve(len);
			for (size_t i = 0; i < len; i++)
			{
				sub += _str[pos + i];
			}
			return sub;
		}
	}

	bool string::operator<(const string& s)const
	{
		return strcmp(_str, s._str) < 0;
	}

	bool string::operator>(const string& s)const
	{
		return !(*this <= s);
	}

	bool string::operator<=(const string& s)const
	{
		return *this < s || *this == s;
	}

	bool string::operator>=(const string& s)const
	{
		return !(*this < s);
	}

	bool string::operator==(const string& s)const
	{
		return strcmp(_str, s._str) == 0;
	}

	bool string::operator!=(const string& s)const
	{
		return !(*this == s);
	}

	void string::clear()
	{
		_str[0] = '\0';
		_size = 0;
	}


	//在日期类中写成友元是为了访问私有成员
	//这里可以不写成友元函数，直接用公有成员
	istream& operator>> (istream& is, string& str)
	{
		//注意：scanf:%c时可以拿到空格，拿不到换行，直接忽略
		//            %s时两个都拿不到，直接忽略
		//        cin:拿不到空格和换行，直接忽略
		//在c++中不能用scanf，因为C++的流和C语言的流缓冲区不同

		str.clear();

		//为了避免一次性输入够多导致频繁扩容
		//开辟一个局部数组buff，先把字符存在buff中，
		//到达一定数量后再存入str
		char buff[128];
		int i = 0;
		char ch = is.get();
		
		while (ch != ' ' && ch != '\n')
		{
			buff[i++] = ch;
			//0 - 126
			if (i == 127)
			{
				buff[i] = '\0';
				str += buff;
				i = 0;
			}

			ch = is.get();
		}

		//如果buff没有装满
		if (i != 0)
		{
			buff[i] = '\0'; 
			str += buff;
		}

		return is;
	}

	ostream& operator<< (ostream& os, const string& str)
	{
		for (size_t i = 0; i < str.size(); i++)
		{
			os << str[i];
		}

		return os;
	}
}