#define _CRT_SECURE_NO_WARNINGS

//注意：这里包含的头文件string.h与C++库里的string.h不会发生冲突 
//因为这里会先去当前目录下查找
#include "string.h"

namespace bit
{
	void test_string1()
	{
		//注意：这里要指定命名空间，不然会调成库里的string
		string s1("hello world");
		cout << s1.c_str() << endl;

		/*bit::string s2;
		cout << s2.c_str() << endl;*/
		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s1[i] << " ";
		}
		cout << endl;

		for (auto ch : s1)
		{
			cout << ch << " ";
		}
		cout << endl;

		const string s2 = "xxxxxx";
		string::const_iterator it1 = s2.begin();
		while (it1 != s2.end())
		{
			//*it1 = 'y';//err 
			cout << *it1 << " ";
			++it1;
		}
		cout << endl;

		for (size_t i = 0; i < s2.size(); i++)
		{
			cout << s2[i] << " ";
		}
		cout << endl;
	}

	void test_string2()
	{
		//注意：这里要指定命名空间，不然会调成库里的string
		string s1("hello world");
		cout << s1.c_str() << endl;

		s1.push_back('x');
		cout << s1.c_str() << endl;

		s1.append("yyyyyy");
		cout << s1.c_str() << endl;

		s1 +='a';
		cout << s1.c_str() << endl;

		s1 += "bbbb";
		cout << s1.c_str() << endl;
	}

	void test_string3()
	{
		//注意：这里要指定命名空间，不然会调成库里的string
		string s1("hello world");
		cout << s1.c_str() << endl;

		s1.insert(6, 'x');
		cout << s1.c_str() << endl;

		s1.insert(0, 'x');
		cout << s1.c_str() << endl;

		string s2("hello world");
		s2.insert(6, "yyy");
		cout << s2.c_str() << endl;

		s2.insert(0, "yyy");
		cout << s2.c_str() << endl;

		string s3("hello world");
		s3.erase(6);//err
		cout << s3.c_str() << endl;
	}

	void test_string4()
	{
		//注意：这里要指定命名空间，不然会调成库里的string
		string s1("hello world");
		cout << s1.c_str() << endl;

		cout << s1.find('o', 3) << endl;
		cout << s1.find("wor", 4) << endl;
	}

	void test_string5()
	{
		//注意：这里要指定命名空间，不然会调成库里的string
		string s1("hello world");
		string s2(s1);

		s1[0] = 'x';
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;

		string s3("zzzzzz");
		s1 = s3;
		cout << s1.c_str() << endl;

		string s4("bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb");
		s1 = s4;
		cout << s1.c_str() << endl;

		s1 = s1;
		cout << s1.c_str() << endl;
	}

	void test_string6()
	{
		//注意：这里要指定命名空间，不然会调成库里的string
		string s1("yyy");
		string s2("zzzzzzzzzzzzzzzzz");
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;
		cout << endl;

		//使用库里的swap函数，代价极大！
		//因为库里的swap函数使用的是模板
		//它经过一次拷贝构造，两次赋值拷贝，都是深拷贝
		//深拷贝要开空间，拷贝数据，释放空间，而且还是传值返回
		std::swap(s1, s2);
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;
		cout << endl;

		s1.swap(s2);
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;
	}

	void test_string7()
	{
		// 取出url中的域名
		string url("http://www.cplusplus.com/reference/string/string/find/");
		size_t pos1 = url.find(':');
		string url1 = url.substr(0, pos1 - 0);
		cout << url1.c_str() << endl;//http

		size_t pos2 = url.find('/', pos1 + 3);
		string url2 = url.substr(pos1 + 3, pos2 - (pos1 + 3));
		cout << url2.c_str() << endl;//www.cplusplus.com

		string url3 = url.substr(pos2 + 1);
		cout << url3.c_str() << endl;//reference/string/string/find/
	}

	void test_string8()
	{
		//注意：这里要指定命名空间，不然会调成库里的string
		string s1("hello world");
		cout << s1 << endl;

		cin >> s1;
		cout << s1 << endl;

	}
}

int main()
{
	bit::test_string8();

	return 0;
}