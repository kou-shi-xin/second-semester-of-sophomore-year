
//void Swap(int *px, int *py)
//{
//	//x,y是形参
//	int z = *px;//z=a
//	*px = *py;//x=y
//	*py = z;//y=z
//}
////当实参传递给形参时，形参是实参的一份临时拷贝
////对形参的修改不影响实参

//
//#include "stdio.h"
//int main()
//{
//	int a = 0;
//	int b = 0;
//	scanf("%d,%d", &a, &b);
//	printf("交换前：a=%d,b=%d\n", a, b);
//	//a,b叫实参
//	Swap(&a, &b);
//	printf("交换后:a=%d,b=%d\n", a, b);
//}




