#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
using namespace std;

//再谈构造函数

//typedef int DataType;
//class Stack
//{
//public:
//	Stack(size_t capacity )
//	{
//		_array = (DataType*)malloc(sizeof(DataType) * capacity);
//		if (NULL == _array)
//		{
//			perror("malloc申请空间失败!!!");
//			return;
//		}
//		_capacity = capacity;
//		_size = 0;
//	}
//
//	void Push(DataType data)
//	{
//		// CheckCapacity();
//		_array[_size] = data;
//		_size++;
//	}
//
//	~Stack()
//	{
//		if (_array)
//		{
//			free(_array);
//			_array = NULL;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//
//private:
//	DataType* _array;
//	int _capacity;
//	int _size;
//};
//
//class MyQueue
//{
//public:
//	//Stack不具备默认构造，MyQueue也无法生成默认构造。
//	//此时MyQueue要显示实现默认构造
//	
//	//初始化列表：冒号开始，逗号分割
//	//本质：可以理解为每个对象中成员定义的地方。
//
//	//总结：所有成员，都可以在初始化列表初始化，也可以在函数体内初始化。
//	//但是有三类成员必须在初始化列表初始化：
//	//1.引用 2.const 3.没有默认构造的自定义类型成员(必须显式传参调构造)
//
//	//MyQueue(int n,int& rr)//这里也可以写缺省值
//	MyQueue(int n= 20)
//		:_pushst(n)//自定义类型会调用构造函数
//		, _popst(n)
//		//, _size(0) //ok
//
//		//,_x (1)
//		//,_ref(rr)
//	{
//		//函数体内
//		_size = 0;//ok
//
//		//_x =1; //err
//
//	}
//private:
//	//声明
//	Stack _pushst;
//	Stack _popst;
//	int _size;
//
//	//const变量必须在定义的时候初始化。因为它只有一次初始化的机会
//	//const int _x;
//
//  //const int _x = 10;//在这里给缺省值ok
//    
//	//引用变量必须在定义的时候初始化
//	//int& _ref;
//};
//
//
//int main()
//{
//	//int xx = 0;
//
//	//MyQueue q1(10,xx);
//	MyQueue q1(10);
//	MyQueue q2;//有缺省值时可以不用赋值，没传默认用缺省值
//
//	//int& aa = xx;
//
//	return 0;
//}


//typedef int DataType;
//class Stack
//{
//public:
//	Stack(size_t capacity = 4)
//	{
//		_array = (DataType*)malloc(sizeof(DataType) * capacity);
//		if (NULL == _array)
//		{
//			perror("malloc申请空间失败!!!");
//			return;
//		}
//		_capacity = capacity;
//		_size = 0;
//	}
//
//	void Push(DataType data)
//	{
//		// CheckCapacity();
//		_array[_size] = data;
//		_size++;
//	}
//
//	~Stack()
//	{
//		if (_array)
//		{
//			free(_array);
//			_array = NULL;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//
//private:
//	DataType* _array;
//	int _capacity;
//	int _size;
//};


//class MyQueue
//{
//public:
//	//初始化列表，不管你写不写，每个成员变量都会先走一遍
//	//自定义类型的成员会调用默认构造(没有默认构造就编译报错)
//	// 内置类型看编译器，有点会处理，有的不会处理
//
//	//实践总结：尽可能使用初始化列表初始化，不方便再使用函数体初始化
//	MyQueue()
//		:_pushst(10)
//		,_ptr((int*)malloc(40)) //ok
//	{
//		_size = 2;
//	}
//
//private:
//	//声明
//	Stack _pushst;
//	Stack _popst;
//
//	//缺省值  给初始化列表用的
//	//但是如果在初始化列表中给定了值，就以列表中的为准，与缺省值无关
//	int _size = 1;
//
//	int* _ptr;
//};


//成员变量在类中声明次序就是其在初始化列表中的初始化顺序，
//与其在初始化列表中的先后次序无关

//class A
//{
//public:
//	A(int a)
//		:_a1(a)
//		, _a2(_a1)
//	{}
//
//	void Print() 
//	{
//		cout << _a1 << " " << _a2 << endl;
//	}
//private:
//	int _a2;
//	int _a1;
//};
//
//int main() 
//{
//	A aa(1);
//	aa.Print();
//}


//隐式类型转换
//class A
//{
//public:
//	//单参数构造函数
//	A(int a)
//		:_a(a)
//	{
//		cout << "A(int a)" << endl;
//	}
//
//	//拷贝构造
//	A(const A& aa)
//		:_a(aa._a)
//	{
//		cout << "A(const A& aa)" << endl;
//	}
//
//private:
//	int _a;
//};
//
//int main()
//{
//	A aa1(1);
//
//	//拷贝构造
//	A aa2 = aa1;
//
//	//隐式类型转换
//	//内置类型转换为自定义类型
//	
//	//过程：先用3构造了一个A的临时对象，再用临时对象去拷贝构造给aa3
//	//优化：在一个表达式中，编译器遇到构造 + 拷贝构造，会优化成直接构造
//	A aa3 = 3;
//
//	//A& raa = 4;//err
//	//当用引用时，由于用3构造了一个临时对象，临时对象具有常性，所以加const
//	//raa引用的是类型转换中用3构造的临时对象
//	const A& raa = 4;//ok
//
//	return 0;
//}


//隐式类型转换的使用
//class A
//{
//public:
//	A(int a)
//		:_a1(a)
//		, _a2(_a1)
//	{}
//
//	void Print()
//	{
//		cout << _a1 << " " << _a2 << endl;
//	}
//private:
//	int _a2;
//	int _a1;
//};
//
//class Stack
//{
//public:
//	//方式1：ok
//	//void Push(A& aa)
//	//{
//	//	//.......
//	//}	
//
//	//方式2：ok
//	void Push(const A& aa)
//	{
//		//....
//	}
//
//	//.....
//};
//
//int main()
//{
//	Stack st;
//
//	//方式1：会进行先构造，后拷贝构造
//	A a1(1);
//	st.Push(a1);
//
//	//方式2：2进行了隐式类型转换，是代码更简洁
//	st.Push(2);
//
//	return 0;
//}

//explcit关键字：禁止构造函数的隐式转换。
//class A
//{
//public:
//	//单参数构造函数
//	explicit A(int a)
//	//A(int a)
//		:_a(a)
//	{
//		cout << "A(int a)" << endl;
//	}
//
//	//拷贝构造
//	A(const A& aa)
//		:_a(aa._a)
//	{
//		cout << "A(const A& aa)" << endl;
//	}
//
//private:
//	int _a;
//};
//
//int main()
//{
//	A aa1(1);
//
//	//拷贝构造
//	A aa2 = aa1;
//
//	A aa3 = 3;
//
//	const A& raa = 4;
//
//	return 0;
//}


//class A
//{
//public:
//	//单参数构造函数
//	//explicit A(int a)
//	A(int a)
//		:_a(a)
//	{
//		cout << "A(int a)" << endl;
//	}
//
//	//多参数构造函数
//	A (int a1,int a2)
//		:_a(0)
//		,_a1(a1)
//		,_a2(a2)
//	{}
//
//	//拷贝构造
//	A(const A& aa)
//		:_a(aa._a)
//	{
//		cout << "A(const A& aa)" << endl;
//	}
//
//private:
//	int _a;
//	int _a1;
//	int _a2;
//};
//
//int main()
//{
//	A aaa1(1, 2);
//	A aaa2 = { 1,2 };
//	const A& aaa3 = { 3,2 };
//
//	return 0;
//}

/////////////////////////////////////////////////////////////////

//static 成员

//静态成员变量
//对象中只存成员变量，不存静态成员变量，静态成员变量在静态区，
// 也不存函数，函数指针，它们在代码段。

//特点：它属于整个类，属于所有对象

//class A
//{
//public:
//	A()
//	{
//		cout << "A()" << endl;
//	}
//
//	A(const A& t)
//	{
//		cout << "A(const A& t)" << endl;
//
//	}
//
//	~A()
//	{
//		cout << "~A()" << endl;
//
//	}
//
//
//private:
//	//声明
//	int _a1;
//	int _a2;
//
//public:
//	//在静态区，不在对象中
//	//不能给缺省值，因为缺省值是给初始化列表用的
//	//它在静态区，不在对象中，不走初始化列表
//	static int _scount;
//};
//
////静态成员变量的定义(如果只有声明，没有定义，会报错！)
//int A::_scount = 1;
//
//
//int main()
//{
//	A aa1;
//	cout << sizeof(aa1) << endl;//8
//
//	//如果它是公有的，就可以按如下访问
//	//aa1._scount++;
//	//cout << A::_scount << endl;
//
//	return 0;
//}


//使用静态成员变量可以统计一共创建了多少个类对象
//因为所有的对象都要走构造或者拷贝构造：累计创建了多少个类对象
//走完析构函数：现在还有多少对象存活

class A
{
public:
	A()
	{
		++_scount;
	}

	A(const A& t)
	{
		++_scount;
	}

	~A()
	{
		--_scount;
	}

private:
	//声明
	int _a1;
	int _a2;

public:
	static int _scount;
};

//静态成员变量的定义(如果只有声明，没有定义，会报错！)
int A::_scount = 0;

int main()
{
	A aa1;
	//cout << sizeof(aa1) << endl;//8

	A aa2;
	A aa3;

	cout << A::_scount << endl;

	return 0;
}


//静态成员函数
//上面的例子是在静态成员变量为公有时的情景，那为私有时呢？
//该如何访问？

//class A
//{
//public:
//	A()
//	{
//		++_scount;
//	}
//
//	A(const A& t)
//	{
//		//但是非静态的可以调用静态的
//		//GetCount();
//
//		++_scount;
//	}
//
//	~A()
//	{
//		--_scount;
//	}
//
//	//特点：static修饰成员函数，没有this指针，意味着只能访问静态成员
//	//不能访问其他的成员变量
//	static int GetCount()
//	{
//		return _scount;
//	}
//
//private:
//	//声明
//	int _a1;
//	int _a2;
//
//	static int _scount;
//};
//
//int A::_scount = 0;
//
//int main()
//{
//	A aa1;
//	//cout << sizeof(aa1) << endl;//8
//
//	A aa2;
//	A aa3;
//
//	//调用方式
//	cout << A::GetCount() << endl;
//
//	return 0;
//}