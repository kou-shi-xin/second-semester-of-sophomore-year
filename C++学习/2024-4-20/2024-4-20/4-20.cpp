#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
using namespace std;


//同一作用域，可以同名，但是要满足重载规则
void Swap(int* pa, int* pb)
{
	cout << "void Swap(int* pa, int* pb)" << endl;
}


void Swap(double* pa, double* pb)
{
	cout << "void Swap(double* pa, double* pb)" << endl;
}

//int main()
//{
//	int a = 0, b = 1;
//	double c = 1.1, d = 2.2;
//
//	Swap(&a, &b);
//	Swap(&c, &d);
//
//	return 0;
//}

//不同一作用域，可以同名
namespace bit1
{
	void Swap(int* pa, int* pb)
	{
		cout << "void Swap(int* pa, int* pb)" << endl;
	}
}

namespace bit2
{
	void Swap(double* pa, double* pb)
	{
		cout << "void Swap(double* pa, double* pb)" << endl;
	}
}



//int main()
//{
//	int a = 0, b = 1;
//	double c = 1.1, d = 2.2;
//
//	bit1::Swap(&a, &b);
//	bit2::Swap(&c, &d);
//
//	return 0;
//}


//参数的个数不同
//void f()
//{
//	cout << "f()" << endl;
//}
//
//void f(int a)
//{
//	cout << "f(int a)" << endl<< endl;
//}


//参数顺序不同
//int f()
//{
//	return 0;
//}
//
//void f()
//{
//	cout << "void f(char c, int d)" << endl;
//}

//int main()
//{
//	int a = 0, b = 1;
//	double c = 1.1, d = 2.2;
//
//	int ret = f();
//	cout << ret << endl;
//	f();
//
//	return 0;
//}

//有一个是缺省参数构成重载。但是调用时存在歧义
//void f()
//{
//	cout << "void f()" << endl;
//}
//
//void f(int a = 10)
//{
//	cout << "void f(int a = 10)" << endl;
//}
//
//
//int main()
//{
//	int a = 0, b = 1;
//	double c = 1.1, d = 2.2;
//
//	f();
//	f(5);
//
//
//	return 0;
//}

//返回值不同，无法区分。因为返回值可接收，可不接受，调用函数产生歧义。

//为什么C++存在重载，C语言不支持？C++为什么支持？


//引用
//int main()
//{
//	int a = 1;
//
//	//引用：&  b是a的别名
//	int& b = a;
//
//	//也可以给别名取别名
//	int& c = b;
//
//	//值相同，地址也相同
//	cout << "a = " << a<< endl; 
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl << endl;
//
//	cout << "&a = " << &a << endl;
//	cout << "&b = " << &b << endl;
//	cout << "&c = " << &c << endl;
//
//
//	return 0;
//}


//引用做参数
//void Swap(int& a, int& b)//a是x的别名，b是y的别名。a,b的交换就是x,y的交换
//{
//	int tmp = a;
//	a = b; 
//	b = tmp;
//}
//
//int main()
//{
//	int x = 1;
//	int y = 2;
//
//	Swap(x, y);//这里就不用传地址过去了
//
//	cout << x << ":" << y << endl;
//
//	return 0;
//}

//引用做返回值这里暂时不讲

//引用一旦引用一个实体，再不能引用其他实体
//int main()
//{
//	int x = 0;
//	int& y = x;//y是x的别名
//
//	int z = 1;
//
//	//这里y是z的别名，还是z赋值给y。
//	//是赋值。引用一旦引用一个实体，就不能再改变指向了！
//	y = z;
//
//	cout << "x = " << x << endl;
//	cout << "y = " << y << endl;
//	cout << "z = " << z << endl << endl;
//
//	return 0;
//}


//引用的权限问题
//权限放大问题存在于引用和指针里，普通的赋值不会。
//权限只能缩小，不能放大
//int main()
//{
//	//这里权限放大了。m是只读，n变成m的别名后，可读可写。
//	const int m = 0;
//	//int& n = m;//err
//	int p = m;//可以，不是权限的放大。只是把m拷贝给p，p的修改不影响m。
//
//	return 0;
//}

//int main()
//{
//	const int m = 0;
//
//	// 权限的放大
//	//p1可以修改，*p1不行,const修饰的是p1指向的内容，即*p1。
//	const int* p1 = &m;
//	p1++;//可以
//
//	//int* p2 = p1;//err. p2是可以修改的，所以权限放大了。
//		
//	return 0;
//}

//int main()
//{
//	//权限的缩小
//	int x = 0; 
//	int& y = x;
//
//	const int& z = x;//可以
//
//	y++;//x,z都会修改
//
//	return 0;
//}
//int main()
//{
//	/
//
//	//权限的平移
//	const int& n = m;//可以
//
//	//权限的缩小
//	int x = 0; 
//	int& y = x;
//	const int& z = x;//可以
//	y++;//x,z都会修改
//
//	return 0;
//}


//传值、传引用效率比较
#include <time.h>

struct A { int a[10000]; };

void TestFunc1(A a) {}
void TestFunc2(A& a) {}

void TestRefAndValue()
{
	A a;
	// 以值作为函数参数
	size_t begin1 = clock();
	for (size_t i = 0; i < 10000; ++i)
		TestFunc1(a);
	size_t end1 = clock();
	// 以引用作为函数参数
	size_t begin2 = clock();
	for (size_t i = 0; i < 10000; ++i)
		TestFunc2(a);
	size_t end2 = clock();

	// 分别计算两个函数运行结束后的时间
	cout << "TestFunc1(A)-time:" << end1 - begin1 << endl;
	cout << "TestFunc2(A&)-time:" << end2 - begin2 << endl;
}
//0毫米表示运行时间<1毫米

//int main()
//{
//	TestRefAndValue();
//
//	return 0;
//}

//int main()
//{
//	double d = 3.24;
//
//	//有警告，要类型转换
//	int i = d;
//	
//	// 因为类型转换会生成临时变量，这个临时变量具有常性。权限被放大了。
//	// 相当于被const修饰
//	//int& r = d;err 
//	const int& r = d;//可以。加了const 就相当于权限平移了。
//
//	return 0;
//}

//常引用
//int main()
//{	
//	int x = 0, y = 1;
//
//	//表达式运算也会生成临时变量
//	//int& r2 = x + y;err
//
//	const int& r2 = x + y; //可以
//
//	return 0;
//}


//引用和指针的区别
//转换成汇编后，就相当于没有引用的概念了，都是相当于指针。
//int main()
//{
//	int a = 0;
//	int& b = a;//语法上不开空间，底层开空间
//
//	int* p = &a;//语法上要开空间，底层开空间
//
//	cout << sizeof(a) << endl;
//	cout << sizeof(int&) << endl;
//
//	return 0;
//}

//int main()
//{
//		int a = 10;
//		int& ra = a;
//
//		cout << "&a =  " <<  &a << endl;
//		cout << "&ra = " << &ra << endl;
//	
//	return 0;
//}


//内联函数
//当我们有一个要频繁调用的小函数 例如Swap
//C -- 宏函数

//Debug版本下默认不展开  -->方便调试
//int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int c = Add(1, 2);
//	cout << c << endl;
//
//	return 0;
//}


//auto关键字
//int main()
//{
//	int i = 0;
//	//根据右边得到初始化自动推导类型
//	auto j = 7;
//
//	return 0;
//}


//typedef的缺陷例子
//typedef char* pstring;
//int main()
//{
//	//const pstring p1;   //err. const修饰指针p1本身，而const修饰的指针要初始化
//	const pstring* p2;  // 可以. const修饰 *p2
//
//	return 0;
//}


int TestAuto()
{
	return 10;
}

int main()
{
	int a = 10;
	auto b = a;
	auto c = 'a';
	auto d = TestAuto();

	//typeid：判断一个对象的类型
	cout << typeid(b).name() << endl;
	cout << typeid(c).name() << endl;
	cout << typeid(d).name() << endl;

	//auto e; 无法通过编译，使用auto定义变量时必须对其进行初始化
	return 0;
}

void TestFor1()
{
	int array[] = { 1, 2, 3, 4, 5 };
	for (int i = 0; i < sizeof(array) / sizeof(array[0]); ++i)
		array[i] *= 2;

	//以前的数组遍历
	for (int* p = array; p < array + sizeof(array) / sizeof(array[0]); ++p)
		cout << *p << " ";
	cout << endl;

	//但是如果要改变array里面的值，就要用引用&
	//变量e是可以改变的
	for (auto& e : array)
	{
		e /= 2;
	}

	//C++11 范围for  不支持倒着遍历
	//数组里的内容自动赋值给e 自动++ 自动判断结束
	for (auto e : array)
	{
		cout << e << " " ;
	}
	cout << endl;
}


//void f(int)
//{
//	cout << "f(int)" << endl;
//}
//
//void f(int*)
//{
//	cout << "f(int*)" << endl;
//}
//
//int main()
//{
//	f(0);
//	f(NULL);
//	f((int*)NULL);
//	return 0;
//}


//int main()
//{
//	TestFor1();
//	return 0;
//}
