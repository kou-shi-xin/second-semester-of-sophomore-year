#define _CRT_SECURE_NO_WARNINGS 

#include "Heap.h"

void HPInit(HP* php)
{
	php->a = (HPDateType*)malloc(sizeof(HPDateType) * 4);
	if (php->a == NULL)
	{
		perror("malloc fail");
		return;
	}
	php->capacity = 4;
	php->size = 0;
}

//直接用数组建堆初始化
void HPInitArray(HP* php, HPDateType* a, int n)
{
	assert(php);

	php->a = (HPDateType*)malloc(sizeof(HPDateType) * n);
	if (php->a == NULL)
	{
		perror("malloc fail");
		return;
	}

	memcpy(php->a, a, sizeof(HPDateType) * n);
	php->capacity = php->size = n;

	//向上调整，建堆O(N*logN)
	/*for (int i = 1; i < php->size; i++)
	{
		//向上调整算法:从下往上调
		//要调最后一层，效率更低
		AdjustUp(php->a, i);
	}*/

	//向下调整，建堆O(N)
	for (int i = (php->size - 1 - 1) / 2; i >= 0; i--)
	{
		//向下调整算法:从上往下调
		//最后一层默认是小堆，不调，效率更高
		AdjustDown(php->a, php->size, i);
	}
}


void HPDestroy(HP* php)
{
	free(php->a);
	php->a = NULL;
	php->capacity = 0;
	php->size = 0;
}

void Swap(HPDateType* px, HPDateType* py)
{
	int tmp = *px;
	*px = *py;
	*py = tmp;
}

//向上调整，建堆的时间复杂度：O(N*logN)
//向下调整，建堆的时间复杂度：O(N)


//n个数据，时间复杂度：O(logN)
void AdjustUp(HPDateType* a, int n)
{
	int child = n - 1;
	int parent = (child - 1) / 2;

	while (child < n)
	{
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void AdjustDown(HPDateType* a, int n, int parent)
{
	int child = parent * 2 + 1;

	//算左孩子，本质是下标，堆是完全二叉树
	//只要判断左孩子是否越界即可
	while (child < n)
	{
		if (child + 1 < n && a[child+ 1] < a[child])
		{
			child += 1;
		}
		//如果右孩子存在
		if ( a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}


//堆的插入
void HPPush(HP* php, HPDateType x)
{
	assert(php);

	if (php->capacity == php->size)
	{
		HPDateType* tmp = (HPDateType*)realloc(php->a, sizeof(HPDateType) * php->capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc fali");
			return;
		}
		else
		{
			php->a = tmp;
			php->capacity *= 2;
		}
	}

	php->a[php->size++] = x;

	//向上调整算法:从下往上调
	AdjustUp(php->a, php->size);
}

//删除堆顶元素
void HPPop(HP* php)
{
	assert(php && php->size > 0);

	Swap(&php->a[0], &php->a[php->size - 1]);
	php->size--;

	//向下调整算法：从上往下调
	AdjustDown(php->a, php->size, 0);
}

//获取堆顶元素
HPDateType HPTop(HP* php)
{
	assert(php && php->size > 0);

	return php->a[0];
}

//判断堆是否为空
bool HPEmpty(HP* php)
{
	assert(php);

	return php->size == 0;
}