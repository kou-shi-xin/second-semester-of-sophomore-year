#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>

typedef int HPDateType;
typedef struct Heap
{
	HPDateType* a;
	int size;
	int capacity;
}HP;

void HPInit(HP* php);

//直接用数组建堆
void HPInitArray(HP* php, HPDateType* a, int n);

void HPDestroy(HP* php);

//堆的插入
void HPPush(HP* php, HPDateType x);

//删除堆顶元素
void HPPop(HP* php);

//获取堆顶元素
HPDateType HPTop(HP* php);

//判断堆是否为空
bool HPEmpty(HP* php);

void Swap(HPDateType* px, HPDateType* py);

void AdjustUp(HPDateType* a, int n);
void AdjustDown(HPDateType* a, int n, int parent);