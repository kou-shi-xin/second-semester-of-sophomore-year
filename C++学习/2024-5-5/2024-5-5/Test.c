#define _CRT_SECURE_NO_WARNINGS 

#include "Heap.h"

//堆排序 O(N*lobN)
//排降序，建小堆
void HeapSort(int* a, int n)
{
	//用a数组直接建堆,复用向下调整算法
	for (int i = (n - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(a, n, i);
	}

	int end = n - 1;
	while (end > 0)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end, 0);
		end--;
	}
}

int main()
{
	int a[] = { 5,9,1,3,7,8,0,2,4,6 };

	HeapSort(a, sizeof(a) / sizeof(int));

	return 0;
}

//int main()
//{
//	int a[] = { 25,45,10,36,54,78 };
//
//	HP hp;
//	HPInit(&hp);
//
//	for (int i = 0; i < sizeof(a) / sizeof(int); i++)
//	{
//		HPPush(&hp, a[i]);
//	}
//
//	while (!HPEmpty(&hp))
//	{
//		printf("%d ", HPTop(&hp));
//		HPPop(&hp);
//	}
//
//	HPDestroy(&hp);
//	return 0;
//}