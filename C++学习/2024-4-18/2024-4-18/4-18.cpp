#define _CRT_SECURE_NO_WARNINGS 

//#include <stdio.h>

//int a = 10;
//
//int main()
//{
//	int a = 0;
//
//	printf("%d\n", ::a);
//
//	return 0;
//}


//int rand = 10;
//
//namespace bit
//{
//	int rand = 0;
//}
//
//int main()
//{
//	printf("%d\n", bit::rand);
//	printf("%d\n", ::rand);
//
//	return 0;
//}

//namespace N1
//{
//    int a;
//    int b;
//    int Add(int left, int right)
//    {
//        return left + right;
//    }
//    namespace N2
//    {
//        int c;
//        int d;
//        int Sub(int left, int right)
//        {
//            return left - right;
//        }
//    }
//}
//
//
//int main()
//{
//    printf("%d\n", N1::Add(2, 3));
//    printf("%d\n", N1::N2::Sub(8 ,5));
//
//    return 0;
//}

//namespace bit
//{
//	int a = 0;
//	int b = 1;
//	int c = 2;
//}
//
//namespace hello
//{
//	int a = 0;
//	int b = 1;
//	int c = 2;
//}

//展开命名空间
//using namespace bit;
//using namespace hello;
//
//int main()
//{
//	printf("%d\n", a);
//	printf("%d\n", a);
//	printf("%d\n", a);
//	printf("%d\n", a);
//	printf("%d\n", a);
//	printf("%d\n", a);
//
//	return 0;
//}

////展开命名空间中的指定成员
//using bit::b;
//
//int main()
//{
//	printf("%d\n", bit::a);
//	printf("%d\n", b);
//
//	return 0;
//}




#include <iostream>
using namespace std;

//void Func(int a , int b , int c )
//{
//	cout << "a = " << a << endl;
//	cout << "b = " << b << endl;
//	cout << "c = " << c << endl << endl;
//}
//
//int main()
//{
//	Func();
//
//	return 0;
//}

//void Func(int a = 0)
//{
//	cout << a << endl;
//}

//int main()
//{
//	//  << 流输入
//	int i = 0;
//	double j = 1.1;
//
//	//自动识别类型
//	cout << i << " " << j << endl;
//
//	//  >>  流提取
//	cin >> i >> j;
//	cout << i <<  " " << j << '\n' << endl;
//
//	return 0;
//}