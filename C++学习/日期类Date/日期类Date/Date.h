#pragma once

#include <iostream>
using namespace std;

#include <stdbool.h>
#include <assert.h>

class Date
{
public:
	//构造函数
	Date(int year, int month, int day);
	void Print()const;

	//判断输入的年月日是否合法
	bool CheckDate();

	//获取某月的天数
	int GetMonthDay(int year, int month)
	{
		assert(_month > 0 && _month < 13);

		static int MonthDay[13] = { -1,31,28,31,30,31,30,31,31,30,31,30,31 };
		if (month == 2 && ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0)))
		{
			return 29;
		}
		else
		{
			return MonthDay[month];
		}
	}
	

	//日期的比较
	bool operator==(const Date& d)const;
	bool operator>(const Date& d)const;
	bool operator>=(const Date& d)const;
	bool operator<=(const Date& d)const;
	bool operator!=(const Date& d)const;
	bool operator<(const Date& d)const;

	//日期+=100
	Date operator+=(int day);
	Date operator-=(int day);

	//日期+100
	Date operator+(int day)const;
	Date operator-(int day)const;

	//前置+-
	Date& operator++();
	Date& operator--();

	//后置+-
	Date operator++(int)const;
	Date operator--(int)const;

	//日期-日期
	int operator-(const Date& d)const;
	
private:
	int _year;
	int _month;
	int _day;
};
