#define _CRT_SECURE_NO_WARNINGS 

#include "Date.h"

bool Date::CheckDate()
{
	if (_month < 1 || _month>13 || _day<1 || _day>GetMonthDay(_year, _month))
		return false;
	else
		return true;
}


//构造函数
Date::Date(int year, int month, int day)
{
		_year = year;
		_month = month;
		_day = day;
	
	if(!CheckDate())
	{
		cout << "日期非法！" << endl;
	}
}

void Date::Print()const
{
	cout << _year << "-" << _month << "-" << _day << endl;
}


//日期的比较
bool Date::operator==(const Date& d)const
{
	return _year == d._year &&
		_month == d._month &&
		_day == d._day;
}

bool Date::operator>(const Date& d)const
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year == d._year)
	{
		if (_month > d._month)
		{
			return true;
		}
		else if (_month == d._month)
		{
			if (_day > d._day)
				return true;
		}
	}

	return false;
}

bool Date::operator>=(const Date& d)const
{
	return *this > d || *this == d;
}

bool Date::operator<=(const Date& d)const
{
	return (*this == d) || (*this < d);
}

bool Date::operator!=(const Date& d)const
{
	return !(*this == d);
}

bool Date::operator<(const Date& d)const
{
	return !(*this >= d);
}

//日期+=100
Date Date::operator+=(int day)
{
	if (day < 0)
	{
		return *this -= day;
	}

	_day += day;
	
	while (_day > GetMonthDay(_year, _month))
	{
			_day -= GetMonthDay(_year, _month);
			_month++;
			if (_month == 13)
			{
				_year++;
				_month = 1;
			}
	}

	return *this;
}

Date Date::operator-=(int day)
{
	if (day < 0)
	{
		return *this += day;
	}

	_day -= day;
	while (_day < 0)
	{
		_day += GetMonthDay(_year, _month-1);
		_month--;
		if (_month == 0)
		{
			_year--;
			_month = 12;
		}
	}
	return *this;
}

//日期+100
Date Date::operator+(int day)const
{
	Date tmp = *this;
	tmp += day;

	return tmp;
}

Date Date::operator-(int day)const
{
	Date tmp = *this;
	tmp -= day;

	return tmp;
}

////前置+-
Date& Date::operator++()
{
	*this+=1;

	return *this;
}

Date& Date::operator--()
{
	*this -= 1;

	return *this;
}

////后置+-
Date Date::operator++(int)const
{
	Date tmp = *this;
	tmp += 1;

	return tmp;
}

Date Date::operator--(int)const
{
	Date tmp = *this;
	tmp -= 1;

	return tmp;
}

////日期-日期
int Date::operator-(const Date& d)const
{
	Date max = *this;
	Date min = d;
	int flag = 1;

	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}

	int n = 0;

	while (max != min)
	{
		++n;
		++min;
	}

	return n * flag;
}
