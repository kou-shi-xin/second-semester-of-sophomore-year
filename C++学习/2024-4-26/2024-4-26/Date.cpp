#define _CRT_SECURE_NO_WARNINGS 

#include "Date.h"

bool Date::CheakDate()
{
	if (_month < 1 || _month>12
		|| _day<1 || _day>GetMonthDay(_year, _month))
	{
		return false;
	}
	else
	{
		return true;
	}
}

//1.缺省参数只能在声明的时候给
//2.成员函数声明与定义分离时，要指定类域
Date::Date(int year, int month, int day)
{
	_year = year;
	_month = month;
	_day = day;

	if (!CheakDate())
	{
		cout << "日期非法!" << endl;
	}
}

void Date::Print() const
{
	cout << _year << "-" << _month << "-" << _day << endl;
}

//思路：先比年，年小就小，年相等比月，月小就小，年月相等比日，日小就小
//d1 <d2 隐含的this是d1, d是d2的别名
bool Date::operator< (const Date& d) const
{
	if (_year < d._year)
	{
		return true;
	}
	else if (_year == d._year)
	{
		if (_month < d._month)
		{
			return true;
		}
		else if (_month == d._month)
		{
			return _day < d._day;
		}
	}

	return false;
}

//先写好大于和等于 或者 小于和等于的函数，其余的进行复用
//d1 <=d2
bool Date::operator<= (const Date& d) const
{
	return *this < d || *this == d;
}

bool Date::operator> (const Date& d) const
{
	return !(*this <= d);
}

bool Date::operator>= (const Date& d) const
{
	return !(*this < d);
}

bool  Date::operator==(const Date& d) const
{
	return _year == d._year
		&& _month == d._month
		&& _day == d._day;
}

bool  Date::operator!=(const Date& d) const
{
	return !(*this == d);
}


//日期 += 天数 ：d1 + 100
//这里的d1 已经改了
Date& Date::operator+=(int day)
{
	//这里是处理有人传负的天数
	if (day < 0)
	{
		return *this -= -day;
	}

	//先加上天数
	_day += day;

	//加上天数后超出了月的范围
	while (_day > GetMonthDay(_year, _month))
	{
		//减去当前月的天数，此时月份+1
		_day -= GetMonthDay(_year, _month);
		++_month;

		//超过12个月时
		if (_month == 13)
		{
			++_year;//年份+1
			_month = 1;//别忘了月份还要从1月开始
		}
	}
	return *this;
}


// d1 + 50,d1没有改变
Date Date::operator+(int day) const
{
    //实例化一个临时的局部变量，用拷贝构造，把d1的日期拷贝给tmp,这样d1就不会改变
	Date tmp = *this;
	tmp += day;//直接复用+=

	//注意：出了这个函数，tmp会被销毁，所以这里不能用引用返回。
	//	    这里是传值返回，所以会形成一个拷贝
	return tmp;
}

//日期 -= 天数 ：d1 - 100
//这里的d1 已经改了
Date& Date::operator-=(int day)
{
	if (day < 0)
	{
		return *this += -day;
	}

	_day -= day;

	while (_day <= 0)
	{
		--_month;
		if (_month == 0)
		{
			_month = 12;
			_year--;
		}

		//借上一个月的天数
		_day += GetMonthDay(_year, _month);
	}

	return *this;
}

Date Date::operator-(int day) const
{
	Date tmp = *this;
	tmp -= day;

	return tmp;
}


// ++d
Date& Date::operator++()
{
	*this += 1;
	return *this;
}

//这两种++ 建议用前置++，因为后置++会产生两次拷贝和一次析构，相比之下前置++有优势。
//d++
Date Date::operator++(int)
{
	Date tmp = *this;
	*this += 1;
	return tmp;
}


//--d
Date& Date::operator--()
{
	*this -= 1;
	return *this;
}

//d--
Date Date::operator--(int) 
{
	Date tmp(*this);
	*this -= 1;

	return tmp;
}

//思路：找出大的年份和小的年份，再定义一个计数器和小的年份一起++，直到和大的年份相等。
//d1 - d2
int Date::operator-(const Date& d) const
{
	Date max = *this;
	Date min = d;
	int flag = 1;

	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}

	int n = 0;
	while (min != max)
	{
		++min;
		++n;
	}

	return n * flag;
}

//流插入
//void operator<<(ostream& out)
//{
//	out << _year << "年" << _month << "月" << _day << "日" << endl;
//}

//void operator<<(ostream& out,const Date& d)
//{
//	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
//}


ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
	return out;
}

istream& operator>> (istream& in, Date& d)
{
	cout << "请依次输入年月日:>";

	in >> d._year >> d._month >> d._day;

	if (!d.CheakDate())
	{
		cout << "日期非法!" << endl;
	}

	return in;
}


