#pragma once

#include <iostream>
using namespace std;
#include <assert.h>
#include <stdbool.h>

class Date
{
    //友元函数声明：在类外面用对象访问私有变量
    friend ostream& operator<< (ostream& out, const Date& d);
    friend istream& operator>> (istream& in, Date& d);
public:
     //构造函数
    Date(int year, int month, int day);
    void Print() const;

    //获取某月的天数
    //由于这个函数一定会频繁调用，所以可以把它定义成内联函数
    //注意：直接在类里面定义，编译器默认是内联函数，inline可加可不加
    int GetMonthDay(int year, int month)
    {
        //断言，确保输入月份的有效性
        assert(month > 0 && month < 13);

        //枚举出月份的天数
        //注意：这里的 static，由于该函数频繁调用，
        //      把数组放在静态区，避免每次调用函数时每次都要开辟数组空间
       static int monthDayArray[13] = { -1, 31,28,31,30,31,30,31,31,30,31,30,31 };

        //判断2月的平年和闰年
        //注意：这里的 month == 2 和后面的取模运算的位置。
        //      首先满足是2月，再判断是否是闰年，效率会更高
        if (month == 2 && (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
        {
            return 29;
        }
        else
        {
            return monthDayArray[month];
        }
    }

    //检查日期的合法性
    bool CheakDate();

    //两个日期之间的比较
    bool operator< (const Date& d) const;
    bool operator<= (const Date& d) const;
    bool operator> (const Date& d) const;
    bool operator>= (const Date& d) const;
    bool operator==(const Date& d) const;
    bool operator!=(const Date& d) const;

    //d1 += 100，d1已经改变
    Date& operator+=(int day);
    Date& operator-=(int day);

    //d1 + 50，d1不变
    Date operator+(int day) const;
    Date operator-(int day) const;

    // d1 - d2
    int operator-(const Date& d) const;

    //前置：返回运算后的值
    //后置：返回运算前的值
    
    //首先我们来理解函数重载和运算符重载：
    // 函数重载：可以让函数名相同，参数不同的函数存在
    // 运算符重载：让自定义类型可以用运算符，并且控制运算符的行为，增强可读性。
    // 这两者各论各的，没有关系。
    // 但是，多个同一运算符重载可以构成函数重载。
    
    //规定：
    //这两种++是同一运算的不同形式 ，但是他们的函数名相同，参数都是隐含的this参数
    // 无法同时存在。所以为了区分，并且构成重载，强行给后置++函数的参数增加了一个
    // int形参，不需要写形参名。并且这个形参没有任何意义。
     
    // ++d1 -> d1.operator++()
    Date& operator++();

    // d1++ -> d1.operator++(1) 整数任意给
    Date operator++(int);

    //前置，后置--
    Date& operator--();
    Date operator--(int);

    //cout 和cin默认仅支持内置类型，不支持自定义类型。
    //比如：cout << d1;是错误的。自定义类型的要自己实现
    
    //流插入
    //注意：istream 和 ostream类只能传引用，因为它们禁止了拷贝构造。
    //不建议定义为成员函数，因为Date* this占据了第一个参数位置，使用d<<cout不习惯
    //void operator<<(ostream& out);

private:
    int _year;
    int _month;
    int _day;
};

//重载成全局
//void operator<< (ostream& out, const Date& d);

ostream& operator<< (ostream& out, const Date& d);
istream& operator>> (istream& in, Date& d);
