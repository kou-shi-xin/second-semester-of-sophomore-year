#define _CRT_SECURE_NO_WARNINGS 

#include "Date.h"

void TestDate1()
{
	
	Date d1(2024, 4, 14);
	Date d2 = d1 + 5000;
	d1.Print();
	d2.Print();

	Date d3(2024, 4, 14);
	Date d4 = d3 - 5000;
	d3.Print();
	d4.Print();

	Date d5 (2024, 4, 14);
	d5 += -5000;
	d5.Print();
}

void TestDate2()
{
	Date d1(2024, 4, 14);
	Date d2 = ++d1;
	d1.Print();
	d2.Print();

	Date d3 = d1++;
	d1.Print();
	d3.Print();

}

void TestDate3()
{
	const Date d1(2024, 4, 14);
	Date d2(2024, 5, 1);

	//cout << (d1 - d2) << endl;

	//调用自己定义的流插入时，需要这样，为啥？
	// 运算符重载中，参数顺序和操作数顺序是一致的
	// 但是如果把函数定义为成员函数，就无法避免问题，
	// 因为成员函数的第一个参数是隐含的this指针
	
	//结论：operator <<可以重载为成员函数，但是用起来不符合正常逻辑
	// 不建议这样处理，建议重载为全局函数
	
	//d1.operator <<(cout);
	//d1 << cout

	//cout << d1;

	//连续流插入
	//cout << d1 << d2;

	//cin >> d1;
	//cout << d1;
}


void TestDate4()
{
	Date d1(2024, 2, 25);
	Date d2(2024, 3, 14);

	cout << (d2 - d1) << endl;
}

int main()
{
	TestDate4();

	return 0;
}