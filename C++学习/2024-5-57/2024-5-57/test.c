#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

void Print(int* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void Swap(int* x, int* y)
{
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

void AdjustDown(int* arr, int sz, int parent)
{
	//默认是左孩子大
	int child = parent * 2 + 1;

	while (child < sz)
	{
		if (child + 1 < sz && arr[child + 1] > arr[child])
		{
			child += 1;
		}

		if ( arr[child] > arr[parent])
		{
			Swap(&arr[child], &arr[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapSort(int* arr, int sz)
{
	//childleft = parent*2+1
	//childright = parent*2+2
	//childd = (parent - 1)/2
	
	//建堆
	for (int i = (sz - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(arr, sz, i);
	}

	int end = sz - 1;
	while (end >= 0)
	{
		Swap(&arr[0], &arr[end]);
		AdjustDown(arr, end, 0);
		end--;

	}
}

//void Swap(int* p1, int* p2)
//{
//	int tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}
//
////sz是数组元素个数
//void AdjustDown(int* arr, int sz, int root)
//{
//	int parent = root;
//	int child = parent * 2 + 1;//默认孩子是左孩子
//
//	while (child < sz)
//	{
//		//选出左右孩子中较大的那一个
//		if (child + 1 < sz && arr[child + 1] > arr[child])
//		{
//			child += 1;
//		}
//		if (arr[child] > arr[parent])
//		{
//			Swap(&arr[child], &arr[parent]);
//			parent = child;
//			child = parent * 2 + 1;
//		}
//		else
//		{
//			break;
//		}
//	}
//}
//
//void HeapSort(int* arr, int sz)
//{
//	//建堆
//
//	//但是，如果我们要排升序，就要建大堆
//	for (int i = (sz - 1 - 1) / 2; i >= 0; i--)
//	{
//		AdjustDown(arr, sz, i);//建好了大堆
//	}
//
//	int end = sz - 1;
//	while (end > 0)
//	{
//		//把第一个最大的和最后一个交换，把它不看作堆里的
//		Swap(&arr[0], &arr[end]);
//
//		//再把前n-1个向下调整成升序，再选出次大的数
//		AdjustDown(arr, end, 0);//end是需要调整的个数，0是根参数，
//		//用的是数组第一个元素的下标
//		end--;
//	}
//}

int main()
{
	int arr[] = { 89,12,7,6,0,9,52,36,-1 ,-2};
	int sz = sizeof(arr) / sizeof(int);

	HeapSort(arr, sz);
	Print(arr, sz);

	return 0;
}