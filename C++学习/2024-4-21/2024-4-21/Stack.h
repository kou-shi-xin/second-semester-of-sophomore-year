#pragma once

class Stack
{
public:
	void Init();

private:
	int* _arr;
	int _top;
	int _capacity;
};
