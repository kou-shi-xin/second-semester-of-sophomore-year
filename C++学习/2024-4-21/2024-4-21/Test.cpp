#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
using namespace std;

#include <assert.h>

//C++兼容C中struct的用法
//类和对象
//一个类型  实例化  n个对象

//C++把struct 升级成了类
//1.类里面可以定义函数
//2.struct名称就可以代表类型

struct Stack
{
	//成员函数
	void Init(int n = 4)
	{
		arr = (int*)malloc(sizeof(int) * n);
		if (nullptr == arr)
		{
			perror("malloc申请空间失败");
			return;
		}

		capacity = n;
		top = 0;

	}

	//成员变量
	int* arr;
	int capacity;
	int top;
};

int main()
{
	Stack st1;
	Stack st2;
	Stack st3;

	return 0;
}

//struct 默认是公有
//一般把成员变量设置为私有，成员函数设置为公有
//class中只要不加限定符，默认是私有

class Stack
{
public://访问限定符：公有

	//成员函数
	void Init(int n = 4)
	{
		arr = (int*)malloc(sizeof(int) * n);
		if (nullptr == arr)
		{
			perror("malloc申请空间失败");
			return;
		}

		capacity = n;
		top = 0;

	}

	void Push(int x)
	{
		//……扩容

		arr[top++] = x;
	}

	int Top()
	{
		assert(top > 0);
		return arr[top - 1];
	}

private://私有

	//成员变量
	int* arr;
	int capacity;
	int top;
};


//int main()
//{
//	//class Stack st1;
//	Stack st2;
//
//	st2.Init();
//	st2.Push(1);
//	st2.Push(2);
//	st2.Push(3);
//	st2.Push(4);
//
//	//防止出现下面的代码
//	//cout << st2.arr[st2.top] << endl;//err
//
//	//可以强制程序员规范的使用，避免各种乱七八糟的问题
//	cout << st2.Top() << endl;
//
//	return 0;
//}


//为什么变量前要加下划线：区分成员变量
class Data
{
public:
	void Init(int day, int month, int year)
	{
		_day = day;
		_month = month;
		_year = year;

		//这样把自己赋给自己，会无法区分，所以赋值失败
		/*day = day;
		month = month;
		year = year;*/
	}

private:
	int _day; // day_  m_day
	int _month;
	int _year;
};

//int main()
//{
//	Data d;
//	d.Init(2024, 4, 21);
//
//	return 0;
//}

#include "Stack.h"

int main()
{
	//类 -->图纸  实例化对象--> 用图纸造房子

	//用类实例化一个对象时，才是变量的定义。这时开了空间
	Stack st1;
	Stack st2;

	//实例化不同的对象调用成员函数的地址都是一样的。
	//成员函数存放在一块公共的区域(公共代码段)
	st1.Init();
	st2.Init();

	//都可以计算出类的大小
	cout << sizeof(st1) << endl;  //根据造出的房子测量
	cout << sizeof(Stack) << endl; //根据图纸上的尺寸

	return 0;
}


// 类中什么都没有---空类 --> 1byte
//这一个字节，不存储有效数据，只是标识对象被定义出来了
class A3
{};


// 类中仅有成员函数  -->  1byte
//每个类至少开一个字节
class A2 
{
public:
	void f2() {}
};


//int main()
//{
//	//类 -->图纸  实例化对象--> 用图纸造房子
//
//	//用类实例化一个对象时，才是变量的定义。这时开了空间
//	Stack st1;
//	Stack st2;
//
//	A3 a;
//	A3 b;
//
//	//实例化不同的对象调用成员函数的地址都是一样的。
//	//成员函数存放在一块公共的区域(公共代码段)
//	st1.Init();
//	st2.Init();
//
//	//都可以计算出类的大小
//	cout << sizeof(st1) << endl;  //根据造出的房子测量
//	cout << sizeof(Stack) << endl; //根据图纸上的尺寸
//	cout << sizeof(A3) << endl;
//
//	return 0;
//}


//隐含的this指针:
// 1.形参和实参的位置不能显示写，编译器自己加
// 2.但是可以在类里面可以显示用

//注释的部分就是编译器的处理
class Date
{
public:
	void Init(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	//void Print(Data* this)
	//void Print(Data* const this)
	void Print()
	{
		//cout << this->_year << "-" << this->_month << "-" << this->_day << endl;
		cout << _year << "-" << _month << "-" << _day << endl;
	}

private:
	int _year;  // 年
	int _month; // 月
	int _day;   // 日
};

//int main()
//{
//	Date d1, d2;
//
//	//d1,d2调用的是同一个函数，怎么做到分别打印出不同日期的?
//	d1.Init(2024, 4, 20);
//	d2.Init(2024, 4, 21);
//
//	d1.Print();
//	//d1.Print(&d1);
//
//	d2.Print();
//	//d2.Print(&d2);
//
//	return 0;
//}


//举例
//class A
//{
//public:
//	void Print()
//	{
//		cout << "Print()" << endl;
//	}
//private:
//	int _a;
//};
//
//int main()
//{
//	A* p = nullptr;
//	p->Print();
//	return 0;
//}


class A
{
public:
	void PrintA()
	{
		cout << _a << endl;
	}
private:
	int _a;
};
int main()
{
	A* p = nullptr;
	p->PrintA();
	return 0;
}
