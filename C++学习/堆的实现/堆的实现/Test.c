#define _CRT_SECURE_NO_WARNINGS 

#include "Heap.h"

int main()
{
	int a[] = { 50,100,70,65,60,32 ,25};

	HP hp;
	HeapInit(&hp);

	for (int i = 0; i < sizeof(a) / sizeof(int); i++)
	{
		HeapPush(&hp, a[i]);
	}

	while (!HeapEmpty(&hp))
	{
		printf("%d ", HeapTop(&hp));
		HeapPop(&hp);
	}

	HeapDestroy(&hp);

	return 0;
}