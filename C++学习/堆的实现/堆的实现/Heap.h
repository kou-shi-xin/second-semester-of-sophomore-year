#pragma once

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>

typedef int HpDateType;

typedef struct Heap
{
	HpDateType* a;
	int size;
	int capacity;
}HP;


//��ʼ����
void HeapInit(HP* php);

//���ٶ�
void HeapDestroy(HP* php);

//�ѵĲ���
void HeapPush(HP* php, HpDateType x);

//�Ѷ���ɾ��
void HeapPop(HP* php);

//��ȡ�Ѷ�Ԫ��
HpDateType HeapTop(HP* php);

//���Ƿ�Ϊ��
bool HeapEmpty(HP* php);

