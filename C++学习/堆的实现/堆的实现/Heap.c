#define _CRT_SECURE_NO_WARNINGS 

#include "Heap.h"

//初始化堆
void HeapInit(HP* php)
{
	php->a = (HpDateType*)malloc(sizeof(HpDateType) * 4);
	if (php->a == NULL)
	{
		return;
	}
	php->capacity = 4;
	php->size = 0;//指向下一个元素
}

//销毁堆
void HeapDestroy(HP* php)
{
	free(php->a);
	php->capacity = 0;
	php->size = 0;
}

void Swap(HpDateType* px, HpDateType* py)
{
	int tmp = *px;
	*px = *py;
	*py = tmp;
}

//向上调整算法
//物理结构是数组，本质是对数组的操作，每次插入都相当于尾插
//每次插入一个数，都要调整成堆，假设建小堆
void AdjustUP(HpDateType* a, int n)
{
	//确定孩子和父亲的位置
	//孩子的位置在数组中是最后一个元素
	//在二叉树中是最后一个叶子节点
	int child = n - 1;
	int parent = (child - 1) / 2;

	while (child < n)
	{
		//如果孩子小于父亲，就交换位置
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			//此时把孩子移到父亲的位置，再更新父亲的位置
			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

//向下调整算法
void AdjustDown(HpDateType* a, int n, int parent)
{
	int child = parent * 2 + 1;//默认左孩子最小
	while (child < n)
	{
		//用假设法，如果不是，就换右孩子
		if (a[child] > a[child + 1])
		{
			child += 1;
		}
		//找出左右孩子小的那个与父亲交换
		if (child+1 < n && a[child] < a[child + 1])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
	}
	
}

//堆的插入
void HeapPush(HP* php, HpDateType x)
{
	assert(php);

	if (php->capacity == php->size)
	{
		HpDateType* tmp = (HpDateType*)realloc(php->a, sizeof(HpDateType) * php->capacity * 2);
		if (tmp == NULL)
		{
			perror("realloc fail");
		}
		else
		{
			php->a = tmp;
			php->capacity *= 2;
		}
	}
	php->a[php->size] = x;
	php->size++;

	//向上调整算法
	AdjustUP(php->a, php->size);
}

//堆的删除
void HeapPop(HP* php)
{
	assert(php && php->size > 0);

	//堆的删除，本质删除的是堆顶元素
	//首尾交换位置，再删除
	Swap(&php->a[0], &php->a[php->size - 1]);
	php->size--;

	//向下调整算法
	AdjustDown(php->a, php->size, 0);
}

//获取堆顶元素
HpDateType HeapTop(HP* php)
{
	assert(php);

	return php->a[0];
}

//堆是否为空
bool HeapEmpty(HP* php)
{
	assert(php);

	return php->size == 0;
}

