#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <iostream>
using namespace std;

//int main()
//{
//	int year = 2000;
//	int month = 4;
//	int day = 7;
//
//	printf("%04d-%02d-%02d", year, month, day);
//
//	return 0;
//}


//void Swap(int& left, int& right)
//{
//	int temp = left;
//	left = right;
//	right = temp;
//}
//
//void Swap(double& left, double& right)
//{
//	double temp = left;
//	left = right;
//	right = temp;
//}
//
//void Swap(char& left, char& right)
//{
//	char temp = left;
//	left = right;
//	right = temp;
//}


//函数模板
//原理：需要什么，编译器帮你实例化
//template<class T>
template<typename T>
void Swap(T& left, T& right)
{
	T tmp = left;
	left = right;
	right = tmp;
}

//int main()
//{
//	int a = 0, b = 1;
//	Swap(a, b);
//
//	double c = 1.1, d = 2.2;
//	Swap(c, d);
//
//	//Swap函数在库函数里有
//	swap(a, b);
//
//	return 0;
//}

//template<class T>
//T Add(const T& left, const T& right)
//{
//	cout << "T Add(const T& left, const T& right)" << endl;
//
//	return left + right;
//}

//如果非要定义成两个模板参数，可以用auto做返回值，自己推类型
//template<class T1,class T2>
//auto Add(const T1& left, const T2& right)
//{
//	cout << "auto Add(const T1& left, const T2& right)" << endl;
//
//	return left + right;
//}


//int main()
//{
//	int a1 = 10, a2 = 20;
//	double d1 = 10.0, d2 = 20.0;
//
//	Add(a1, a2);
//	Add(d1, d2);
//
//	//Add(a1, d2);//err 推演时会报错
//
//	Add(a1, (int)d2);//ok
//	Add((double)a1, d2);//ok
//
//	//显式实例化,此时不需要推演，直接指定类型
//	Add<int>(a1, d2); //d2会隐式转换成int
//	Add<double>(a1, d2); //a1会隐式转换成double
//
//	Add(a1, d2);//ok
//}


//注意下面的最优选择
//当注释掉两个半成品时，仍能通过编译

// 专门处理int的加法函数  现成的
int Add(int left, int right)
{
	cout << "int Add(int left, int right)" << endl;

	return left + right;
}

// 通用加法函数  半成品
//template<class T>
//T Add(T left, T right)
//{
//	cout << "T Add(T left, T right)" << endl;
//
//	return left + right;
//}

////半成品
//template<class T1, class T2>
//auto Add(const T1& left, const T2& right)
//{
//	cout << "auto Add(const T1& left, const T2& right)" << endl;
//
//	return left + right;
//}


//1.都有的情况，优先匹配普通函数+参数类型匹配的(成品+口味对)
//2.没有普通函数，优先函数模板+参数类型匹配(半成品+口味对)
//3.只有一个，类型转换一下也能用，也可以匹配调用
//int main()
//{
//	Add(1, 2); // 与非函数模板类型完全匹配，不需要函数模板实例化
//
//	Add(1.1, 2.2);
//
//	Add(1, 2.0); // 模板函数可以生成更加匹配的版本，编译器根据实参生成更加匹配的Add函数
//
//	return 0;
//}


// 动态顺序表
// 注意：Vector不是具体的类，是编译器根据被实例化的类型生成具体类的模具
template<class T>
class Vector
{
public:
	Vector(size_t capacity = 10)
		: _pData(new T[capacity])
		, _size(0)
		, _capacity(capacity)
	{}

	// 使用析构函数演示：在类中声明，在类外定义。
	~Vector();

	void PushBack(const T& data)；
		void PopBack()；
		// ...

		size_t Size() { return _size; }

	T& operator[](size_t pos)
	{
		assert(pos < _size);
		return _pData[pos];
	}

private:
	T* _pData;
	size_t _size;
	size_t _capacity;
};


//模板不建议声明和定义分离到.h 和 .cpp，会出现链接错误。
// 注意：类模板中函数放在类外进行定义时，需要加模板参数列表
template <class T>
Vector<T>::~Vector()
{
	if (_pData)
		delete[] _pData;
	_size = _capacity = 0;
}


int main()
{
	// Vector类名，Vector<int>才是类型
	Vector<int> s1;
	Vector<double> s2;

	return 0;
}