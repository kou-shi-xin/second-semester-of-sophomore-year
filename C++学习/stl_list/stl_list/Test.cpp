#define _CRT_SECURE_NO_WARNINGS 

#include <list>
#include <iostream>
#include <algorithm>
using namespace std;

void test_list1()
{
	//由于list的底层是带头双向循环链表
	// list的访问方式是迭代器，不支持下标访问

	//string和vector的是随机迭代器
	//list的迭代器是双向迭代器，不是随机迭代器，
	// 所以只支持++和--，由于效率原因不支持+和-

	list<int> it = { 5,9,7,4,1,2 };
	list<int>::iterator it1 = it.begin();
	while (it1 != it.end())
	{
		cout << *it1 << " ";
		it1++;
	}
	cout << endl;

	for (auto e : it)
	{
		cout << e <<" ";
	}
	cout << endl;
}

void test_list2()
{
	//list有自己的sort函数，底层是归并排序
	//list不能用算法库里的sort，因为这个sort传的是随机迭代器
	//list自己的sort函数效率太低，如果要排大量数据，
	// 建议把数据导入vector，再使用算法库里的sort

	list<int> it = { 5,9,4,7,4,1,2 };

	//list<int>::iterator it1 = it.begin();

	//it.sort();//默认排升序

	it.sort(greater<int>());//排降序

	for (auto e : it)
	{
		cout << e << " ";
	}
	cout << endl;

	//在有序的前提下：去重
	it.unique();
	for (auto e : it)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_list3()
{
	//值(区间)的转移(粘贴)，本质是把一个链表的节点移到另一个链表里

	std::list<int> mylist1, mylist2;
	std::list<int>::iterator it;

	for (int i = 1; i <= 4; ++i)
		mylist1.push_back(i); // mylist1: 1 2 3 4

	for (int i = 1; i <= 3; ++i)
		mylist2.push_back(i * 10);// mylist2: 10 20 30

	it = mylist1.begin();
	++it;  // points to 2

	//使用splice把mylist2的值全部转移到mylist1中，此时mylist2空了
	mylist1.splice(it, mylist2); // mylist1: 1 10 20 30 2 3 4

	for (auto e : mylist1)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_list4()
{
	std::list<int> mylist1;

	for (int i = 1; i <= 4; ++i)
		mylist1.push_back(i); // mylist1: 1 2 3 4

	//把3转移到链表的头部
	auto it = find(mylist1.begin(), mylist1.end(), 3);
	mylist1.splice(mylist1.begin(), mylist1, it);

	for (auto e : mylist1)
	{
		cout << e << " ";
	}
	cout << endl;
}
									

int main()
{
	test_list4();

	return 0;
}