#pragma once

#include <iostream>
#include <assert.h>
#include <stdbool.h>

using namespace std;

namespace bit
{
	class string
	{
	public:
		//������
		typedef char* iterator;
		typedef char* const_iterator;

		iterator begin();
		iterator end();

		const_iterator begin()const;
		const_iterator end()const;

		string(const char* str = "");
		string(const string& s);
		~string();

		//��ֵ����
		string& operator=(string& s);

		//Ԥ���ռ�
		void reserve(size_t n);

		//β���ַ�/�ַ���
		void push_back(char ch);
		void append( const char* str);


		char* c_str()const;

		size_t size()const;

		char& operator[](size_t pos);
		char& operator[](size_t pos)const;

		//+=���������
		string& operator+=(char ch);
		string& operator+=(const char* str);

		//��posλ�ò����ַ�/�ַ���
		void insert(size_t pos, char ch);
		void insert(size_t pos, const char* str);

		//��posλ��ɾ������Ϊlen
		void erase(size_t pos = 0, size_t len = npos);

		//��posλ�ò����ַ�/�ַ���
		size_t find(char ch, size_t pos = 0);
		size_t find(const char* str, size_t pos = 0);

		void swap(string& s);

		//��ָ��λ��ȡlen���ַ��Ĵ�
		string substr(size_t pos = 0, size_t len = npos) const;

		//�ַ����ıȽ�
		bool operator>(string& s)const;
		bool operator>=(string& s)const;
		bool operator<(string& s)const;
		bool operator<=(string& s)const;
		bool operator==(string& s)const;
		bool operator!=(string& s)const;

		//���
		void clear();

	private:
		char* _str;
		size_t _size;
		size_t _capacity;

		const static size_t npos;
	};

	//������/����ȡ
	ostream& operator<< (ostream& os, const string& str);
	istream& operator>> (istream& is, string& str);
}
