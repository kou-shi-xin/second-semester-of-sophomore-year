#define _CRT_SECURE_NO_WARNINGS 

#include "string.h"

namespace bit
{
	const size_t string::npos = -1;//易错:多写static,忘写类域

	string::string(const char* str)
		:_size(strlen(str))
	{
		_str = new char[_size + 1];
		_capacity = _size;
		strcpy(_str, str);
	}

	//s2(s1)
	//易忘写const
	string::string(const string& s)
	{
		_str = new char[s._capacity + 1];
		strcpy(_str, s._str);
		_size = s._size;
		_capacity = s._capacity;
	}

	//s2 = s1
	string& string::operator=(string& s)
	{
		if (this != &s)
		{
			char* tmp = new char[s._capacity + 1];
			strcpy(tmp, s._str);
			delete[] s._str;
			_str = tmp;
			_size = s._size;
			_capacity = s._capacity;

			return *this;
		}
	}

	char* string:: c_str()const
	{
		return _str;
	}

	size_t string::size()const
	{
		return _size;
	}

	char& string::operator[](size_t pos)
	{
		assert(pos < _size);//避免越界
		
		return _str[pos];
	}

	char& string::operator[](size_t pos)const
	{
		assert(pos < _size);//避免越界
		
		return _str[pos];
	}

	string::iterator string::begin()
	{
		return _str;
	}

	string::iterator string::end()
	{
		return _str + _size;
	}

	string::const_iterator string::begin()const
	{
		return _str;
	}

	string::const_iterator string::end()const
	{
		return _str + _size;
	}

	//预留空间
	void string::reserve(size_t n)
	{
		char* tmp = new char[n + 1];
		strcpy(tmp, _str);
		delete[] _str;
		_str = tmp;
		_capacity = n;
	}

	//尾插字符/字符串
	void string::push_back( char ch)
	{
		if (_size == _capacity)
		{
			size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
			reserve(newcapacity);
		}

		_str[_size] = ch;
		_str[_size + 1] = '\0';
		_size++;
	}

	void string::append( const char* str)
	{
		size_t len = strlen(str);
		if (_size + len > _capacity)
		{
			reserve(_size+len);
		}

		strcpy(_str + _size, str);
		_size += len;
	}

	//+=运算符重载
	string& string::operator+=(char ch)
	{
		push_back(ch);
		return *this;
	}

	string& string::operator+=(const char* str)
	{
		append(str);
		return *this;
	}

	//在pos位置插入字符/字符串
	void string::insert(size_t pos, char ch)
	{
		assert(pos <= _size);

		if (_size == _capacity)
		{
			size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
			reserve(newcapacity);
		}

		//方式1
		int end = _size + 1;
		while (end >= (int)pos)
		{
			_str[end] = _str[end - 1];
			end--;
		}

		//方式2
		/*size_t end = _size;
		while (end >= pos)
		{
			_str[end + 1] = _str[end];
			end--;
		}*/

		_str[pos] = ch;
		_size++;
	}

	void string::insert(size_t pos, const char* str)
	{
		assert(pos <= _size);

		size_t len = strlen(str);

		if (_size + len > _capacity)
		{
			reserve(_size + len);
		}
		
		int end = _size + len;
		while (end >= (int)pos + len - 1)
		{
			_str[end] = _str[end -len ];
			end--;
		}

		memcpy(_str + pos, str, len);
		_size += len;
	}

	//在pos位置删除长度为len
	void string::erase(size_t pos, size_t len)
	{
		assert(pos < _size);

		//len大于后面的字符个数时
		if (_size - pos <= len)
		{
			_str[pos] = '\0';
			_size -= len;
		}
		else
		{
			strcpy(_str + pos, _str + pos + len);
			_size -= len;
		}
	}

	//查找字符/字符串
	size_t string::find(char ch, size_t pos)
	{
		for (size_t i = pos; i < _size; i++)
		{
			if (_str[i] == ch)
			{
				return i;
			}
		}
		return npos;
	}

	size_t string::find(const char* str, size_t pos)
	{
		const char* p = strstr(_str, str);
		return p - _str;
	}

	void string::swap(string& s)
	{
		std::swap(_str, s._str);
		std::swap(_size, s._size);
		std::swap(_capacity, s._capacity);
	}

	string string::substr(size_t pos, size_t len) const
	{
		assert(pos < _size);

		if (len >= _size - pos)
		{
			string sub(_str + len);
			return sub;
		}
		else
		{
			string sub;
			sub.reserve(len);
			for (size_t i = 0; i < len; i++)
			{
				sub += _str[pos + i];
			}
			return sub;
		}
	}

	//字符串的比较
	bool string::operator>(string& s)const
	{
		return strcmp(_str, s._str) > 0;
	}

	bool string::operator>=(string& s)const
	{
		return (*this > s) || (*this == s);
	}

	bool string::operator<(string& s)const
	{
		return !(*this >= s);
	}

	bool string::operator<=(string& s)const
	{
		return !(*this > s);
	}

	bool string::operator==(string& s)const
	{
		return strcmp(_str, s._str) == 0;
	}

	bool string::operator!=(string& s)const
	{
		return !(*this == s);
	}

	//清除
	void string::clear()
	{
		_str[0] = '\0';
		_size = 0;
	}

	//流插入/流提取
	ostream& operator<< (ostream& os, const string& str)
	{
		for (size_t i = 0; i < str.size(); i++)
		{
			os << str[i];
		}
		return os;
	}

	istream& operator>> (istream& is, string& str)
	{
		str.clear();

		char ch = is.get();
		while (ch != ' ' && ch != '\n')
		{
			str += ch;//易忘
			ch = is.get();
		}
		return is;
	}

	string::~string()
	{
		delete[] _str;
		_str = nullptr;
		_size = 0;
		_capacity = 0;
	}
}