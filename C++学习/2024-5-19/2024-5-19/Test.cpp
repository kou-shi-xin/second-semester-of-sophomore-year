#define _CRT_SECURE_NO_WARNINGS

#include "string.h"

namespace bit
{
	void test_string1()
	{
		string s1("hello world");
		//string s2(s1);

		cout << s1.c_str() << endl;
		//cout << s2.c_str() << endl;

		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s1[i] << " ";
		}
		cout << endl;

		string::iterator it1 = s1.begin();
		while (it1 != s1.end())
		{
			cout << *it1 << " ";
			it1++;
		}
		cout << endl;

		for (auto e : s1)
		{
			cout << e << " ";
		}
	}

	void test_string2()
	{
		string s1("hello world");

		s1.push_back('x');
		s1.append("aaaa");

		s1 += 'b';
		s1 += "zzz";

		cout << s1.c_str() << endl;
	}


	void test_string3()
	{
		string s1("hello world");
		//s1.insert(11, 'z');
		//s1.insert(0, 'v');

		//s1.insert(3, "xxxx");
		//s1.insert(0, "xxxx");

		//s1.erase(3, 4);
		s1.erase(3);


		cout << s1.c_str() << endl;
	}

	void test_string4()
	{
		string s1("hello world");
		//cout << s1.find('e') << endl;

		string s2(s1);
		cout << s2.c_str() << endl;

		string s3 = s1;
		cout << s3.c_str() << endl;
	}

	void test_string5()
	{
		string s1("hello");
		string s2("zzzzzzzzzzzzzzzzzzzzz");
		s1.swap(s2);
		cout << s1.c_str() << endl;
		cout << s2.c_str() << endl;
	}

	void test_string6()
	{
		string s1("hello");
		cout << s1 << endl;

		//string s2;
		cin >> s1;
		cout << s1 << endl;
	}


	void test_string7()
	{
		// 取出url中的域名
		string url("http://www.cplusplus.com/reference/string/string/find/");
		size_t pos1 = url.find(':');
		string url1 = url.substr(0, pos1 - 0);
		cout << url1.c_str() << endl;//http

		size_t pos2 = url.find('/', pos1 + 3);
		string url2 = url.substr(pos1 + 3, pos2 - (pos1 + 3));
		cout << url2.c_str() << endl;//www.cplusplus.com

		string url3 = url.substr(pos2 + 1);
		cout << url3.c_str() << endl;//reference/string/string/find/
	}
}

int main()
{
	bit::test_string7();

	return 0;
}