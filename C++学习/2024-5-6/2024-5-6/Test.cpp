#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
using namespace std;

//class A
//{
//public:
//	//构造函数
//	//单参数
//	A(int a = 0)
//		:_a(a)
//	{
//		cout << "A(int a = 0)" << endl;
//	}
//	//多参数
//	A(int a1,int a2)
//	{
//		cout << "A(int a1,int a2)" << endl;
//	}
//
//	//拷贝构造
//	A(const A& aa)
//	{
//		cout << "A(const A& aa)" << endl;
//	}
//
//	//析构函数
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//private:
//	int _a;
//
//};

//int main()
//{
//	//内置类型
//
//	//new除了用法方便(不用强转，不用检查)，和malloc没有区别，也不会初始化
//	//但是它可以初始化
//
//	//动态申请一个int对象
//	//int* p1 = new int;
//
//	//动态申请一块连续内存
//	//int* p2 = new int[10];
//
//	int* p1 = new int(0);//初始化
//
//	int* p2 = new int[10] {1, 2, 3, 4, 5, 6};//初始化
//	
//	//释放空间
//	delete p1;
//	delete[] p2;
//
//	return 0;
//}

//int main()
//{
//	//自定义类型
//	//malloc对于自定义类型也无法初始化
//	//A* p1 = (A*)malloc(sizeof(A));
//
//	//开空间/释放空间，还会调用构造和析构
//	A* p2 = new A;
//	A* p3 = new A(2);
//
//	delete p2;
//	delete p3;
//
//	cout << endl;
//
//	//n个对象的开辟和销毁
//	//调用n次构造，n次析构
//	//A* p4 = new A[5];
//
//	//自定义类型的初始化
//	//1可以隐式转换成A，用1构造一个临时对象，这个临时对象又进行拷贝构造
//	//编译器又把构造+拷贝构造优化成构造
//	//单参数
//	//A* p4 = new A[5]{ 1,2,3,4 };
//	
//	//多参数
//	//同样支持隐式类型转换，同样调用构造函数
//	A* p4 = new A[10]{ 1,2,3,4 ,{6,7} };
//
//	delete[] p4;
//
//	return 0;
//}

//#include <stack>

//new 和delete 的底层
//int main()
//{
//	//编译器看到new操作符后，就会转换成operator new + 构造函数
//	//而operator new又是malloc的封装
//	A* p2 = new A;
//
//	//转换成 析构 + operator delete
//	//注意：析构 和 operator delete 释放的不是同一块空间
//	//operator delete 释放的是p2指向的这块空间
//	//析构 的是A对象里面的资源的清理(有资源清理资源，没资源调用函数)
//	delete p2;
//
//	stack<int>* p3 = new stack<int>;
//	delete p3;
//
//	return 0;
//}

//new的抛异常

class A
{
public:
	A(int a = 0)
		: _a(a)
	{
		cout << "A():" << this << endl;
	}

	~A()
	{
		cout << "~A():" << this << endl;
	}
private:
	int _a;
};

//int main()
//{
//	A* p1 = new A;
//	A* p2 = new A[10]; //44字节
//}

int main()
{
	//自定义类型
	A* p2 = new A[10]; //44字节
	//程序崩溃！
	//如果把类中的析构函数注释，程序正常运行。为啥？
	//原因：当类中有显式析构函数，new开辟自定义类型的空间时，
	//new会在这块空间的前面多申请4个字节空间，用来存对象的个数，
	// 那么存这个数据干嘛用呢？方便delete[]时，让编译器知道析构时，调用多少次函数。
	//free(p2);//err

	//delete[] 会往前偏移
	delete p2;//ok

	

	//内置类型
	//交叉使用不报错
	//int* p3 = new int[10]; //40字节
	//free(p3);

	return 0;
}


//定位new表达式(placement-new)

//int main()
//{
//	A* p1 = (A*)operator new(sizeof(A));
//	//p1->A(); //err 不支持这样显式调用构造
//
//	new(p1)A;// 对已有的空间，显式调用构造
//	new(p1)A(10);//可以进行初始化
//
//	p1->~A(); //ok
//	operator delete(p1);
//
//	return 0;
//}
