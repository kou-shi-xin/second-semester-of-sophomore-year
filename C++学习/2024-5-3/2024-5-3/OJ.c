#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdbool.h>
//
//typedef char STQType;
//typedef struct Stack
//{
//    STQType* a;
//    int top;
//    int capacity;
//}ST;
//
//void STInit(ST* ps)
//{
//    ps->a = (STQType*)malloc(sizeof(STQType) * 4);
//    ps->top = 0;//指向栈顶元素的下一个位置
//    ps->capacity = 4;
//}
//
//void STDestroy(ST* ps)
//{
//    free(ps->a);
//    ps->a == NULL;
//    ps->top = 0;
//    ps->capacity = 0;
//}
////栈顶插入元素
//void STPush(ST* ps, STQType x)
//{
//    assert(ps);
//    //检查扩容
//    if (ps->top == ps->capacity)
//    {
//        ps->a = (STQType*)realloc(ps->a, sizeof(STQType) * ps->capacity * 2);
//        ps->capacity *= 2;
//    }
//
//    ps->a[ps->top++] = x;
//}
////获取栈顶元素
//STQType STtop(ST* ps)
//{
//    assert(ps && ps->top > 0);
//    return ps->a[ps->top - 1];
//}
////删除栈顶元素
//void STPop(ST* ps)
//{
//    assert(ps && ps->top > 0);
//    ps->top--;
//}
////判断栈是否为空，为空返回非零，不为空返回0
//bool STEmpty(ST* ps)
//{
//    assert(ps);
//    return ps->top == 0;
//}

//bool isValid(char* s) {
//    ST st;
//    STInit(&st);
//    //遇到左括号就入栈，右括号就出栈
//    while (*s)
//    {
//        if (*s == '(' || *s == '[' || *s == '{')
//        {
//            STPush(&st, *s);
//        }
//        else
//        {
//            //右括号比左括号多
//            if (STEmpty(&st))
//            {
//                STDestroy(&st);
//                return false;
//            }
//
//            char top = STtop(&st);
//            STPop(&st);
//
//            //括号不匹配时
//            if ((*s == ')' && top != '(')
//                || (*s == ']' && top != '[')
//                || (*s == '}' && top != '{'))
//            {
//                STDestroy(&st);
//                return false;
//            }
//        }
//        s++;
//    }
//
//    //左括号比右括号多，栈不为空
//    bool ret = STEmpty(&st);
//    STDestroy(&st);
//    return ret;
//}
//
//
///**
// * Definition for a Node.
// * struct Node {
// *     int val;
// *     struct Node *next;
// *     struct Node *random;
// * };
// */
//typedef struct Node Node;
//struct Node* copyRandomList(struct Node* head) {
//    Node* cur = head;
//    //申请拷贝节点在原链表的后面，再进行链接
//    while (cur)
//    {
//        Node* copy = (struct Node*)malloc(sizeof(struct Node));
//        copy->val = cur->val;
//
//        //链接
//        copy->next = cur->next;
//        cur->next = copy;
//
//        //迭代
//        cur = cur->next->next;
//    }
//    //控制random指针的指向
//    cur = head;
//    while (cur)
//    {
//        Node* copy = cur->next;
//
//        if (cur->random == NULL)
//        {
//            copy->random = NULL;
//        }
//        else
//        {
//            copy->random = cur->random->next;
//        }
//
//        //迭代
//        cur = cur->next->next;
//    }
//    //建立新链表，把拷贝指针解下来，进行尾插
//    Node* copyhead = NULL, * tail = NULL;
//    cur = head;
//    while (cur)
//    {
//        Node* copy = cur->next;
//        Node* next = copy->next;
//        if (tail == NULL)
//        {
//            copyhead = tail = copy;
//        }
//        else
//        {
//            tail->next = copy;
//            tail = copy;
//        }
//        //恢复原链表
//        cur->next = next;
//        cur = next;
//    }
//
//    return copyhead;
//}
//
//
///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     struct ListNode *next;
// * };
// */
//typedef struct ListNode LNode;
//struct ListNode* detectCycle(struct ListNode* head) {
//    LNode* slow = head, * fast = head;
//    while (fast && fast->next)
//    {
//        //用快慢指针找到相遇点
//        slow = slow->next;
//        fast = fast->next->next;
//
//        if (slow == fast)
//        {
//            LNode* meet = slow;
//            while (meet != head)
//            {
//                head = head->next;
//                meet = meet->next;
//            }
//            return head;
//        }
//        //一个指针从头开始，一个指针从相遇点开始，会在入口点相遇
//
//    }
//    return false;
//}
//
//
///**
// * Definition for singly-linked list.
// * struct ListNode {
// *     int val;
// *     struct ListNode *next;
// * };
// */
//typedef struct ListNode LNode;
//
//bool hasCycle(struct ListNode* head) {
//    LNode* slow = head, * fast = head;
//
//    while (fast && fast->next)
//    {
//        slow = slow->next;
//        fast = fast->next->next;
//
//        if (slow == fast)
//        {
//            return true;
//        }
//
//    }
//    return false;
//}


typedef int QTypeData;
typedef struct QueueNode
{
    struct QueueNode* next;
    QTypeData val;
}QNode;

typedef struct Queue
{
    QNode* phead;
    QNode* ptail;
    int size;//元素个数
}Queue;

void QueueInit(Queue* pq)
{
    assert(pq);

    pq->phead = NULL;
    pq->ptail = NULL;
    pq->size = 0;
}

void QueueDestroy(Queue* pq)
{
    QNode* cur = pq->phead;
    while (cur)
    {
        QNode* next = cur->next;
        free(cur);
        cur = next;
    }
    //置空
    pq->phead = pq->ptail = NULL;
}

//队尾入  尾插
void QueuePush(Queue* pq, QTypeData x)
{
    assert(pq);

    //申请新节点
    QNode* newNode = (QNode*)malloc(sizeof(QNode));
    newNode->next = NULL;
    newNode->val = x;

    //没有节点时
    if (pq->ptail == NULL)
    {
        pq->ptail = pq->phead = newNode;
    }
    else
    {
        //有多个节点时
        pq->ptail->next = newNode;
        pq->ptail = newNode;
    }
    pq->size++;
}

//队头出  头删
void QueuePop(Queue* pq)
{
    //防止传空指针
    assert(pq);
    //防止队列为空
    assert(pq->phead);

    //只有一个节点时
    if (pq->phead->next == NULL)
    {
        free(pq->phead);
        //防止ptail成为野指针
        pq->phead = pq->ptail = NULL;
    }
    else
    {
        //有多个节点时
        QNode* cur = pq->phead->next;
        free(pq->phead);
        pq->phead = cur;
    }
    pq->size--;
}

//获取头部元素
QTypeData QueueFront(Queue* pq)
{
    assert(pq && pq->phead);

    return pq->phead->val;
}

//获取队尾元素
QTypeData QueueBack(Queue* pq)
{
    assert(pq && pq->phead);

    return pq->ptail->val;
}

//获取队列的元素个数
int QueueSize(Queue* pq)
{
    assert(pq);

    return pq->size;
}

//队列是否为空，为空返回非零，不为空返回0
bool QueueEmpty(Queue* pq)
{
    assert(pq);

    return pq->phead == NULL;
}

//用两个队列模拟实现栈。
//保持一个队列存数据，一个队列为空
//入数据：入到不为空的队列
//出数据：把前size-1个数据倒入空队列中，取出最后一个

typedef struct {
    //定义两个队列
    Queue q1;
    Queue q2;

} MyStack;

MyStack* myStackCreate()
{
    //要在堆上申请一个栈
    MyStack* pst = (MyStack*)malloc(sizeof(MyStack));

    //初始化栈,相当于初始化两个队列
    //QueueInit(Queue* pq)
    QueueInit(&pst->q1);
    QueueInit(&pst->q2);

    return pst;
}

void myStackPush(MyStack* obj, int x) {
    //把数据插入到不为空队列中
    if (!QueueEmpty(&obj->q1))
    {
        QueuePush(&obj->q1, x);
    }
    else
    {
        QueuePush(&obj->q2, x);
    }
}

int myStackPop(MyStack* obj) {
    //把不为空的队列中的前size-1个倒入另一个队列中，删除最后一个
    Queue* pEmptyQ = &obj->q1;
    Queue* pNonEmptyQ = &obj->q2;
    if (!QueueEmpty(&obj->q1))
    {
        pNonEmptyQ = &obj->q1;
        pEmptyQ = &obj->q2;
    }
    //把不为空的队列中的前size-1个倒入另一个队列中
    while (QueueSize(pNonEmptyQ) > 1)
    {
        int front = QueueFront(pNonEmptyQ);
        QueuePop(pNonEmptyQ);
        QueuePush(pEmptyQ, front);
    }

    //此时非空队列中的最后一个元素就相当于栈的顶部元素
    int front = QueueFront(pNonEmptyQ);
    QueuePop(pNonEmptyQ);

    return front;
}

int myStackTop(MyStack* obj) {
    //队列和栈的特性相反，队列的尾，此时就相当于栈的顶
    if (!QueueEmpty(&obj->q1))
    {
        return QueueBack(&obj->q1);
    }
    else
    {
        return QueueBack(&obj->q2);
    }
}

bool myStackEmpty(MyStack* obj) {

    return QueueEmpty(&obj->q1) && QueueEmpty(&obj->q2);
}

void myStackFree(MyStack* obj) {
    QueueDestroy(&obj->q1);
    QueueDestroy(&obj->q2);

    free(obj);
}
