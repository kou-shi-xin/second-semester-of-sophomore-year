#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

//struct S
//{
//	int i;
//	int arr[];//柔性数组成员
//};
//
//int main()
//{
//	int sz = sizeof(struct S);
//	printf("%d\n", sz);
//
//	return 0;
//}

#include <stdlib.h>
#include <string.h>
#include <errno.h>

//柔性数组的使用
//int main()
//{
//	//struct S s;//4 柔性数组没有申请空间
//
//	struct S* ps=(struct S*)malloc(sizeof(struct S) + 40);//40字节是给柔性数组开辟的
//	if (ps == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 1;
//	}
//	ps->i = 100;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		ps->arr[i] = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", ps->arr[i]);
//	}
//
//	//调整空间，柔性数组柔性的体现
//	struct S* ptr = (struct S*)realloc(ps, sizeof(struct S) + 80);
//	if (ptr != NULL)
//	{
//		ps = ptr;
//		ptr = NULL;
//	}
//	//……
//
//	//释放
//	free(ps);
//	ps = NULL;
//
//	return 0;
//}


//struct S
//{
//	int n;
//	int* arr;
//};
//
//int main()
//{
//	struct S*ps = (struct S*)malloc(sizeof(struct S));
//	if (ps == NULL)
//	{
//		return 1;
//	}
//	ps->n = 100;
//	ps->arr = (int*)malloc(40);
//	if (ps->arr == NULL)
//	{
//		return 1;
//	}
//	//使用
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		ps->arr[i] = i;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", ps->arr[i]);
//	}
//
//	//扩容
//	int* ptr = (int*)realloc(ps->arr, 80);
//	if (ptr != NULL)
//	{
//		ps->arr = ptr;
//	}
//
//	//释放
//	free(ps->arr);
//	free(ps);
//	ps = NULL;
//
//	return 0;
//}



//struct S
//{
//	int i;
//	int arr[];//柔性数组成员
//};
//
//int main()
//{
//	int sz = sizeof(struct S);
//	printf("%d\n", sz);
//
//	return 0;
//}


