#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdbool.h>

//前序遍历
void _prevOrder(struct TreeNode* root, int* a, int* pi)
{
    if (root == NULL)
        return;

    a[*pi] = root->val;
    (*pi)++;

    _prevOrder(root->left, a, pi);
    _prevOrder(root->right, a, pi);
}

//由于不知道数组开多大的空间，所以要提前计算树的结点个数
int TreeSize(struct TreeNode* root)
{
    return root == NULL ? 0 : TreeSize(root->left) + TreeSize(root->right) + 1;
}

int* preorderTraversal(struct TreeNode* root, int* returnSize) {
    int size = TreeSize(root);

    //开辟数组空间
    int* a = (int*)malloc(sizeof(int) * size);

    int i = 0;
    _prevOrder(root, a, &i);

    *returnSize = size;

    return a;
}


//二叉树的最大深度
int maxDepth(struct TreeNode* root) {
    if (root == NULL)
    {
        return 0;
    }

    int leftDepth = maxDepth(root->left);
    int rightDepth = maxDepth(root->right);

    return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
}


//平衡二叉树
int maxDepth(struct TreeNode* root) {
    if (root == NULL)
    {
        return 0;
    }

    int leftDepth = maxDepth(root->left);
    int rightDepth = maxDepth(root->right);

    return leftDepth > rightDepth ? leftDepth + 1 : rightDepth + 1;
}

bool isBalanced(struct TreeNode* root) {
    if (root == NULL)
        return ture;

    int leftDepth = maxDepth(root->left);
    int rightDepth = maxDepth(root->right);

    //先检查自己满不满足，再递归检查左子树，右子树满不满足
    return abs(leftDepth - rightDepth) < 2
        && isBalanced(root->left)
        && isBalanced(root->right);
}


//遍历二叉树

//定义结点
typedef struct TreeNode
{
    struct TreeNode* left;
    struct TreeNode* right;
    char val;
}TNode;

//创建二叉树
TNode* CreateTree(char* a, int* pi)
{
    if (a[*pi] == '#')
    {
        (*pi)++;
        return NULL;
    }

    TNode* root = (TNode*)malloc(sizeof(TNode));
    if (root == NULL)
    {
        perror("malloc fail!\n");
        exit(-1);
    }

    root->val = a[*pi];
    (*pi)++;

    root->left = CreateTree(a, pi);
    root->right = CreateTree(a, pi);

    return root;
}

void InOrder(TNode* root)
{
    if (root == NULL)
        return;

    InOrder(root->left);
    printf("%c ", root->val);
    InOrder(root->right);
}

int main() {
    char str[100] = { 0 };
    scanf("%s", str);

    int i = 0;

    //构建一棵树
    TNode* root = CreateTree(str, &i);

    InOrder(root);

    return 0;
}
