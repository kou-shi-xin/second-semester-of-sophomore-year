#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>


//用malloc开辟3*5二维数组并赋值

//int main()
//{
//	int** arr = (int**)malloc(sizeof(int*) * 3);
//	if (arr == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//
//	for (int i = 0; i < 3; i++)
//	{
//		arr[i] = (int*)malloc(sizeof(int)*5);
//		if (arr[i] == NULL)
//		{
//			perror("malloc");
//			return 1;
//		}
//	}
//	//进行赋值
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			arr[i][j] = 1;
//		}
//	}
//    //打印
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//
//	//释放空间
//	for (int i = 0; i < 3; i++)
//	{
//		free(arr[i]);
//		arr[i] = NULL;
//	}
//
//	return 0;
//}


//写程序拷贝文件
//void copy_data()
//{
//	FILE* pf1 = fopen("data.txt", "r");
//	if (fopen == NULL)
//	{
//		perror("fopen pf1");
//		return;
//	}
//
//	FILE* pf2 = fopen("data_copy.txt", "w");
//	if (fopen == NULL)
//	{
//		perror("fopen pf2");
//		fclose(pf1);///
//		return;
//	}
//
//	//边读边拷贝
//	char ch = 0;
//	while ((ch = fgetc(pf1)) != EOF)
//	{
//		fputc(ch, pf2);
//	}
//
//	fclose(pf1);
//	pf1 = NULL;
//	fclose(pf2);
//	pf2 = NULL;
//}
//int main()
//{
//	copy_data();
//
//	return 0;
//}

typedef char BTDataType;

typedef struct BinaryTreeNode
{
	struct BinaryTree* left;
	struct BinaryTree* right;
	BTDataType data;

}BTNode;

//前序
void PrevOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("NULL ");
		return;
	}

	printf("%c ", root->data);
	PrevOrder(root->left);
	PrevOrder(root->right);

}

int main()
{
	//开辟各个节点
	BTNode* A = (BTNode*)malloc(sizeof(BTNode));
	if (A == NULL)
	{
		perror("malloc A");
		return;
	}
	A->data = 'A';
	A->left = NULL;
	A->right = NULL;

	BTNode* B = (BTNode*)malloc(sizeof(BTNode));
	if (B == NULL)
	{
		perror("malloc B");
		return;
	}
	B->data = 'B';
	B->left = NULL;
	B->right = NULL;

	BTNode* C = (BTNode*)malloc(sizeof(BTNode));
	if (C == NULL)
	{
		perror("malloc C");
		return;
	}
	C->data = 'C';
	C->left = NULL;
	C->right = NULL;

	BTNode* D = (BTNode*)malloc(sizeof(BTNode));
	if (D == NULL)
	{
		perror("malloc D");
		return;
	}
	D->data = 'D';
	D->left = NULL;
	D->right = NULL;

	BTNode* E = (BTNode*)malloc(sizeof(BTNode));
	if (E == NULL)
	{
		perror("malloc E");
		return;
	}
	E->data = 'E';
	E->left = NULL;
	E->right = NULL;

	//链接各个节点
	A->left = B;
	A->right = C;
	B->left = D;
	B->right = E;

	//调用函数，打印出前序结构
	PrevOrder(A);
	printf("\n");

	return 0;
}