#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int STDataType;

typedef struct Stack
{
	STDataType* a;
	STDataType top;//定义栈顶
	size_t capacity;//栈的容量
}ST;

//初始化栈
void StackInit(ST* ps);

//销毁栈
void StackDestory(ST* ps);

//从栈顶插入数据
void StackPush(ST* ps, STDataType x);

//从栈顶删除数据
void StackPop(ST* ps);

//获取栈顶元素
STDataType StackTop(ST* ps);

//获取栈内有效元素个数
int StackSize(ST* ps);

//判断栈内是否为空，如果为空返回非0结果，不为空返回0
bool StackEmpty(ST* ps);