#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>
#include "Stack.h"
#include <time.h>



void InsertSort(int* arr, int sz)
{
	for (int i = 0; i < sz - 1; i++)
	{
		int end = i;
		int tmp = arr[end + 1];//保存下一个值

		while (end >= 0)
		{
			if (tmp < arr[end])
			{
				arr[end + 1] = arr[end];//把前面的数按顺序往后挪
				end--;
			}
			else
			{
				break; //比前一个数要大时，说明找到了位置
			}
		}
		arr[end+1] = tmp;//包含了两种情况：1是在比较的过程中比前一个数要大，就把tmp放进去
	                         //2是把前面的数都比完了，此时end==-1了，就把tmp放到最前面

	}
}

void ShellSort(int* arr, int sz)
{
	int gap = sz;
	while (gap > 1)
	{
		gap = gap / 2;   // O（log2N)
		//gap = gap / 3 + 1;   // O（log3N)
		//gap>1时，都是预排序         接近有序
		//gap==1时,就是直接插入排序   有序


		//当gap很大时，下面两层循环预排序接近O（N）
		//当gap很小时，数组已经很接近有序了，差不多也是O（N）

		//对多组间隔为gap的数据同时排序
		for (int i = 0; i < sz - gap; i++)
		{
			int end = i;
			int tmp = arr[end + gap];

			while (end >= 0)
			{
				if (tmp > arr[end])
				{
					arr[end + gap] = arr[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			arr[end + gap] = tmp;
		}
	}
	
}


void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}


//向下调整算法:
//假设建大堆
void AdjustDown(int* arr, int sz, int root)
{
	int parent = root;
	int child = parent * 2 + 1;//默认孩子是左孩子

	while (child <sz)
	{
		//选出左右孩子中较小的那一个
		if (child + 1 < sz && arr[child + 1] > arr[child])
		{
			child += 1;
		}
		if ( arr[child] > arr[parent])
		{
			Swap(&arr[child], &arr[parent]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}


void HeapSort(int* arr, int sz)
{
	//建堆

	//但是，如果我们要排升序，就要建大堆
	for (int i = (sz - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDown(arr, sz, i);//建好了大堆
	}

	int end = sz - 1;
	while (end > 0)
	{
		//把第一个最大的和最后一个交换，把它不看作堆里的
		Swap(&arr[0], &arr[end]);
		//再把前n-1个向下调整成升序，再选出次大的数
		AdjustDown(arr, end, 0);//end是需要调整的个数，0是根参数，用的是数组第一个元素的下标
		end--;
	}
}

void SelectSort(int* arr, int sz)
{
	int begin = 0;
	int end = sz - 1;

	while (begin < end)
	{
		int maxi = begin;
		int mini = end;

		//循环找出当前数中的最大数和最小数
		for (int i = begin; i <= end; i++)
		{
			if (arr[i] > arr[maxi])
			{
				maxi = i;
			}
			if (arr[i] < arr[mini])
			{
				mini = i;
			}
		}
		Swap(&arr[begin], &arr[mini]);

		//如果begin和maxi重叠，就要修正一下maxi的位置
		if (begin == maxi)
		{
			maxi = mini;
		}
		Swap(&arr[end], &arr[maxi]);
		begin++;
		end--;
	}
}


void BubbleSort(int* arr, int sz)
{

	for (int j = 0; j < sz; j++)
	{
		int exchange = 0;//默认是有序的
		//一趟排序
		for (int i = 1; i < sz-j; i++)
		{
			if (arr[i - 1] > arr[i])
			{
				//前一个比后一个大，就交换
				Swap(&arr[i - 1], &arr[i]);
				exchange = 1; //如果不是有序的就发生了交换，exchange=1
			}
		}
		//如果一趟比较过后发现是有序的，就直接跳出循环
		if (exchange == 0)
		{
			break;
		}
	}
}


//三数取中
int GetMidIndex(int* arr, int left, int right)
{
	int mid = (left + right) >> 1;

	if (arr[mid] > arr[left])
	{
		if (arr[mid] < arr[right])
		{
			return mid;
		}
		else if(arr[left]>arr[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
	else//arr[mid] < arr[left]
	{
		if (arr[mid] > arr[right])
		{
			return mid;
		}
		else if (arr[left] < arr[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
}


//挖坑法
int PartSort1(int* arr, int left, int right)
{
	int index = GetMidIndex(arr, left, right);
	//  Swap(&arr[index], &arr[left]);//交换一下，保证key还是最左边的数

	int begin = left;
	int end = right;
	int key = arr[begin];
	int pivot = begin;

	//这是排一趟，只排好了一个数
	while (begin < end)
	{
		//左边有坑，右边end找比key小的
		while (begin < end && arr[end] > key)
		{
			end--;
		}

		//小的放到了左边的坑里，右边end自己形成了新的坑
		arr[pivot] = arr[end];
		pivot = end;

		//右边有坑，左边end找比key大的
		while (begin < end && arr[begin] < key)
		{
			begin++;
		}

		//大的放到右边的坑里，左边begin自己形成新的坑
		arr[pivot] = arr[begin];
		pivot = begin;
	}

	//最后begin和end相遇了，把key放入该位置
	pivot = begin;
	arr[begin] = key;

	return key;
}


//左右指针法
int PartSort2(int* arr, int left, int right)
{
	int index = GetMidIndex(arr, left, right);
	Swap(&arr[index], &arr[left]);//交换一下，保证key还是最左边的数

	int begin = left;
	int end = right;
	int keyi = begin;

	while (begin < end)
	{
		//找比key小的
		while (begin < end && arr[keyi] <= arr[end])
		{
			end--;
		}
		//找比key大的
		while (begin < end && arr[keyi] >= arr[begin])
		{
			begin++;
		}
		Swap(&arr[begin], &arr[end]);
	}

	//当begin与end相遇时
	Swap(&arr[begin], &arr[keyi]);

	return begin;
}

//前后指针法
int  PartSort3(int* arr, int left, int right)
{
	int index = GetMidIndex(arr, left, right);
	Swap(&arr[index], &arr[left]);//交换一下，保证key还是最左边的数

	int keyi = left;
	int prev = left;
	int cur = left + 1;

	while (cur <= right)
	{
		if (arr[cur] < arr[keyi]&& ++prev != cur)//++prev != cur是指当cur和prev重合时不用多于的交换
		{
			//prev++;
			Swap(&arr[cur], &arr[prev]);
		}
		cur++;
	}
	Swap(&arr[keyi], &arr[prev]);
	return prev;
}

//快速排序的非递归
void QuickSortNonR(int* arr, int n)
{
	ST st;
	StackInit(&st);

	StackPush(&st, n - 1);
	StackPush(&st, 0);

	while (!StackEmpty(&st))
	{
		int left = StackTop(&st);
		StackPop(&st);

		int right = StackTop(&st);
		StackPop(&st);

		int keyIndex = PartSort1(arr, left, right);
		//[left keyIndex-1] keyIndex [keyIndex right]

		if (keyIndex + 1 < right)
		{
			StackPush(&st, right);
			StackPush(&st, keyIndex + 1);
		}

		if (left < keyIndex - 1)
		{
			StackPush(&st, keyIndex - 1);
			StackPush(&st, left);
		}
	}

	StackDestory(&st);
}


void QuickySort(int* arr, int left,int right)
{
	if (left >= right)//当左右子区间不存在，或只有一个元素时，就不需要递归了，排序完成
	{
		return;
	}

	//排序一趟
	//int keyindex = PartSort1(arr, left, right);
	//int keyindex = PartSort2(arr, left, right);
	int keyindex = PartSort3(arr, left, right);


	//[left] pivot [right]
	// [left pivot-1]  pivot [pivot+1 right]
	//左子区间和右子区间右序，整体就有序了

	QuickySort(arr, left, keyindex - 1);
	QuickySort(arr, keyindex + 1, right);

	////小区间优化法:减少递归调用次数
	//if (keyindex - 1 - left > 100)
	//{
	//	QuickySort(arr, left, keyindex - 1);
	//}
	//else
	//{
	//	InsertSort(arr + left, keyindex - 1 - left + 1);
	//}

	//if (right - (keyindex + 1) > 100)
	//{
	//	QuickySort(arr, keyindex + 1, right);
	//}
	//else
	//{
	//	InsertSort(arr + keyindex + 1, right - (keyindex + 1) + 1);
	//}

}


//把数组分成区间
void _MergrSort(int* arr,int left,int right,int* tmp)
{
	if (left >= right)
		return ;

	int mid = (left + right) >> 1;

	//假设[left mid] [mid+1 right]有序，就可以归并
	_MergrSort(arr, left, mid, tmp);
	_MergrSort(arr, mid + 1, right, tmp);

	int begin1 = left;
	int end1 = mid;
	int begin2 = mid + 1;
	int end2 = right;
	int index = left;

	while (begin1 <= end1 && begin2 <= end2)
	{
		if (arr[begin1] < arr[begin2])
		{
			tmp[index++] = arr[begin1++];
		}
		else
		{
			tmp[index++] = arr[begin2++];

		}
	}

	//当其中有一个区间结束时，另一个有序区间的数据直接拷贝进临时数组里
	while (begin1 <= end1)
	{
		tmp[index++] = arr[begin1++];
	}

	while (begin2 <= end2)
	{
		tmp[index++] = arr[begin2++];
	}

	//最后把临时数组里面的数据拷贝回原数组
	for (int i = left; i <= right; i++)
	{
		arr[i] = tmp[i];
	}
}


//归并排序的非递归
void MergrSortNonR(int* arr, int sz)
{
	int* tmp = (int*)malloc(sizeof(int) * sz);
	if (tmp == NULL)
	{
		perror("malloc");
		return;
	}

	int gap = 1;//每组数据的个数

	while (gap < sz)
	{

		for (int i = 0; i < sz; i += 2 * gap)
		{
			// [i, i+gap-1]  [i+gap,i+2*gap-1]
			int begin1 = i;
			int end1 = i + gap - 1;

			int begin2 = i + gap;
			int end2 = i + 2 * gap - 1;

			int index = i;

			while (begin1 <= end1 && begin2 <= end2)
			{
				if (arr[begin1] < arr[begin2])
				{
					tmp[index++] = arr[begin1++];
				}
				else
				{
					tmp[index++] = arr[begin2++];

				}
			}

			//当其中有一个区间结束时，另一个有序区间的数据直接拷贝进临时数组里
			while (begin1 <= end1)
			{
				tmp[index++] = arr[begin1++];
			}

			while (begin2 <= end2)
			{
				tmp[index++] = arr[begin2++];
			}
		}

		//最后把临时数组里面的数据拷贝回原数组
		for (int j = 0; j < sz; j++)
		{
			arr[j] = tmp[j];
		}

		gap *= 2;
	}
	free(tmp);

}


void MergrSort(int* arr, int sz)
{
	int* tmp = (int*)malloc(sizeof(int) * sz);
	if (tmp == NULL)
	{
		perror("malloc");
		return;
	}
	_MergrSort(arr, 0, sz - 1, tmp);

	free(tmp);
	tmp = NULL;
}


void CountSort(int* arr, int sz)
{
	int max = arr[0];
	int min = arr[0];

	//找出最大值和最小值
	for (int i = 0; i < sz; i++)
	{
		if (arr[i] > max)
		{
			max = arr[i];
		}

		if (arr[i] < min)
		{
			min = arr[i];
		}
	}

	//求出范围
	int range = max - min + 1; 

	int* count = (int *)calloc(range, sizeof(int)*range);
	if (count == NULL)
	{
		perror("calloc fail!\n");
		return;
	}

	//统计每个数字出现的次数，相对映射到开辟的数组中
	for (int i = 0; i < sz; i++)
	{
		count[arr[i] - min]++;
	}

	//把数据进行排序，放回原数组中
	int j = 0;
	for (int i = 0; i < range; i++)
	{
		while (count[i]--)
		{
			arr[j++] = i + min;
		}
	}
	free(count);
	count = NULL;

}

void PrintArray(int* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");

}


// 测试排序的性能对比
void TestOP()
{
	srand(time(0));
	const int N = 100000;
	int* a1 = (int*)malloc(sizeof(int) * N);
	int* a2 = (int*)malloc(sizeof(int) * N);
	int* a3 = (int*)malloc(sizeof(int) * N);
	int* a4 = (int*)malloc(sizeof(int) * N);
	int* a5 = (int*)malloc(sizeof(int) * N);
	int* a6 = (int*)malloc(sizeof(int) * N);

	for (int i = 0; i < N; ++i)
	{
		a1[i] = rand();
		//a2[i] = a1[i];
		a3[i] = a1[i];
		a4[i] = a1[i];
		//a5[i] = a1[i];
		//a6[i] = a1[i];
	}
	/*int begin1 = clock();
	InsertSort(a1, N);
	int end1 = clock();*/

	/*int begin2 = clock();
	ShellSort(a2, N);
	int end2 = clock();*/

	int begin3 = clock();
	SelectSort(a3, N);
	int end3 = clock();

	int begin4 = clock();
	HeapSort(a4, N);
	int end4 = clock();

	//int begin5 = clock();
	//QuickSort(a5, 0, N - 1);
	//int end5 = clock();

	//int begin6 = clock();
	//MergeSort(a6, N);
	//int end6 = clock();

	//printf("InsertSort:%d\n", end1 - begin1);
	//printf("ShellSort:%d\n", end2 - begin2);
	printf("SelectSort:%d\n", end3 - begin3);
	printf("HeapSort:%d\n", end4 - begin4);
	//printf("QuickSort:%d\n", end5 - begin5);
	//printf("MergeSort:%d\n", end6 - begin6);
	//free(a1);
	//free(a2);
	free(a3);
	free(a4);
    //free(a5);
	//free(a6);
}

int main()
{
	//TestOP();

	int arr[] = { 6,7,9,2,4,3,5,1,0,8,-1};
	int sz = sizeof(arr) / sizeof(int);

	//计数排序
	CountSort(arr, sz);
	PrintArray(arr, sz);


	//归并排序
	//MergrSort(arr, sz);

	//MergrSortNonR(arr, sz);

	//快速排序
	//QuickySort(arr, 0, sz - 1);

	//希尔排序
	//ShellSort(arr, sz);

	//堆排序
	//HeapSort(arr, sz);

	//直接选择排序
	//SelectSort(arr, sz);


	

	//直接插入排序
	//InsertSort(arr, sz);

	//冒泡排序
	//BubbleSort(arr, sz);

	
	//QuickSortNonR(arr, sz);


	

	//计数排序
	//CountSort(arr, sz);

	return 0;
}
