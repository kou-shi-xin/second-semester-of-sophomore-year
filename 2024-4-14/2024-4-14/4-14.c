#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>

//struct S
//{
//	char name[20];
//	int age;
//	float score;
//
//};
//
//int main()
//{
//	struct S s = { "张三",20,65.5f };
//	//想把s中的数据存放在文件中
//	FILE* pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//写文件
//	fprintf(pf, "%s %d %f", s.name, s.age, s.score);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}


//struct S
//{
//	char name[20];
//	int age;
//	float score;
//
//};

//int main()
//{
//	struct S s = { 0 };
//	//想从test.txt文件中把数据放入s
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//
//	//读文件
//	fscanf(pf, "%s %d %f", s.name, &(s.age), &(s.score));
//
//	//想打印在屏幕上看看
//	printf("%s %d %f", s.name, s.age, s.score);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}


struct S
{
	char name[20];
	int age;
	float score;

};

int main()
{
	char buf[200] = { 0 };
	struct S s = { "张三",20,65.5f };

	//把结构体中格式化的数据转化成字符串
	sprintf(buf, "%s %d %f", s.name, s.age, s.score);

	//直接以字符串的形式打印
	printf("%s\n", buf);

	struct S t = { 0 };

	//把buf中的字符串转化成结构体t中格式化的数据
	sscanf(buf, "%s %d %f", t.name, &(t.age), &(t.score));

	//打印结构体t中的数据
	printf("%s %d %f\n", t.name, t.age, t.score);



	return 0;
}