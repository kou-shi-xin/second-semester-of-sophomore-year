#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//int main()
//{
//	printf("hehe\n");
//	main();
//
//	return 0;
//}

//int Fact(unsigned int n)
//{
//	if (n == 0)
//		return 1;
//	else
//		return n * Fact(n - 1);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret=Fact(n);
//	printf("%d\n", ret);
//
//	return 0;
//}


//void Print(int n)
//{
//	if (n > 9)
//	{
//		Print(n / 10);
//		printf("%d ", n % 10);
//	}
//	else
//		printf("%d ", n % 10);
//}

//可优化为：
//void Print(int n)
//{
//	if (n > 9)
//	{
//		Print(n / 10);
//	}
//	printf("%d ", n % 10);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	Print(n);
//
//	return 0;
//}


//用迭代法求第N个斐波那契数
//int Fib(int n)
//{
//	int a = 1;
//	int b = 1;
//	int c = 1;
//
//	while (n>=3)
//	{
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int ret = Fib(n);
//	printf("%d\n", ret);
//
//	return 0;
//}


 // 请计算一下Func1基本操作执行了多少次？
//void Func1(int N)
//{
//	int count = 0;
//	for (int i = 0; i < N; ++i)
//	{
//		for (int j = 0; j < N; ++j)
//		{
//			++count;
//		}
//	}
//	for (int k = 0; k < 2 * N; ++k)
//	{
//		++count; 
//	}
//
//	int M = 10;
//	while (M--)
//	{
//		++count;
//	}
//
//	printf("%d\n", count);
//}
//
////计算Func2的时间复杂度？
//void Func2(int N)
//{
//	int count = 0;
//
//	for (int k = 0; k < 2 * N; ++k)
//	{
//		++count;
//	}
//
//	int M = 10;
//	while (M--)
//	{
//		++count;
//	}
//
//	printf("%d\n", count);
//}

//void Bubble_Sort(int arr[], int sz)
//{
//	//趟数
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//每趟两两之间的比较
//		int j = 0;
//		for (j = 0; j < sz-1-i; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//}
//

//
//int main()
//{
//	int arr[] = { 9,8,7,6,5,4,3,2,1,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	Bubble_Sort(arr, sz);
//	Print_Arr(arr, sz);
//
//	return 0;
//}


//qsort(待排序的第一个元素的地址，待排序的元素的个数，待排序的每个元素的大小，函数指针）
// 
//排序整型
//int compar_int(const void* p1, const void* p2)
//{
//	return *(int*)p1 - *(int*)p2;
//}
//
//void Print_Arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//int main()
//{
//	int arr[] = { 5,2,4,1,6,9,8,6 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), compar_int);
//	Print_Arr(arr, sz);
//
//	return 0;
//}


//排序结构体
//struct Stu
//{
//	char name[20];
//	int age;
//};
//
////按名字排序
//int cmp_struct_name(const void* p1, const void* p2)
//{
//	return strcmp(((struct Stu*)p1)->name, ((struct Stu*)p2)->name);
//}
//
//void Print_struct(struct Stu* arr)
//{
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		printf("%s %d\n", arr[i].name, arr[i].age);
//	}
//}
//
//int main()
//{
//	struct Stu arr[3] = { {"zhangsan",20},{"lisi",23},{"wangwu",19} };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp_struct_name);
//	Print_struct(arr);
//
//	return 0;
//}