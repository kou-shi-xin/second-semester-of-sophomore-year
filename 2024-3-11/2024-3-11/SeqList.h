#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SQDateType;

typedef struct SeqList
{
	SQDateType* a;
	int size;     //有效数据
	int capacity; //容量
}SL;

//初始化顺序表
void SeqListInit(SL *ps);

//头部插入数据
void SepListPushFront(SL* ps, SQDateType x);

//打印数据
void SepListPushPrint(SL* ps);


//尾部插入数据
void SepListPushBack(SL* ps, SQDateType x);

//头部删除数据
void SepListPopFront(SL* ps);


//尾部删除数据
void SepListPopBack(SL* ps);


//任意位置插入数据
void SepListInsert(SL* ps,int pos, SQDateType x);

//任意位置删除数据
void SepListInsertErase(SL* ps,int pos);