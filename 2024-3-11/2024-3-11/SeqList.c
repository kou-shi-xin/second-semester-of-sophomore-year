#define _CRT_SECURE_NO_WARNINGS 

#include "SeqList.h"

void SeqListInit(SL* ps)
{
	ps->a = (SQDateType *)malloc( sizeof(SQDateType) * 4);
	if (ps->a == NULL)
	{
		printf("calloc fail!\n");
		return;
	}
	ps->size = 0;
	ps->capacity = 4;
}

void CheckSeqListCapacity(SL* ps)
{
	assert(ps);
	//检查容量是否已满
	if (ps->size == ps->capacity)
	{
		int newcapacity = ps->capacity * 2;
		SQDateType* tmp = (SQDateType*)realloc(ps->a, newcapacity * sizeof(SQDateType));
		//判断是否扩容成功
		if (tmp == NULL)
		{
			printf("realloc fail!\n");
			exit(-1);
		}
		else
		{
			ps->a = tmp;
			ps->capacity = newcapacity;
		}
	}
}

void SepListPushFront(SL* ps, SQDateType x)
{
	assert(ps);
	CheckSeqListCapacity(ps);
	for (int end =ps->size ; end >=0; end--)
	{
		ps->a[end] = ps->a[end - 1];
	}
	ps->size++;
	ps->a[0] = x;
}

void SepListPushBack(SL* ps, SQDateType x)
{
	CheckSeqListCapacity(ps);

	ps->a[ps->size] = x;
	ps->size++;

}

void SepListPopBack(SL* ps)
{
	assert(ps->size>0);
	
	ps->size--;
}

void SepListPopFront(SL* ps)
{
	assert(ps->size>0);
	
	for (int start = 0; start < ps->size; start++)
	{
		ps->a[start] = ps->a[start+1 ];
	}
	ps->size--;
}

void SepListInsert(SL* ps, int pos, SQDateType x)
{
	assert(pos < ps->size);
	CheckSeqListCapacity(ps);
	for (int end = ps->size - 1; end > pos; end--)
	{
		ps->a[end + 1] = ps->a[end];
	}

	/*int end = ps->size - 1;
	while (end >= pos)
	{
		ps->a[end + 1] = ps->a[end];
		end--;
	}*/
	ps->a[pos] = x;
	ps->size++;
}

void SepListErase(SL* ps, int pos)
{
	assert(pos < ps->size);
	for (int start = pos; start < ps->size; start++)
	{
		ps->a[start] = ps->a[start+1];
	}
	ps->size--;
}

void SepListPushPrint(const SL* ps)
{
	//判断顺序表中是否有数据
	assert(ps);
	if (ps->size == 0)
	{
		printf("顺序表内无数据!\n");
		return;
	}

	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}



