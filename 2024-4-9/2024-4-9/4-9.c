#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include "Stack.h"
#include <stdlib.h>

//int func(int n)
//{
//	return n == 1 ? 1 : n + func(n - 1);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//
//	int sum = func(n);
//
//	printf("%d\n", sum);
//
//	return 0;
//}



//void QuickySort(int* arr, int left, int right)
//{
//	//当左右子区间不存在，或只有一个元素时，
//	//就不需要递归了，排序完成
//	if (left >= right)
//	{
//		return;
//	}
//
//	int keyi = left;
//	int prev = left;
//	int cur = left + 1;
//
//	while (cur <= right)
//	{
//		//++prev != cur是指当cur和prev重合时不用多于的交换
//		if (arr[cur] < arr[keyi] && ++prev != cur)
//		{
//			Swap(&arr[cur], &arr[prev]);
//		}
//		cur++;
//	}
//	Swap(&arr[keyi], &arr[prev]);
//
//	//[left] pivot [right]
//	// [left pivot-1]  pivot [pivot+1 right]
//	//左子区间和右子区间有序，整体就有序了
//
//	QuickySort(arr, left, keyi - 1);
//	QuickySort(arr, keyi + 1, right);
//
//}


//int  PartSort3(int* arr, int left, int right)
//{
//	int index = GetMidIndex(arr, left, right);
//	Swap(&arr[index], &arr[left]);//交换一下，保证key还是最左边的数
//
//	int keyi = left;
//	int prev = left;
//	int cur = left + 1;
//
//	while (cur <= right)
//	{
//		//++prev != cur是指当cur和prev重合时不用多于的交换
//		if (arr[cur] < arr[keyi] && ++prev != cur)
//		{
//			Swap(&arr[cur], &arr[prev]);
//		}
//		cur++;
//	}
//	Swap(&arr[keyi], &arr[prev]);
//
//	return prev;
//}

//void Swap(int* p1, int* p2)
//{
//	int tmp = *p1;
//	*p1 = *p2;
//	*p2 = tmp;
//}


//void QuickSortNonR(int* arr, int n)
//{
//	ST st;
//	StackInit(&st);
//
//	//先入右，后入左
//	StackPush(&st, n - 1);
//	StackPush(&st, 0);
//
//	while (!StackEmpty(&st))
//	{
//		//先出左边界
//		int left = StackTop(&st);
//		StackPop(&st);
//
//		////后出右边界
//		int right = StackTop(&st);
//		StackPop(&st);
//
//		//int keyIndex = PartSort1(arr, left, right);
//		//[left keyIndex-1] keyIndex [keyIndex right]
//
//		int keyi = left;
//		int prev = left;
//		int cur = left + 1;
//
//		while (cur <= right)
//		{
//			//++prev != cur是指当cur和prev重合时不用多于的交换
//			if (arr[cur] < arr[keyi] && ++prev != cur)
//			{
//				Swap(&arr[cur], &arr[prev]);
//			}
//			cur++;
//		}
//		Swap(&arr[keyi], &arr[prev]);
//
//		keyi = prev;
//
//		if (keyi + 1 < right)
//		{
//			StackPush(&st, right);
//			StackPush(&st, keyi + 1);
//		}
//
//		if (left < keyi - 1)
//		{
//			StackPush(&st, keyi - 1);
//			StackPush(&st, left);
//		}
//	}
//
//	StackDestory(&st);
//}

// 时间复杂度都是O(N*logN)
void MergeSortNonR(int* a, int sz)
{
	int* tmp = (int*)malloc(sizeof(int) *sz);

	int gap = 1; // 每组数据个数

	while (gap < sz)
	{
		for (int i = 0; i < sz; i += 2 * gap)
		{
			// [i, i+gap-1] [i+gap,i+2*gap-1]
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;

			// 归并过程中右半区间可能就不存在
			if (begin2 >= sz)
				break;

			// 归并过程中右半区间算多了, 修正一下
			if (end2 >= sz)
			{
				end2 = sz - 1;
			}

			int index = i;
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] < a[begin2])
				{
					tmp[index++] = a[begin1++];
				}
				else
				{
					tmp[index++] = a[begin2++];
				}
			}

			while (begin1 <= end1)
			{
				tmp[index++] = a[begin1++];
			}

			while (begin2 <= end2)
			{
				tmp[index++] = a[begin2++];
			}

			// 归一部分，拷一部分，没归的就不拷
			for (int j = i; j <= end2; ++j)
			{
				a[j] = tmp[j];
			}
		}

		gap *= 2;
	}

	free(tmp);
}

void PrintArray(int* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");

}

int main()
{
	int arr[] = { 6,7,9,2,4,3,5,1,0,8,-1 };
	int sz = sizeof(arr) / sizeof(int);

	MergeSortNonR(arr, sz);
	PrintArray(arr, sz);


	//快速排序
	//QuickSortNonR(arr, sz);

	//QuickySort(arr, 0, sz - 1);

}