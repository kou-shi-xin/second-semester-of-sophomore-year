#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

typedef int QTDataType;

typedef struct QNode
{
	struct QNode* next;
	QTDataType data;
}QNode;

typedef struct Queue
{
	struct QNode* tail;
	struct QNode* head;
}Queue;

//初始化队列
void QueueInit(Queue* pq);

//销毁队列
void QueueDestory(Queue* pq);

//队尾入队列
void QueuePush(Queue* pq, QTDataType x);

//队头出队列
void QueuePop(Queue* pq);

//获取队列头部元素
QTDataType QueueFront(Queue* pq);

//获取队列尾部元素
QTDataType QueueBack(Queue* pq);

//判断队列是否为空，为空返回非0，不为空返回0
int QueueEmpty(Queue* pq);

//获取队列中有效元素的个数
int QueueSize(Queue* pq);


