#define _CRT_SECURE_NO_WARNINGS 

#include "Stack.h"
#include "Queue.h"

void StackTest()
{
	ST st;
	StackInit(&st);
	StackPush(&st, 1);
	StackPush(&st, 2);
	StackPush(&st, 3);
	StackPush(&st, 4);

	//打印栈内的数据，由于不能破坏栈的特性，所以不能遍历
	while (!StackEmpty(&st))
	{
		printf("%d ", StackTop(&st));
		StackPop(&st);
	}

	StackDestory(&st);
}


void QueueTest()
{
	Queue q;
	QueueInit(&q);

	QueueDestory(&q);

}


int main()
{
	//StackTest();
	QueueTest();


	return 0;
}