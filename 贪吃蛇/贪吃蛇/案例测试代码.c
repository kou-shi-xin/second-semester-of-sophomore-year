#define _CRT_SECURE_NO_WARNINGS 

#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <stdbool.h>
#include <locale.h>


//设置控制台的窗口大小，窗口名称
//int main()
//{
//	system("mode con cols=100 lines=30");
//	system("title 贪吃蛇");
//
//	getchar();
//	//system("pause");//暂停
//	return 0;
//}



//int main()
//{
//	//COORD pos = { 40,10 };//设置控制台窗口中的坐标
//	//CONSOLE_CURSOR_INFO cursor_info = { 0 };//控制台鼠标信息的指针，包含有关控制台光标的信息
//
//	//HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);//获取控制台的句柄
//	//GetConsoleCursorInfo(handle, &cursor_info);//获取指定句柄的控制台的鼠标信息
//
//	//改变控制台中光标的高度
//	//cursor_info.dwSize = 100;
//	//cursor_info.bVisible = false;//隐藏光标
//
//	//将改变后的高度进行设置
//	//SetConsoleCursorInfo(handle, &cursor_info);
//
//	return 0;
//}


//void SetPos(int x, int y)
//{
//	//获取设备句柄
//	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
//	//定位光标需要移动到的位置
//	COORD pos = { x ,y };
//	//将光标位置设置在到指定位置
//	SetConsoleCursorPosition(handle, pos);
//}

//int main()
//{
//	SetPos(20,5);
//	printf("hehe\n");//注意：位置是可以覆盖的
//
//	return 0;
//}

//获取按键情况，检测按键是否按过，1为按过，0为没2按过
//#define KEY_PRESS(vk)   (GetAsyncKeyState(vk) & 0x1 ? 1:0)
//
//int main()
//{
//	//检测数字按键是否按下
//	while (1)
//	{
//		if (KEY_PRESS(0x30))
//			printf("0\n");
//		else if (KEY_PRESS(0x31))
//			printf("1\n");
//		else if (KEY_PRESS(0x32))
//			printf("2\n"); 
//		else if (KEY_PRESS(0x33))
//			printf("3\n"); 
//		else if (KEY_PRESS(0x34))
//			printf("4\n"); 
//		else if (KEY_PRESS(0x35))
//			printf("5\n"); 
//		else if (KEY_PRESS(0x36))
//			printf("6\n"); 
//		else if (KEY_PRESS(0x37))
//			printf("7\n");
//		else if (KEY_PRESS(0x38))
//			printf("8\n"); 
//		else if (KEY_PRESS(0x39))
//			printf("9\n"); 
//		
//	}
//	return 0;
//}


//切换本地信息模式
//int main()
//{
//	char* loc;
//	loc = setlocale(LC_ALL, NULL);
//	printf("默认的本地信息：%s\n", loc);
//
//	loc = setlocale(LC_ALL, "");
//	printf("设置后的本地信息：%s\n", loc);
//
//	return 0;
//}


//宽字符的打印
//int main()
//{
//	setlocale(LC_ALL, "");
//
//	wchar_t ch1 = L'中';
//	wchar_t ch2 = L'国';
//	wchar_t ch3 = L'★';
//	wchar_t ch4 = L'●';
//
//	wprintf(L"%lc\n", ch1);
//	wprintf(L"%lc\n", ch2);
//	wprintf(L"%lc\n", ch3);
//	wprintf(L"%lc\n", ch4);
//
//	printf("ab\n");
//
//	return 0;
//}


