#define _CRT_SECURE_NO_WARNINGS 

#include "Snake.h"

void test()
{
	int ch = 0;
	do
	{
		//创建贪吃蛇
		Snake snake = { 0 };
		GameStart(&snake);//游戏开始前的初始化
		GameRun(&snake);//玩游戏的过程
		GameEnd(&snake);//游戏的善后工作

		SetPos(20, 15);
		printf("是否再来一局？(Y/N):");
	    getchar();
		ch = getchar();

	} while (ch == 'Y' || ch == 'y');
}


int main()
{
	//修改适配本地中文环境
	setlocale(LC_ALL, "");

	test();//贪吃蛇游戏的测试

	SetPos(0, 26);

	return 0;
}