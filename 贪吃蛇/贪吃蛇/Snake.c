#define _CRT_SECURE_NO_WARNINGS 

#include "Snake.h"

void SetPos(int x, int y)
{
	//获取设备句柄
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	//定位光标需要移动到的位置
	COORD pos = { x ,y };
	//将光标位置设置在到指定位置
	SetConsoleCursorPosition(handle, pos);
}


void WelcomeToGame()
{
	//欢迎信息
	SetPos(35, 10);
	printf("欢迎来到贪吃蛇小游戏!\n");
	SetPos(38, 20);
	system("pause");
	system("cls");//清屏

	//功能介绍信息
	SetPos(18, 10);
	printf("用↑.↓ . ← . →来控制蛇的移动，F3是加速，F4是减速。");
	SetPos(18, 11);
	printf("加速能够得到更高的分数!");
	SetPos(38, 20);
	system("pause");
	system("cls");

}


void CreateMap()
{
	int i = 0;

	//上
	SetPos(0, 0);
	for (i = 0; i <= 56; i += 2)
	{
		wprintf(L"%lc", WALL);
	}
	//下
	SetPos(0, 26);
	for (i = 0; i <= 56; i += 2)
	{
		wprintf(L"%lc", WALL);
	}
	//左
	for (i = 1; i <= 25; i++)
	{
		//每定位一次，打印一个
		SetPos(0, i);
		wprintf(L"%lc", WALL);
	}
	//右
	for (i = 1; i <= 25; i++)
	{
		SetPos(56, i);
		wprintf(L"%lc", WALL);
	}

}


void InitSnake(pSnake ps)
{
	//初始化5个蛇身节点
	pSnakeNode cur = NULL;

	int i = 0;
	for (i = 0; i < 5; i++)
	{
		cur = (pSnakeNode)malloc(sizeof(SnakeNode));
		if (cur == NULL)
		{
			perror("InitSnake:malloc():");
			return;
		}

		cur->x = POS_X + 2 * i;
		cur->y = POS_Y;
		cur->next = NULL;

		//用头插,把5个节点链在一起
		if (ps->pSnake == NULL)
		{
			ps->pSnake = cur;
		}
		else
		{
			cur->next = ps->pSnake;
			ps->pSnake = cur;
		}
	}


	//打印蛇身
	cur = ps->pSnake;
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}
	

	//贪吃蛇的其他信息初始化
	ps->dir = RIGHT;
	ps->FoodWeight = 10;
	ps->pFood = NULL;
	ps->Score = 0;
	ps->SleepTime = 180;//毫秒，就是2秒
	ps->status = OK;

}


void CreateFood(pSnake ps)
{
	//1.食物是随机出现的，坐标就是随机的
	//2.坐标必须在墙内
	//3.坐标不能在蛇身上

	int x = 0;
	int y = 0;

again:
	do
	{
		x = rand() % 53 + 2;
		y = rand() % 24 + 1;

	} while (x % 2 != 0);//生成的x坐标必须为偶数，蛇头的坐标是2的倍数，方便蛇头吃
	
	//坐标和蛇的身体的每个节点做比较，遍历比较
	pSnakeNode cur = ps->pSnake;
	while (cur)
	{
		if (x == cur->x && y == cur->y)
		{
			goto again;//当出现重合时，重新回去生成坐标
		}
		cur = cur->next;
	}

	//创建食物
	pSnakeNode pFood = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pFood == NULL)
	{
		perror("CreateFood::malloc()\n");
		return;
	}

	pFood->x = x;
	pFood->y = y;

	ps->pFood = pFood;
	SetPos(x, y);//定位在坐标的位置，产生食物
	wprintf(L"%lc", FOOD);

}


void GameStart(pSnake ps)
{
	//设置控制台的信息：窗口大小，窗口名
	system("mode con cols=100 lines=30");
	system("title 贪吃蛇");

	//隐藏光标
	//获取句柄
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	//获取控制台光标信息
	CONSOLE_CURSOR_INFO cursor_info = { 0 };
	GetConsoleCursorInfo(handle, &cursor_info);
	//隐藏光标
	cursor_info.bVisible = false;
	//将改变后的光标进行设置
	SetConsoleCursorInfo(handle, &cursor_info);

	//打印欢迎信息
	WelcomeToGame();
	
	//绘制地图
	CreateMap();

	//初始化蛇
	InitSnake(ps);

	//创建食物
	CreateFood(ps);
}

void PrintHelpInfo()
{
	SetPos(62, 15);
	printf("1.不能穿墙，不能咬到自己！");
	SetPos(62, 16);
	printf("2.用↑.↓ . ← . →来控制蛇的移动!");
	SetPos(62, 17);
	printf("3.F3是加速，F4是减速.");
	SetPos(62, 18);
	printf("4.ESC:退出游戏.space:暂停游戏.");

}

void Pause()
{
	while (1)
	{
		Sleep(100);
		if (KEY_PRESS(VK_SPACE))
		{
			break;
		}
	}
}

int NextIsFood(pSnake ps, pSnakeNode pNext)
{
	if (ps->pFood->x == pNext->x && ps->pFood->y == pNext->y)
		return 1;//下一个位置是食物
	else
		return 0;
}

void EatFood(pSnake ps, pSnakeNode pNext)
{
	//是食物，就把那个食物节点链到蛇头
	pNext->next = ps->pSnake;
	ps->pSnake = pNext;

	//打印出此时的蛇
	pSnakeNode cur = ps->pSnake;//指向蛇头
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}

	ps->Score += ps->FoodWeight;
	//吃掉食物后，食物节点就消失了
	free(ps->pFood);
	//再生成新的食物
	CreateFood(ps);
}


void NotEatFood(pSnake ps, pSnakeNode pNext)
{   
	//不是食物
	//用头插法，先把pNext节点挂上去
	pNext->next = ps->pSnake;
	ps->pSnake = pNext;

	//再把尾结点删除,保持蛇的原长
	pSnakeNode cur = ps->pSnake;
	while (cur->next->next != NULL)
	{
		//重新打印蛇身
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);

		cur = cur->next;
	}
	//把尾结点的位置打印成空白字符
	SetPos(cur->next->x, cur->next->y);
	printf("  ");

	free(cur->next);
	cur->next = NULL;//易错

}


//检测是否撞墙
void KillByWall(pSnake ps)
{
	if (ps->pSnake->x == 0 ||  //左
		ps->pSnake->x == 56 || //右
		ps->pSnake->y == 0 ||  //上
		ps->pSnake->y == 26)  //下
	{
		ps->status = KILL_BY_WALL;
	}
}


//检测是否撞到自己
void KillBySeif(pSnake ps)
{
	//判断头结点是否撞到头之后的节点，
	pSnakeNode cur = ps->pSnake->next;//从第二个节点开始
	while (cur)
	{
		if (cur->x == ps->pSnake->x && cur->y == ps->pSnake->y)
		{
			ps->status = KILL_BY_SELF;
			return;  //由于有循环,撞到自己时结束循环
		}
		cur = cur->next;
	}
}


void SnakeMove(pSnake ps)
{
	//可以专门创建一个节点，判断它是否与食物在同一位置，是就直接吃掉
	pSnakeNode pNext = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pNext == NULL)
	{
		perror("SnakeMove::malloc()");
		return;
	}
	pNext ->next = NULL;

	//根据按键按的方向进行移动
	switch(ps->dir)
	{
	case UP:
		pNext->x = ps->pSnake->x;
		pNext->y = ps->pSnake->y - 1;
		break;
	case DOWN:
		pNext->x = ps->pSnake->x;
		pNext->y = ps->pSnake->y + 1;
		break;
	case LEFT:
		pNext->x = ps->pSnake->x - 2;
		pNext->y = ps->pSnake->y;
		break;
	case RIGHT:
		pNext->x = ps->pSnake->x + 2;
		pNext->y = ps->pSnake->y;
		break;

	}

	//下一个坐标处是否是食物,是，返回1，不是，返回0
	if (NextIsFood(ps, pNext))
	{
		//是食物就吃掉，蛇身变长
		EatFood(ps, pNext);
	}
	else
	{
		//不是食物就正常走一步
		NotEatFood(ps, pNext);
	}

	//检测是否撞墙
	KillByWall(ps);

	//检测是否撞到自己
	KillBySeif(ps);
}


void GameRun(pSnake ps)
{
	//打印帮助信息
	PrintHelpInfo();
	do
	{
		//当前的分数情况
		SetPos(62, 10);
		printf("总分：%5d\n", ps->Score);
		SetPos(62, 11);
		printf("每个食物的分值：%02d\n", ps->FoodWeight);

		//检测按键情况
		//上，下，左，右，ESC，空格，F3，F4
		if (KEY_PRESS(VK_UP) && ps->dir != DOWN)
		{
			ps->dir = UP;
		}
		else if (KEY_PRESS(VK_DOWN) && ps->dir != UP)
		{
			ps->dir = DOWN;
		}
		else if ((KEY_PRESS(VK_LEFT) && ps->dir != RIGHT))
		{
			ps->dir = LEFT;
		}
		else if ((KEY_PRESS(VK_RIGHT) && ps->dir != LEFT))
		{
			ps->dir = RIGHT;
		}
		else if (KEY_PRESS(VK_ESCAPE))
		{
			ps->status = ESC;
			break;
		}
		else if (KEY_PRESS(VK_SPACE))
		{
			//游戏要暂停
			Pause();//暂停和恢复
		}
		else if (KEY_PRESS(VK_F3))
		{
			//加速：休眠时间要变短
			if (ps->SleepTime >= 80)
			{
				ps->SleepTime -= 30;//每加速一次，休眠时间减少30毫秒
				ps->FoodWeight += 2;//每加速一次，食物分数加2分
			}
		}
		else if (KEY_PRESS(VK_F4))
		{
			//减速：休眠时间要变长
			if (ps->FoodWeight>2)
			{
				ps->SleepTime += 30;//每减速一次，休眠时间增加30毫秒
				ps->FoodWeight -= 2;//每减速一次，食物分数减2分
			}
		}

		//睡眠一下
		Sleep(ps->SleepTime);

		//蛇走的过程
		SnakeMove(ps);

	} while (ps->status == OK);

}


void GameEnd(pSnake ps)
{
	SetPos(18, 12);
	//说明清楚游戏结束的状态
	switch(ps->status)
	{
	case ESC:
		printf("退出游戏!\n");
		break;
	case KILL_BY_WALL:
		printf("很遗憾，撞墙了，游戏结束!\n");
		break;
	case KILL_BY_SELF:
		printf("很遗憾，撞到自己了，游戏结束!\n");
		break;
	}

	//是否贪吃蛇链表资源
	pSnakeNode cur = ps->pSnake;
	while (cur)
	{
		pSnakeNode del = cur->next;
		free(cur);
		cur = del;
	}

	free(ps->pFood);
	ps = NULL;
}