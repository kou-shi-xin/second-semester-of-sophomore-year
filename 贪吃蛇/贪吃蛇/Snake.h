#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>
#include <stdbool.h>
#include <locale.h>

#define WALL L'□' //墙体
#define BODY L'●' //蛇身
#define FOOD L'★' //食物

//蛇默认的起始位置
#define POS_X 24
#define POS_Y 5 

//游戏的状态
enum GAME_STATUS
{
	OK = 1,//游戏正常进行
	ESC,//按ESC键退出
	KILL_BY_WALL,//撞墙
	KILL_BY_SELF//自己撞自己

};


//蛇的方向
enum DIRECTION
{
	UP=1,
	DOWN,
	LEFT,
	RIGHT

};

//维护一个蛇身的节点
//定义蛇身的节点: 蛇身坐标+下一个节点的指针
typedef struct SnakeNode
{
	int x;
	int y;
	struct SnakeNode* next;
}SnakeNode, * pSnakeNode;


//贪吃蛇 -- 整个游戏中的维护：
// 蛇头+食物+方向+当前总分数+一个食物的分数+蛇的状态+蛇的速度
typedef struct Snake
{
	pSnakeNode pSnake;//维护整条蛇的指针
	pSnakeNode pFood;//指向食物的指针，蛇会吃掉食物，所以类似蛇的节点类型,增长蛇身
	int Score;//当前累计的分数
	int FoodWeight;//一个食物的分数
	int SleepTime;//蛇休眠的时间，休眠的时间越短，蛇的速度越快，休眠的时间越长，蛇的速度越慢
	enum GAME_STATUS status;//游戏当前的状态
	enum DIRECTION dir;//蛇当前走的方向

}Snake ,* pSnake;

//定位函数
void SetPos(int x, int y);

//获取按键情况，检测按键是否按过，1为按过，0为没2按过
#define KEY_PRESS(vk)   (GetAsyncKeyState(vk) & 0x1 ? 1:0)

//游戏开始的准备环节
void GameStart(pSnake ps);

//打印欢迎信息
void WelcomeToGame();

//绘制地图
void CreateMap();

//初始化蛇
void InitSnake(pSnake ps);

//创建食物
void CreateFood(pSnake ps);

//游戏运行的整个逻辑
void GameRun(pSnake ps);

//打印帮助信息
void PrintHelpInfo();

//蛇移动的函数 -- 每一次走一步
void SnakeMove(pSnake ps);

//判断蛇头的下一步要走的节点是不是食物
int NextIsFood(pSnake ps, pSnakeNode pNext);

//下一步要走的位置是食物，就吃掉
void EatFood(pSnake ps, pSnakeNode pNext);

//下一步要走的位置不是食物，就继续走
void NotEatFood(pSnake ps, pSnakeNode pNext);

//检测是否撞墙
void KillByWall(pSnake ps);

//检测是否撞到自己
void KillBySeif(pSnake ps);

//游戏结束释放资源
void GameEnd(pSnake ps);